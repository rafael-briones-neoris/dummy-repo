const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const merge = require('webpack-merge');
var CopyWebpackPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (env) => {
    const extractCSS = new ExtractTextPlugin('vendor.css');
    const isDevBuild = !(env && env.prod);
    const sharedConfig = {
        watchOptions: {
            ignored: /node_modules/
        },
        stats: { modules: false },
        resolve: { extensions: ['.js'] },
        module: {
            rules: [
                { test: /\.(png|woff|woff2|eot|ttf|svg)(\?|$)/, use: 'url-loader' }
            ]
        },
        entry: {
            vendor: [
                // '@angular/animations',
                '@angular/common',
                // '@angular/compiler',
                '@angular/core',
                // '@angular/forms',
                '@angular/http',
                // '@angular/platform-browser',
                // '@angular/platform-browser-dynamic',
                '@angular/router',
                'es6-shim',
                'es6-promise',
                'event-source-polyfill',
                'font-awesome/css/font-awesome.css',
                //'localforage',
                'zone.js',
            ]
        },
        output: {
            publicPath: '/drivers/dist/',
            filename: '[name].js',
            library: '[name]_[hash]'
        },
        plugins: [
            new CopyWebpackPlugin([{ from: path.join(__dirname, './wwwroot/vendor/locale-en.json'), to: path.join(__dirname, './wwwroot/drivers/dist/locale-en.json') }]),
            new CopyWebpackPlugin([{ from: path.join(__dirname, './wwwroot/vendor/locale-es.json'), to: path.join(__dirname, './wwwroot/drivers/dist/locale-es.json') }]),
            new webpack.ContextReplacementPlugin(/\@angular\b.*\b(bundles|linker)/, path.join(__dirname, './ClientApp')), // Workaround for https://github.com/angular/angular/issues/11580
            new webpack.ContextReplacementPlugin(/angular(\\|\/)core(\\|\/)@angular/, path.join(__dirname, './ClientApp')), // Workaround for https://github.com/angular/angular/issues/14898
            new webpack.IgnorePlugin(/^vertx$/) // Workaround for https://github.com/stefanpenner/es6-promise/issues/100
        ]
    };

    const clientBundleConfig = merge(sharedConfig, {
        output: { path: path.join(__dirname, 'wwwroot', 'drivers', 'dist') },
        module: {
            rules: [
                { test: /\.css(\?|$)/, use: extractCSS.extract({ use: isDevBuild ? 'css-loader' : 'css-loader?minimize' }) }
            ]
        },
        plugins: [
            extractCSS,
            new webpack.DllPlugin({
                path: path.join(__dirname, 'wwwroot', 'drivers', 'dist', '[name]-manifest.json'),
                name: '[name]_[hash]'
            })
        ].concat(isDevBuild ? [] : [
            new webpack.optimize.UglifyJsPlugin(),
            new BundleAnalyzerPlugin({
                analyzerMode: 'static',
                reportFilename: 'vendor-stats.html',
                defaultSizes: 'parsed',
                openAnalyzer: false,
                generateStatsFile: false,
                statsFilename: 'vendor-stats.json',
                statsOptions: null,
                logLevel: 'info'
            })
        ])
    });

    const bootstrapBundle = {
        stats: { modules: false },
        module: {
            rules: [
                { test: /\.(png|woff|woff2|eot|ttf|svg)(\?|$)/, use: 'url-loader' },
                {
                    test: /\.css(\?|$)/,
                    use: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: [
                            { loader: 'css-loader', options: { minimize: true } },
                        ],
                    })
                }
            ]
        },
        entry: {
            bootstrap: [
                'jquery',
                // 'tether',
                'bootstrap'
            ],
            bootstrapcss: 'bootstrap/dist/css/bootstrap.min.css'
        },
        output: {
            path: path.join(__dirname, './wwwroot/drivers/dist'),
            publicPath: '/drivers/dist/',
            filename: '[name].bundle.js',
        },
        plugins: [
            new ExtractTextPlugin("bootstrap.css"),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery",
                Tether: "tether",
                "window.Tether": "tether",
                Popper: ['popper.js', 'default'],
            }),
        ].concat(isDevBuild ? [] : [
            new webpack.optimize.UglifyJsPlugin(),
            new BundleAnalyzerPlugin({
                analyzerMode: 'static',
                defaultSizes: 'parsed',
                reportFilename: 'bootstrap-stats.html',
                openAnalyzer: false,
                generateStatsFile: false,
                statsFilename: 'bootstrap-stats.json',
                statsOptions: null,
                logLevel: 'info'
            })
        ])
    }

    return [bootstrapBundle, clientBundleConfig];
}