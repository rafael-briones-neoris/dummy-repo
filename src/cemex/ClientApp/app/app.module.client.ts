import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { sharedConfig } from './app.module.shared';
import { TRUCK_LOCATION_OPTIONS } from './shared/types/settings/driver-setting.dto';

@NgModule({
    bootstrap: sharedConfig.bootstrap,
    declarations: sharedConfig.declarations,
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ...sharedConfig.imports
    ],
    providers: [
        { provide: 'ORIGIN_URL', useValue: location.origin },
        { provide: 'truckLocations', useValue: TRUCK_LOCATION_OPTIONS },
        { provide: 'USE_TRANSLATION_SERVER', useValue: true },
        { provide: 'PRODUCT_PATH', useValue: "/drivers/" },
        ...sharedConfig.providers
    ]
})
export class AppModule {
}
