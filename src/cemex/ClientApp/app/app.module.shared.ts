import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// Components
import { AppComponent } from './components/app/app.component';

// Services
import { AuthGuard } from '@cemex-core/angular-services-v2/dist';
import { PlantService } from './shared/services/api';

import { CmxCoreCommonModule } from '@cemex-core/angular-services-v2/dist';
import { ReportService } from './shared/services/report.service';
import { ParametersService } from './shared/services/parameters.service';

export const sharedConfig: NgModule = {
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
    ],
    providers: [
        PlantService,
        ReportService,
        ParametersService
    ],
    imports: [
        CmxCoreCommonModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'app', pathMatch: 'full' },
            { path: 'login', loadChildren: './shared/components/login/login.module#LoginModule?chunkName=login' },
            // { path: 'demo', loadChildren: './shared/components/demo-autocomplete/demo-autocomplete.module#DemoAutoCompleteModule?chunkName=demoautocomplete' },
            { path: 'app', loadChildren: './components/dashboard/dashboard.module#DashboardModule?chunkName=dashboard', canActivate: [AuthGuard] },
            { path: 'terms', loadChildren: './components/terms/terms.module#TermsModule?chunkName=terms', canActivate: [AuthGuard] },
            { path: 'legal', loadChildren: './components/legal/legal.module#LegalModule?chunkName=legal' },
            { path: '**', redirectTo: 'app' }
        ])
    ],
};
