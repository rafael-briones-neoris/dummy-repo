import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '@cemex-core/angular-services-v2/dist';
import { flatten } from '@angular/compiler';
import { DriversCoreService, RefreshTokenService } from '../../shared/services';
import { LegalDocumentsDialogComponent } from '../../shared/components/legal-documents-dialog/legal-documents-dialog.component';
import { Utils } from '../../shared/helpers/utils';
import { CmxSidebarDrivers } from '../../shared/components/cmx-sidebar-drivers/cmx-sidebar-drivers.component';
import { IDriversAplicationMenu } from '../../shared/types/index.interface'

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.html',
    styleUrls: ['./dashboard.scss']
})

export class DashboardComponent implements OnInit {

    @ViewChild(CmxSidebarDrivers)
    private _sidebar: CmxSidebarDrivers;
    private _isCollapsed: boolean = false;
    private _menuItems: IDriversAplicationMenu[];
    private options: any = {
        enableToClose: true,
        enableFooter: true
    }

    @ViewChild('legalDocumentGU')
    private legalDocumentGU: LegalDocumentsDialogComponent;

    private Subscriptions = [];
    private termVersionItem: any;

    constructor(private t: TranslationService, private core: DriversCoreService,
        refreshTokenService:RefreshTokenService,
    ) {
        this._menuItems = core.getMenuByRole();
    }

    ngOnInit() {

    }

    private clickMenuButton(event: any) {
        this._sidebar.isCollapsed = !this._sidebar.isCollapsed;
    }

    private collapseEvent(isCollapsed: boolean): void {
        this._isCollapsed = isCollapsed;
    }

    private openLegalDocument(document: string) {
        let docType = Utils.getLegalDocumentType(document);
        this.Subscriptions.push(this.core.getDocumentsClientHTML(docType.type).subscribe((terms) => {
            this.termVersionItem = terms.TermVersionItem;
            this.legalDocumentGU.initDialog(this.termVersionItem, docType.label);
        }));
    }

    private closeLegalDocument() {
        this.legalDocumentGU.closeDialog();
    }

    private onLegalDocumentClicked(params: any) {
        this.options.enableFooter = params.enableFooter;
        this.openLegalDocument(params.type);
    }
}
