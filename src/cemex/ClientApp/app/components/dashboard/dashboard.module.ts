import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CmxNavHeaderModule } from '@cemex/cmx-nav-header-v1/dist';
import { CmxCoreCommonModule } from '@cemex-core/angular-services-v2/dist';
import { DashboardComponent } from './dashboard.component';
import { DriversSharedModule } from '../../shared/components/index';

import { DriversCoreService, SearchSortService, DriversGuard, RefreshTokenService } from './../../shared/services/index';

const ROUTES: Routes = [
    {
        path: '', component: DashboardComponent,
        canActivateChild: [DriversGuard],
        children: [
            { path: '', redirectTo: 'map', pathMatch: 'full' },
            { path: 'map', loadChildren: '../map-drivers/map-drivers.module#MapDriversModule?chunkName=mapdrivers' },
            { path: 'scheduling', loadChildren: '../scheduling/scheduling.module#SchedulingModule?chunkName=scheduling' },
            { path: 'reports', loadChildren: '../reports/reports.module#ReportsModule?chunkName=reports' },
            { path: 'geofences',loadChildren:'../geofences/geofences.module#GeofencesModule?chunkName=geofences'}
        ]
    }
];

@NgModule({
    imports: [
        FlexLayoutModule,
        FormsModule, 
        ReactiveFormsModule,
        CommonModule,
        RouterModule.forChild(ROUTES),
        CmxCoreCommonModule,
        CmxNavHeaderModule,
        DriversSharedModule,
    ],
    declarations: [
        DashboardComponent,
    ],
    providers: [
        DriversCoreService, 
        SearchSortService,
        DriversGuard,
        RefreshTokenService
    ],
    entryComponents: []
})
export class DashboardModule { }