import { Component, OnInit, ViewChild, ElementRef, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CmxDialogV3 } from "@cemex/cmx-dialog-v3/dist";
import { DriversCoreService } from '../../shared/services/drivers-core.service';
import { LegalDocumentsDialogComponent } from '../../shared/components/legal-documents-dialog/legal-documents-dialog.component';
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { Utils } from '../../shared/helpers/utils';
import { DriversConstants } from '../../shared/constants/drivers.constants';

let moment = require('moment');

@Component({
    selector: 'app-legal',
    templateUrl: './legal.component.html',
    styleUrls: ['./legal.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LegalComponent implements OnDestroy {

    private dataContainer: any;
    
    private legalURL: string;
    private legalDocumentType: string = 'terms';
    private legalDocumentLabel: string = 'label.legal.terms_and_conditions';
    private subscription;
    
    private countrySelected: number = -1;
    private menuSelected: boolean = true;
    private menuSelectedLang: boolean = false;
    private langSelected: string = "en";
    private text_country = "label.selectCountry";
    private text_lang: string;

    private languages: Language[] = [
        new Language('en', 'views.header.english'),
        new Language('es', 'views.header.spanish')
    ];
  
    private countries: Country[] = [
        new Country(DriversConstants.COUNTRY_NAME_USA, DriversConstants.COUNTRY_ISOCODE_USA),
        new Country(DriversConstants.COUNTRY_NAME_MEXICO, DriversConstants.COUNTRY_ISOCODE_MEXICO)
    ];

    constructor(private t: TranslationService, private route:Router, private core: DriversCoreService) {
        this.subscription = route.events.subscribe(event => this.reloadEvent(event));
        this.changeLanguage(localStorage.getItem('Language') || 'en');
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private reloadEvent(event: any){
        if(this.legalURL !== event.url){
            this.legalURL = event.url;
            this.modifyPage();
        }
    }

    private modifyPage(){
        if(this.legalURL.endsWith('terms') || this.legalURL.endsWith('privacy')){
            const type = this.legalURL.endsWith('terms') ? 'legal' : 'privacy';
            this.countrySelected = -1;
            this.legalDocumentType = type;
            this.legalDocumentLabel = type === 'legal' ? 'label.legal.terms_and_conditions' : 'label.legal.privacy_policy';
            this.dataContainer = null;
        }
    }

    private openMenu() {
        this.menuSelected = !this.menuSelected;
    }

    private changeCountry(isoCode: number) {
        this.menuSelected = !this.menuSelected;
        this.countrySelected = isoCode;

        if(this.isoCodeInsideCountries(this.countrySelected)) {
            this.triggerDocumentDownload();
        } else{
            this.deselectCountry();
        }
    }

    private isoCodeInsideCountries(isoCode: number){
        for(let c of this.countries){
            if(c.isoCode == isoCode) {
                return true;
            }
        }
        return false;
    }

    private deselectCountry() {
        this.menuSelected = !this.menuSelected;
        this.countrySelected = -1;
        this.dataContainer = null;
    }

    private triggerDocumentDownload() {
        if(this.countrySelected != -1){
            let doc = Utils.getLegalDocumentType(this.legalDocumentType);
            this.core.getPublicDocumentsClientHTML(doc.type, this.countrySelected).subscribe((terms) => {
                this.dataContainer = terms.TermVersionItem.termDocument;
            });
        }
    }

    private openMenuLang() {
        this.menuSelectedLang = !this.menuSelectedLang;
    }

    private changeLanguage(lang: any) {
        this.menuSelectedLang = !this.menuSelectedLang;
        this.t.lang(lang);
        this.langSelected = lang;
    
        if (lang == "en") {
            this.text_lang = 'views.header.english';
    
        } else {
            this.text_lang = 'views.header.spanish';
        }
        localStorage.setItem('Language', lang);
    }

    private onLegalDocumentClicked(params: any){
        if(params.type === 'legal'){
            this.route.navigate(['legal/terms']);
        }
        else if(params.type === 'privacy'){
            this.route.navigate(['legal/privacy']);
        }
    }
  
  }
  
class Language {
    id: string;
    text: string;
    
    constructor(id: string, text: string) {
        this.id = id;
        this.text = text;
    }
}

class Country {
    name: string;
    isoCode: number;

    constructor(name: string, isoCode: number) {
        this.name = name;
        this.isoCode = isoCode;
    }
}