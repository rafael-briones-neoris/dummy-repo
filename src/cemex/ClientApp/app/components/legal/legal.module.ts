import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LegalComponent } from './legal.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DriversSharedModule } from '../../shared/components/index';
import { DriversCoreService, SearchSortService } from '../../shared/services/index';

const ROUTES: Routes = [
    {
        path: '', component: LegalComponent, children: [
            {path: '', redirectTo: 'terms', pathMatch: 'full'},
            {path: 'terms', component: LegalComponent},
            {path: 'privacy', component: LegalComponent}
          ]
    }
];

@NgModule({
    imports: [
        FlexLayoutModule,
        ReactiveFormsModule,
        CommonModule,
        RouterModule,
        RouterModule.forChild(ROUTES),
        DriversSharedModule
    ],
    declarations: [
        LegalComponent
    ],
    providers: [DriversCoreService, SearchSortService],
    entryComponents: []
})
export class LegalModule { }

