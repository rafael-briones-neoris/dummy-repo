
import { Component, Input, ViewChild, OnInit, PipeTransform, Pipe, Inject } from '@angular/core';
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { PlantService } from '../../shared/services/api/PlantService';
import { DriversConstants } from '../../shared/constants/drivers.constants';

/*sdfdf*/
import { OrderService } from '../../shared/services/order.service';
import { ISearchElement } from '../../shared/components/cmx-input-autocomplete/cmx-autocomplete-utils';
import { ParametersService } from '../../shared/services/parameters.service';
let _  = require("underscore");
@Component({
    selector: 'reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss']
})

export class ReportsComponent {
    @ViewChild('review') review;
    @ViewChild('autocomplete') autocomplete;

    public checkedAllPlants:boolean = false;

    public _resultData: any[];
    public criteriaData: any;
    public plantsPlaceHolder: string = "Select plants";
    public date: any;
    public enableDatepicker = false;
    public selectedReport: string;
    public selectedReportHeaderData: any;
    
    public data: any = {};
    public criteria: any;
    public plants: any;
    public plantsSelected: any = [];

    /*autocomplete*/
    public searchValue: string = "";
    public showAutocomplete:boolean = true;

    constructor( private t: TranslationService, 
                 private svrplants: PlantService, 
                 public orderService: OrderService,
                 private parametersService: ParametersService ) {
        this.svrplants.getAllPlants().then((data) => {
            this.plants = data.plants;
        }).catch((error) => {
            console.log("Error message", error.err);
        });
    }

    private getSearchResults(result: ISearchElement[]): void {
        // const checked:boolean = this.plants.some((it) => it.checked);

        // if(checked) { 
            this._resultData = _.clone(result); 
        // }
    }

    public onSelectElementEvent(item) {
        // console.log("onSelectElementEvent", item);
    }

    public onClickItem(item) { 
        // console.log("onClickItem", item);
        this.searchValue = item.displayData;
        this.criteriaData = item.objectData.value;
    }

    public tableReportHeader = {
        speeding: [
            { columnName: 'Date', columnField: 'date', localeName: 'reports.date', width: 14 },
            { columnName: 'Hour', columnField: 'hour', localeName: 'reports.hour', width: 14}, 
            { columnName: 'Plant' , columnField: 'plant', localeName: 'reports.plant', width: 14}, 
            { columnName: 'Driver Id' , columnField: 'driverCode', localeName: 'reports.driverId', width: 14},
            { columnName: 'Driver Name', columnField: 'driverDesc', localeName: 'reports.drivername', width: 14}, 
            { columnName: this.parametersService.get("UNIT_OF_SPEED"), columnField: 'speed', localeName: false, width: 14}, 
            { columnName: 'Truck Id', columnField: 'truckPlate', localeName: 'reports.truckid', width: 15}
        ],
       
        waitingTime: [
            { columnName: 'Order', columnField: 'orderCode', width: 11, localeName: "reports.order"},
            { columnName: 'Load', columnField: 'load', width: 10, localeName: "reports.load"},
            { columnName: 'Client Id',columnField: 'customerCode', width: 10, localeName: "reports.clientid"},
            { columnName: 'Client Name', columnField: 'customerDesc', width: 18, localeName: "reports.clientname"},
            { columnName: 'Plant', columnField: 'plant', width: 20, localeName: "reports.plant"},
            { columnName: 'Delivery #', columnField: 'ticketCode', width: 11, localeName: "reports.delivery"},
            { columnName: 'Date', filter: true, columnField: 'date', width: 8, localeName: "reports.date"},
            { columnName: 'Wait', columnField: 'waitingTime', width: 8, localeName: "reports.wait"},
        ],

        hoursOfService: [
            { columnName: 'Driver Id', columnField: 'driverCode', width: 15, localeName: "reports.driverId"},
            { columnName: 'Driver Name',columnField: 'driverDesc', width: 30, localeName: "reports.drivername"},
            { columnName: 'Plant',columnField: 'plantDesc', width: 25, localeName: "reports.plant"},
            { columnName: 'Date', filter: false, columnField: 'date', width: 15, localeName: "reports.date"},
            { columnName: 'Hours Of Service', columnField: 'hourOfService', width: 15, localeName: "reports.hoursofservice"},
        ],
       
        perTrip: [
            { columnName: 'Order', columnField: 'orderCode', localeName: "reports.order"},
            { columnName: 'Plant', columnField: 'plantDesc', localeName: "reports.plant"},
            { columnName: 'Delivery', columnField: 'delivery', localeName: "reports.delivery"},
            { columnName: 'Truck', columnField: 'truckPlate', localeName: "reports.truckid"},
            { columnName: 'Status', columnField: 'status', localeName: "reports.status"},
            { columnName: 'Date', filter: true, columnField: 'date', localeName: "reports.date"},
            { columnName: 'Hour', columnField: 'hour', localeName: "reports.hour"},
        ]
    }

    
    public getSelectedReport($event) {
        this.selectedReport = $event
    }
    
    public onSelection( e ) { 
        this.date = e.value;
        this.updateUrlToAutocomplete();
    }

    public checkAllPlants( e:boolean ) { 
        this.checkedAllPlants = e;
        
        for( let plant of this.plants ) { plant.checked = e; }
        this.updateUrlToAutocomplete();
        this.plantsPlaceHolder = ( e ) ? this.t.pt("reports.all-plants") : this.t.pt("views.newuser.all_plants_modal");
        this.plantsSelected = this.getSelectedPlants();
    }

    public checkPlant(plant, e:boolean) {
        plant.checked = e;
        this.updatePlantsDropdown();
        this.updateUrlToAutocomplete();
    }

    public updatePlantsDropdown() { 
        let _plants = this.getSelectedPlants();
        this.plantsSelected = this.getSelectedPlants();
        if(! _plants.length ) { this.plantsPlaceHolder = "Select plants"; return; };
        this.plantsPlaceHolder = _plants.length + " plants";
        if( _plants.length === 1 ) { this.plantsPlaceHolder = _plants[0].plantDesc; };
    }

    public generateReport( reportType ) {
        this.selectedReportHeaderData = this.tableReportHeader[reportType];
        this.data.report = reportType;
        this.data.criteria = this.criteria;
        this.data.plants = this.getSelectedPlants(true);
        this.data.date = this.date;
        this.data.criteriaData = this.criteriaData; 
        console.log("data to send", this.data);
        this.review.generateReport(this.data);
    }

    public getSelectedPlants(stringable?: boolean) {
        let _plants:any =  ( stringable ) ? "" : [];
        for( let plant of this.plants ) { 
            if( plant.checked ) {
                if ( stringable ) { 
                    _plants += plant.plantId + ",";
                }
                else { 
                    _plants.push(plant);
                }
            }
        }
        return ( stringable ) ? _plants.substring(0, _plants.length - 1) : _plants;
    }

    public exportToExcel() { 
        this.review.exportToExcel(this.data.report);
    }

    public resetReport(criteria) {
        // this.searchValue = "";
        this.autocomplete.reset();

        this.criteria = criteria;
        this.date = null;
        if( this.selectedReportHeaderData ) {
            this.selectedReportHeaderData = false;
        }

        if(this.criteriaData) { 
            this.criteriaData = null;
        }

        
        if( this.plants ) { 
            this.checkAllPlants(false);
        }
        

        if(!this.enableDatepicker){ 
            this.enableDatepicker = true;
        }
        else { 
            this.enableDatepicker = false;
            setTimeout(() => {
                this.enableDatepicker = true;
            }, 50);
        }
        this.review.resetData();
    }

    public updateUrlToAutocomplete() {  
        if( this.criteria !== 'perTrip' ) { return; }
        if( this.date ) { this.orderService.setDates(this.date); }
        this.orderService.setPlants(this.getSelectedPlants(true));
    }
}
