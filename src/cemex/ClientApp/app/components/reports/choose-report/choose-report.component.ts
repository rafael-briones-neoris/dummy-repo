import { Component, Input, Output, EventEmitter } from '@angular/core';



@Component({
    selector: 'choose-report-template',
    templateUrl: './choose-report.component.html',
    styleUrls: ['./choose-report.component.scss']
})

export class ChooseReportComponent {
    public reportSelected: string = 'Speeding';
    @Output() selectedReport = new EventEmitter();
    public chooseReportList = [
        {id: 1, title: 'Speeding', 
        content: 'Verify the drivers and the units that incurred in excess of the speed by date renge and plant.'} ,
        {id: 2, title: 'Hours of Service', content: 'Get data of what drivers have gone beyond the recommended hours of service for a certain period.'} ,
        {id: 3, title: 'Waiting Time', content: 'Verify what drivers have waited more than the recommended time before unloading the product at client site.'} , 
        {id: 4, title: 'Per Trip', content: 'Gather the data of the status changes per trip and verify the exact times when they occurred.'} , 
        {id: 5, title: 'Map History', content: 'Refine whole delivery journeys by plant and date and review the detailed sheet of each delivery.'} ,
    ]

    onReportChange($event){
        this.reportSelected = $event.value;
        let reportFound = this.chooseReportList.find((report) => report.title === $event.value)
        this.selectedReport.emit(reportFound);
    }

}

