import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ReportService } from '../../../shared/services/report.service';
import * as XLSX from 'xlsx';
import { TranslationService } from '@cemex-core/angular-services-v2/dist';
import { Utils } from '../../../shared/helpers/utils';
import { DriversConstants } from '../../../shared/constants/drivers.constants';

let moment = require('moment');
let _ = require('underscore');

@Component({
    selector: 'review-report-template',
    templateUrl: './review-report.component.html',
    styleUrls: ['./review-report.component.scss']
})

export class ReviewReportComponent {
    @Input() public selectedReportHeaderData;
    @Output() public onSelectTableItem: EventEmitter<any> = new EventEmitter();
    public reviewReportData: any = [];
    public reportType: any;
    constructor(public reportService: ReportService, private t: TranslationService) { }

    public generateReport(params) {
        const url = this.generateURL(params);
        const pathConstant = this.generatePathConstants(params);
        if (!url) { return; }

        this.reportService.getReportData(pathConstant, url).then((data) => {
            if (data) {
                this.reviewReportData = this.generateData(params.report, data);
            } else {
                console.log(params.report, 'no content');
            }
            return;
        });
    }

    public exportToExcel(criteria) {
        const data = this.getDataToExport(criteria);
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data, { skipHeader: true });
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, criteria);

        // save to file
        const wbout: string = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
        const file = new Blob([wbout]);
        this.saveAs(file, criteria + '.xlsx');
    }

    private getHumanTime(hours) {
        const time = moment.duration(hours, 'hours');
        let _time: string = '';
        if (time._data.hours > 0) {
            _time += time._data.hours + ' hours ';
        }
        if (time._data.minutes > 0) {
            _time += time._data.minutes + ' minutes';
        }
        return _time;
    }

    private saveAs(file: Blob, name: string) {
        let link = document.createElement('a');
        let url = URL.createObjectURL(file);
        link.setAttribute('href', url);
        link.setAttribute('download', name);
        link.click();
        link = undefined;
    }

    private generateURL(params): string {
        let url: string = null;
        let dateFrom = '';
        let dateTo = '';
        if (params.date) {
            dateFrom = moment(params.date.min).format('YYYY-MM-DD');
            dateTo = moment(params.date.max).format('YYYY-MM-DD');
        }

        switch (params.report) {
            case 'speeding': {
                url = '?plantId=' + params.plants + '&dateFrom=' + dateFrom + '&dateTo=' + dateTo + '&pagination=false';
                break;
            }

            case 'waitingTime': {
                let criteria = (params.criteria === 'order') ? '&customerId=&orderId=' : '&orderId=&customerId=';
                url = '?plantId=' + params.plants + '&dateFrom=' + dateFrom + '&dateTo=' + dateTo + criteria + params.criteriaData + '&pagination=false';
                break;
            }

            case 'hoursOfService': {
                url = '?plantId=' + params.plants + '&dateFrom=' + dateFrom + '&dateTo=' + dateTo + '&pagination=false';
                break;
            }

            case 'perTrip': {
                url = '?plantId=' + params.plants + '&orderId=' + params.criteriaData + '&pagination=false';
                break;
            }

            case 'mapHistory': {
                let criteria: string;
                let hourFrom: string = params.time.min + ':00';
                let hourTo: string = params.time.max + ':00';
                switch (params.criteria) {
                    case 'order': {
                        criteria = 'orderCode=' + params.criteriaData + '&driverCode=&truckPlate=';
                        break;
                    }
                    case 'truck': {
                        criteria = 'orderCode=&driverCode=&truckPlate=' + params.criteriaData;
                        break;
                    }
                    case 'driver': {
                        criteria = 'orderCode=&driverCode=' + params.criteriaData + '&truckPlate=';
                        break;
                    }
                }
                //truckPlate=328DZ2
                url = '?' + criteria + '&dateFrom=' + dateFrom + '&dateTo='
                    + dateTo + '&include=ticket&reportType=maphistory' + '&hourFrom='
                    + hourFrom + '&hourTo=' + hourTo;
            }
        }
        return url;
    }

    private generatePathConstants(params): string {
        let path: string = '';

        switch (params.report) {
            case 'speeding': {
                path = DriversConstants.SPEEDING_REP_PATH;
                break;
            }

            case 'waitingTime': {
                path = DriversConstants.WAITING_PATH;
                break;
            }

            case 'hoursOfService': {
                path = DriversConstants.HOURS_PATH;
                break;
            }

            case 'perTrip': {
                path = DriversConstants.PERTRIPREP_PATH;
                break;
            }

            case 'mapHistory': {
                path = DriversConstants.REPORDERS;
                break;
            }
        }
        return path;
    }

    private generateData(type: string, data: any): any[] {
        let result = [];
        switch (type) {

            case 'speeding': {
                for (let item of data.reportResults) {
                    item.dataReport = [];
                    for (let subitem of item.speedingAlertReports) {
                        let object = {
                            date: moment(subitem.alertTime).format(Utils.getDateFormat()),
                            hour: moment(subitem.alertTime).format('HH:mm'),
                            plant: item.plantCode + ' | ' + item.plantDesc,
                            driverCode: subitem.driverCode,
                            driverDesc: subitem.driverDesc,
                            speed: subitem.speed,
                            truckPlate: subitem.truckPlate,
                        }
                        item.dataReport.push(object);
                    }
                }
                result = data;
                break;
            }

            case 'waitingTime': {
                for (let item of data.reportResults) {
                    item.dataReport = [];
                    for (let subitem of item.waitingTimeReports) {
                        let object = {
                            orderCode: subitem.orderCode,
                            load: subitem.load,
                            customerCode: subitem.customerCode,
                            customerDesc: subitem.customerDesc,
                            plant: item.plantCode + ' | ' + item.plantDesc,
                            ticketCode: subitem.ticketCode,
                            date: moment(subitem.alertTime).format(Utils.getDateFormat()),
                            waitingTime: subitem.waitingTime,
                        }
                        item.dataReport.push(object);
                    }
                }
                result = data;
                break;
            }

            case 'hoursOfService': {
                for (let item of data.reportResults) {
                    item.dataReport = [];
                    let object = {};
                    for (let subitem of item.hourOfServiceReports) {
                        subitem.hours = subitem.totalHours;
                        for (let subsubitem of subitem.hourOfServices) {
                            object = {
                                driverCode: subitem.driverCode,
                                driverDesc: subitem.driverDesc,
                                plantDesc: item.plantCode + ' | ' + item.plantDesc,
                                date: moment(subsubitem.shiftDate).format(Utils.getDateFormat()),
                                hourOfService: this.getHumanTime(subsubitem.hourOfService),
                            }
                            item.dataReport.push(object);
                        }
                    }
                }
                result = data;
                break;
            }

            case 'perTrip': {
                for (let item of data.reportResults) {
                    item.dataReport = [];
                    let object = {};
                    for (let subitem of item.perTripStatusReports) {
                        for (let subsubitem of subitem.truckEvents) {
                            object = {
                                orderCode: item.orderCode,
                                plantDesc: item.plantCode + ' | ' + item.plantDesc,
                                delivery: subitem.ticketCode,
                                truckPlate: subitem.truckPlate,
                                status: subsubitem.truckEventDesc,
                                date: moment(subsubitem.eventDate).format(Utils.getDateFormat()),
                                hour: moment(subsubitem.eventDate).format('HH:mm'),
                            }
                            item.dataReport.push(object);
                        }
                    }
                }
                result = data;
                break;
            }

            case 'mapHistory': {
                data.reportResults = [];
                let newdata = [];
                for (let item of data.orders) {
                    for (let subitem of item.tickets) {
                        let row = {
                            orderCode: item.orderCode,
                            driverCode: subitem.shipment.driver.driverCode,
                            driverName: subitem.shipment.driver.driverDesc,
                            truckId: subitem.shipment.driver.assignation.truck.truckPlate,
                            truckRealId: subitem.shipment.driver.assignation.truck.truckId,
                            date: moment(subitem.deliveryDate).format(Utils.getDateFormat()),
                            hour: moment(subitem.deliveryDate).format('HH:mm'),
                            ticketId: subitem.ticketId,
                        }
                        newdata.push(row);
                    }
                }
                if (!newdata.length) {
                    result = [];
                    break;
                }

                data.reportResults[0] = { dataReport: newdata };
                data.orders = undefined;
                result = data;
                break;
            }
        };
        return result;
    }

    public onSelectItem(item) {
        this.onSelectTableItem.emit(item);
    }

    private getDataToExport(criteria) {
        let data = [];
        for (let item of this.reviewReportData.reportResults) {
            for (let subitem of item.dataReport) {
                data.push(subitem);
            }
        }
        data.unshift(this.getHeaders(criteria, data[0]));
        return data;
    }

    private getHeaders(criteria, item) {
        if (!item) { return {}; }

        let _new = {};
        let headers = [];
        if (criteria === 'speeding') {
            headers = [this.t.pt('reports.date'), this.t.pt('reports.hour'), this.t.pt('reports.plant'), this.t.pt('reports.driverId'), this.t.pt('reports.drivername'), 'MPH', this.t.pt('reports.truckid')];
        }

        if (criteria === 'waitingTime') {
            headers = [this.t.pt('reports.order'), this.t.pt('reports.load'), this.t.pt('reports.clientid'), this.t.pt('reports.clientname'), this.t.pt('reports.plant'), this.t.pt('reports.delivery'), this.t.pt('reports.date'), this.t.pt('reports.wait')];
        }

        if (criteria === 'hoursOfService') {
            headers = [this.t.pt('reports.driverId'), this.t.pt('reports.drivername'), this.t.pt('reports.plant'), this.t.pt('reports.date'), this.t.pt('reports.hoursofservice')];
        }

        if (criteria === 'perTrip') {
            headers = [this.t.pt('reports.order'), this.t.pt('reports.plant'), this.t.pt('reports.delivery'), this.t.pt('reports.truckid'), this.t.pt('reports.status'), this.t.pt('reports.date'), this.t.pt('reports.hour')]
        }

        let keys = Object.keys(item);
        for (let index in keys) {
            _new[keys[index]] = headers[index];
        }

        return _new;
    }

    public resetData() {
        this.reviewReportData = [];
    }
}
