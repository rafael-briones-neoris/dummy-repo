import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { CmxDialogV3 } from "@cemex/cmx-dialog-v3/dist";
import { CmxMapComponent, ApiLoaderService, MAP_STYLES } from '@cemex/cmx-map-v1';
import { ReportService } from '../../../shared/services/report.service';
import { SassHelper } from '../../../components/sass-helper/sass-helper.component';
import { DriversConstants } from '../../../shared/constants/drivers.constants';
import { MapConstants } from '../../../shared/constants/maps.constants';
import { RequestOptions, Headers, URLSearchParams, Response } from '@angular/http';

let moment = require("moment");

@Component({
    selector: 'map-history-template',
    templateUrl: './map-history.component.html',
    styleUrls: ['./map-history.component.scss']
})

export class MapHistoryComponent {

    // Misc. Data
    public criteriaData: any;
    public time: any;
    public date: any;
    public data: any = {};
    public ticket: any = {};

    // Map
    private _mapOptions: google.maps.MapOptions;
    private mapElement: CmxMapComponent;
    
    // Icons used in the Map
    protected jobsiteIcon: string = MapConstants.ICONS.JOBSITE.icon;
    protected plantIcon: string = MapConstants.ICONS.POD.icon;
    protected alertIcons = {
        1 : MapConstants.ICONS.HISTORY_ALERTS.speedingIcon,
        2 : MapConstants.ICONS.HISTORY_ALERTS.hoursServiceIcon,
        3 : MapConstants.ICONS.HISTORY_ALERTS.waitingTimeIcon
    };

    // Label that defines what it expects in the criteriaData input, depending in the Radio Button value
    public criteria: string = 'order';
    public titles = {
        "order": 'map-history.panel.order',
        "truck": 'map-history.panel.truck',
        "driver": 'map-history.panel.driver'
    };

    @ViewChild('review') public review;
    @ViewChild('mapHistoryDialog') private mapHistoryDialog: CmxDialogV3;
    @ViewChild('SassHelper') private sassHelper: SassHelper;

    public tableReportHeader = {
        mapHistory: [
            { columnName: 'Order Id', clickable: true, localeName: 'reports.order', columnField: 'orderCode', width: 18 },
            { columnName: 'Driver Id', clickable: true, localeName: 'reports.driverId', columnField: 'driverCode', width: 18 },
            { columnName: 'Driver Name', clickable: true, localeName: 'reports.drivername', columnField: 'driverName', width: 20 },
            { columnName: 'Truck Id', clickable: true, localeName: 'reports.truckid', columnField: 'truckId', width: 15 },
            { columnName: 'Date', filter: true, clickable: true, localeName: 'reports.date', columnField: 'date', width: 19 },
            { columnName: 'Hour', clickable: true, columnField: 'hour', localeName: 'reports.hour', width: 10 },
        ]
    };

    public timeFrom: string = '';
    public timeTo: string = '';
    public mask = [/\d/, /\d/, ':', /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, ':', /\d/, /\d/];

    constructor(private t: TranslationService, 
        public reportService: ReportService, 
        private _cdRef: ChangeDetectorRef) {
    }

    public onSelection(date) {
        this.date = date.value;
    }

    public getMapHistory() {
        this.data.report = 'mapHistory';
        this.data.criteria = this.criteria;
        this.data.date = this.date;
        this.data.time = { min: this.timeFrom, max: this.timeTo };
        this.data.criteriaData = this.criteriaData;
        this.review.generateReport(this.data);
    }

    public setMapHistory(item) {
        this.mapHistoryDialog.open();
        this.ticket = {};

        // These methods don't depend on each other
        this.getTicketData(item);
        this.getEventsData(item);
        this.getLocationsData(item);
    }

    private getTicketData(item) {
         let constants = DriversConstants.TICKET_DETAIL + "/" + item.ticketId;
         let params = "?include=shipment,driver,ticketitems,order,jobsite,weight,plant,statuslog";
         this.reportService.getReportData(constants, params).then((data) => {
             if (data) {
                 this.ticket.plant = data.plant.address.streetName;
                 this.ticket.plantPosition = {
                     lat: data.plant.address.geoPlace.latitude,
                     lng: data.plant.address.geoPlace.longitude,
                 };
                 this.ticket.jobsite = data.order.jobsite.address.streetName;
                 this.ticket.jobsitePosition = {
                     lat: data.order.jobsite.address.geoPlace.latitude,
                     lng: data.order.jobsite.address.geoPlace.longitude,
                 };
                 this.ticket.ticketCode = data.ticketCode;
                 this.ticket.orderCode = data.order.orderCode;
                 this.ticket.truckId = item.truckId;
                 this.ticket.driverCode = item.driverCode;
                 this.ticket.driverName = item.driverName;
                 this.ticket.date = item.date;
 
                 let plantPosition = new google.maps.LatLng(this.ticket.plantPosition.lat, this.ticket.plantPosition.lng);
                 let jobsitePosition = new google.maps.LatLng(this.ticket.jobsitePosition.lat, this.ticket.jobsitePosition.lng);
                 this.drawPlantJobsiteMarkers(plantPosition, jobsitePosition);
             } else {
                 console.log(data, 'No content');
             }
             return;
         });
    }

    private getEventsData(item) {
        let constants = DriversConstants.EVENTS_REP_PATH;
        let params = "?ticketId=" + item.ticketId + "&truckId=" + item.truckRealId;
        this.reportService.getReportData(constants, params).then((data) => {
            if (data) {
                this.ticket.truckEvents = [];
                for(let event of data.truckEvents) {
                    let item = {
                        truckEventDesc: event.truckEventDesc,
                        eventTime: event.eventDate ? moment(event.eventDate).format("HH:mm") : "NA",
                        fullEventTime: event.eventDate ? event.eventDate : "NA"
                    };
                    this.ticket.truckEvents.push(item);
                }
                this.setEventIcons();
            } else {
                console.log(data, 'No content');
            }
            this.getAlertsData(item);
            return;
        });
    }

    private setEventIcons() {
        for(let i = 0; i < this.ticket.truckEvents.length; i++) {
            if (this.ticket.truckEvents[i].eventTime != "NA" || i == 0) {
                this.ticket.truckEvents[i].eventIcon = "status-done";
            } else {
                this.ticket.truckEvents[i].eventIcon = "status-pending";
                if (this.ticket.truckEvents[i - 1].eventIcon == "status-done") {
                    this.ticket.truckEvents[i - 1].eventIcon = "status-progress";
                }
            }
        }
        return;
    }

    private getAlertsData(item) {
        let constants = DriversConstants.NOTIFY_PATH;
        let params = "?ticketId=" + item.ticketId + "&truckId=" + item.truckRealId;
        this.reportService.getReportData(constants, params).then((data) => {
            if (data) {
                this.ticket.truckAlerts = [];
                for(let alert of data.alerts) {
                    let item = {
                        alertType: alert.alertType.alertTypeId,
                        truckEventDesc: this.t.pt("map-history.panel.alert_type_" + alert.alertType.alertTypeId),
                        eventTime: alert.alertTime ? moment(alert.alertTime).format("HH:mm") : "NA",
                        fullEventTime: alert.alertTime ? alert.alertTime : "NA",
                        eventIcon: "status-alert-" + alert.alertType.alertTypeId,
                        latitude: alert.latitude,
                        longitude: alert.longitude
                    };
                    this.ticket.truckAlerts.push(item);
                    this.ticket.truckEvents.push(item);
                }
            } else {
                console.log(data, 'No content');
            }
            this.sortEventsAndAlerts();
            this.drawAlertMarkers();
            return;
        });
    }

    private sortEventsAndAlerts() {
        function compare(a, b) {
            let comparison = 0;

            if (a.fullEventTime == "NA" || b.fullEventTime == "NA") {
                comparison = (a.order > b.order) ? 1 : -1;
            } else if (a.fullEventTime > b.fullEventTime) {
                comparison = 1;
            } else if (b.fullEventTime > a.fullEventTime) {
                comparison = -1;
            }

            return comparison;
        }
        this.ticket.truckEvents.sort(compare);
        return;
    }

    private getLocationsData(item) {
        let constants = DriversConstants.TRUCKS_LOCATION;
        let params = "/" + item.truckRealId + "/" + item.ticketId;
        this.reportService.getTruckLocations(constants, params).then((data) => {
            if (data) {
                this.drawTripLine(data);
            } else {
                console.log(data, 'No content');
            }
            return;
        });
    }

    public createMap() {
        this._cdRef.detectChanges();
        google.maps.event.trigger(this.mapElement.mapInstance, 'resize');
    }

    public clearMap() {
        this.mapElement.deleteAllMarkers();
    }

    public onMapReady(mapComponent: CmxMapComponent) {
        this._mapOptions = {
            styles: MAP_STYLES,
            mapTypeControl: false,
            fullscreenControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: false,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
        };

        mapComponent.getCurrentLocation().subscribe((position: google.maps.LatLng) => {
            if (position) {
                mapComponent.centerCurrentLocation([position]);
            }
            mapComponent.mapOptions = this._mapOptions;
            this.mapElement = mapComponent;
            this.createMap();
        });
    }

    private drawTripLine(dataPositions:Array<any>) {
        let positions = [];
        let bounds = new google.maps.LatLngBounds();
        for(let position of dataPositions){
            let item = {
                lat: position.latitude,
                lng: position.longitude,
            }
            const latlng = new google.maps.LatLng(position.latitude, position.longitude);
            bounds.extend(latlng);
            positions.push(item);
        }

        let polyline = new google.maps.Polyline({
            path: positions,
            geodesic: true,
            strokeColor: this.sassHelper.readProperty('bright-blue'),
            strokeOpacity: 1.0,
            strokeWeight: 5
        });

        polyline.setMap(this.mapElement.mapInstance);
        this.mapElement.mapInstance.fitBounds(bounds);
    }

    private drawPlantJobsiteMarkers(plantPosition: google.maps.LatLng, jobsitePosition: google.maps.LatLng) {
        let bounds = new google.maps.LatLngBounds();
        let plantMarker = new google.maps.Marker({
            position: plantPosition,
            icon: this.plantIcon,
            clickable: false,
            draggable: false,
            zIndex: 15
        });

        let jobsiteMarker = new google.maps.Marker({
            position: jobsitePosition,
            icon: this.jobsiteIcon,
            clickable: false,
            draggable: false,
            zIndex: 15
        });

        bounds.extend(plantPosition);
        bounds.extend(jobsitePosition);
        this.mapElement.addMarker("plantMarker", plantMarker);
        this.mapElement.addMarker("jobsiteMarker", jobsiteMarker);
        this.mapElement.mapInstance.fitBounds(bounds);
    }

    private drawAlertMarkers() {
        for(let i = 0; i < this.ticket.truckAlerts.length; i++) {
            let alertIcon = {
                url: String(this.alertIcons[this.ticket.truckAlerts[i].alertType]),
                scaledSize: new google.maps.Size(12, 12),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(6, 6)
            };

            let alertMarker = new google.maps.Marker({
                position: new google.maps.LatLng(this.ticket.truckAlerts[i].latitude, this.ticket.truckAlerts[i].longitude),
                icon: alertIcon,
                clickable: false,
                draggable: false,
                zIndex: 15
            });
            this.mapElement.addMarker("alertMarker_" + i, alertMarker);
        }
    }
}