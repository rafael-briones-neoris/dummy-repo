import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { DriversSharedModule } from '../../shared/components/index';

import { ReportsComponent } from './reports.component';
import { ChooseReportComponent } from './choose-report/choose-report.component';
import { FineTuneComponent } from './fine-tune/fine-tune.component';
import { CmxRadioModule } from '@cemex/cmx-radio-v1';
import { CmxDatepickerInputModule } from '@cemex/cmx-datepicker-input-v1';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v1';
import { CmxTabsModule } from '@cemex/cmx-tabs-v1';
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v1';
import { ReviewReportComponent } from './review-report/review-report.component';
import { CmxInputAutoCompleteModule } from '../../shared/components/cmx-input-autocomplete/index';
import { OrderService } from '../../shared/services/order.service';
import { MapHistoryComponent } from './map-history/map-history.component';
import { TextMaskModule } from 'angular2-text-mask';
import { CmxMapModule } from '@cemex/cmx-map-v1/dist';
import { SassHelper } from './../sass-helper/sass-helper.component';

const ROUTES: Routes = [
    { path: '', redirectTo: 'delivery-reports', pathMatch: 'full'},
    { path: 'delivery-reports', component: ReportsComponent, pathMatch: 'full' },
    { path: 'map-history', component: MapHistoryComponent, pathMatch: 'full' }
];

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule.forChild(ROUTES),
        DriversSharedModule,
        CmxRadioModule,
        CmxDatepickerInputModule,
        CmxDropdownModule,
        CmxTabsModule,
        CmxFormFieldModule,
        CmxInputAutoCompleteModule,
        TextMaskModule,
        CmxMapModule.forRoot(),
    ],
    declarations: [
        ReportsComponent,
        FineTuneComponent,
        ChooseReportComponent,
        ReviewReportComponent,
        MapHistoryComponent,
        SassHelper,
    ],
    providers: [OrderService],
    entryComponents: []
})
export class ReportsModule { }