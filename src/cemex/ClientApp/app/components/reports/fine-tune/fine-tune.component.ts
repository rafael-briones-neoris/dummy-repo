import { Component, Input, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { PlantService } from '../../../shared/services/api/PlantService';
import { TranslationService } from "@cemex-core/angular-services-v2/dist";



@Component({
    selector: 'fine-tune-template',
    templateUrl: './fine-tune.component.html',
    styleUrls: ['./fine-tune.component.scss'],
    providers: [DatePipe]
})

export class FineTuneComponent implements OnInit {
    public reportSelected: string;
    //plantSelected: string = "";
    public fineTune: any = {};
    public plants: any = [];
    @Input() selectedReport;
    
    private _checkPlants: {
        checked: boolean;
        valuePlant: any
    }[] = [];

    constructor(public svrplants: PlantService, 
            private datePipe: DatePipe,
            private t: TranslationService) { }

    ngOnInit() {
        console.log(this.selectedReport);
        if (!this.selectedReport) {
            this.selectedReport = {
                id: 1, title: 'Speeding',
                content: 'Verify the drivers and the units that incurred in excess of the speed by date renge and plant.'
            };
        }
        if (this.selectedReport.title !== 'Map History') {
            this.svrplants.getAllPlants().then((data) => {
                this.plants = data.plants;
            }).catch((error) => {
                console.log("Error message", error.err);
            });
        }
        
    }

    onReportChange(event) {
         this.fineTune['radio'] = {};
         this.fineTune['radio'][event.value] = event.source.checked;
        this.reportSelected = event;
    }

    sendParamObj() {
        this.fineTune['report'] = this.selectedReport.title;
        this.fineTune['plants'] = [];
        this.plants.forEach((plant) => {
            if(plant.checked)
            this.fineTune['plants'].push(plant.plantId);
        })
        // this.fineTune['plants'] = this.plants.map((plant) =>{
        //     if(plant.checked){
        //         return plant.plantId;
        //     }          
        // })
        return this.fineTune;
    }

    onSelection(event) {
        this.fineTune['minDate'] = this.datePipe.transform(event.value.min, 'yyyy-MM-dd');
        this.fineTune['maxDate'] = this.datePipe.transform(event.value.max, 'yyyy-MM-dd');
    }
     selectPlant(plant:any) {
         
         this.plants = [];
         if(plant.checked){
            this.plants.push(plant.plantId);
         }
       
        //  this.plants = this.plantSelected ;
        //     this.plantSelected =  plant.plantDesc; 
        //         console.log( this.plantSelected );
        //         console.log(  plant.plantDesc );
    }

              private checkAllPlants(check: boolean): void {
                console.log('checking all to:', check);
                for (const plant of this.plants) {
                    plant.checked = check;
                }
            }
}

