import { Component, ViewEncapsulation } from '@angular/core';

export const PREFIX = '--';

@Component({
    selector: '<sass-helper>',
    template: `<div></div>`,
    styleUrls: ['./sass-helper.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class SassHelper {
    constructor () {}

    // Read the custom property of body section with given name:
    readProperty(name: string): string {
        let sassHelperElement = document.getElementsByTagName("sass-helper")[0];
        let sassHelperStyles = window.getComputedStyle(sassHelperElement);
        let sassVarValue = sassHelperStyles.getPropertyValue(PREFIX + name);

        return sassVarValue;
    }
}