import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CmxDialogV3 } from "@cemex/cmx-dialog-v3/dist";
import { DriversCoreService } from '../../shared/services/drivers-core.service';
import { LegalDocumentsDialogComponent } from '../../shared/components/legal-documents-dialog/legal-documents-dialog.component';
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { DriversConstants } from '../../shared/constants/drivers.constants';

let moment = require('moment');

@Component({
    selector: 'app-terms',
    templateUrl: './terms.component.html',
    styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

    @ViewChild('legalDocumentGU') 
    private legalDocumentGU: LegalDocumentsDialogComponent;

    private Subscriptions = [];

    private termsTitle: string = 'label.legal.terms_and_conditions'; 
    private termVersionItem: any;

    constructor(private t: TranslationService, private core: DriversCoreService, private router: Router, private activatedRoute: ActivatedRoute) { 
        this.activatedRoute.queryParams.subscribe(params => {
            this.showTermsDialog();
        });
    }

    ngOnInit() {

    }

    public showTermsDialog() {
        this.Subscriptions.push(this.core.getDocumentsClientHTML(DriversConstants.typeTerms).subscribe((terms) => {
            // If terms have not been accepted, show modal.
            if(!terms.acceptTerm) {
                this.termVersionItem = terms.TermVersionItem;
                this.legalDocumentGU.initDialog(this.termVersionItem, this.termsTitle);
            }
            // If not, redirect to app.
            else {
                this.router.navigate(['app']);
            }
      }));
    }

    public acceptTermsDialog() {
        let accepted = {
              dateTime: moment.utc().format(),
              acceptTerm: true,
              deviceCode: DriversConstants.deviceCode,
              platformCode: DriversConstants.platformCode,
              termVersionId: this.termVersionItem.termVersionId
        }

        this.core.acceptTerms(accepted).subscribe(accept => {
              if (accept.status == 200) {
                this.router.navigate(['app']);
              } else {
                    //cant save the accepted terms...
                    this.router.navigate(['login']);
                    this.legalDocumentGU.closeDialog();
              }
        })

        return false;
    }

    public cancelTermsDialog() {
        this.router.navigate(['login']);
        this.legalDocumentGU.closeDialog();
        return false;
    }

}