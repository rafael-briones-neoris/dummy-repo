import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TermsComponent } from './terms.component';
import { CmxCoreCommonModule } from '@cemex-core/angular-services-v2/dist';
import { CmxLoginModule } from '@cemex/cmx-login-v1/dist';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CmxNavHeaderModule } from '@cemex/cmx-nav-header-v1';
import { DriversSharedModule } from '../../shared/components/drivers-shared.module';
import { SearchSortService } from '../../shared/services/heap-sort.service';
import { DriversCoreService } from '../../shared/services/drivers-core.service';


const ROUTES: Routes = [
    {
        path: '', component: TermsComponent,
    }
];

@NgModule({
    imports: [
        FlexLayoutModule,
        ReactiveFormsModule,
        CommonModule,
        RouterModule.forChild(ROUTES),
        CmxCoreCommonModule,
        CmxNavHeaderModule,
        DriversSharedModule,
    ],
    declarations: [
        TermsComponent,
    ],
    providers: [
        DriversCoreService, 
        SearchSortService,
    ],
    entryComponents: []
})
export class TermsModule { }

