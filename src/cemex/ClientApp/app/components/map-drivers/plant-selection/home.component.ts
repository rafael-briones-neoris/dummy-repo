import { Component, Input, ViewChild, OnInit, PipeTransform, Pipe, Inject } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { userDataDTO } from "@cemex-core/types-v1/dist";
import { PlantService } from "./../../../shared/services/api/PlantService";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { DriversSharedModule } from "./../../../shared/components/index";
import { SorterCompareFn, SearchFilterFn } from '../../../shared/components/cemex-filter-content/filter-sorter.component';

@Component({
    selector: "home-page",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.scss"],

})
export class HomeComponent implements OnInit {

    plantsSearch: any[];
    plantsForFilters: any[] = [];
    userData: userDataDTO;
    CssClass = "plantitemActive";
    CssClassi = "plantitemInactive";
    bottonsdisable: boolean = true;
    value: string = "";
    isCheckAll: boolean = false;
    isAZ: boolean = true;
    isDisable: boolean = true;
    filtericonplant: boolean = false;

    private sorterPlantsFn: SorterCompareFn = function (objectLeft: any, objectRight: any): number {
        if (objectLeft.plantDesc < objectRight.plantDesc) {
            return -1;
        } else if (objectLeft.plantDesc > objectRight.plantDesc) {
            return 1;
        }
        return 0;
    }

    private searchPlantsFn: SearchFilterFn = function (plants: any[], searchValue: string): any[] {
        return plants.filter((plant) => {
            return plant.plantDesc.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0;
        });
    };
    

    constructor(
        public svrplants: PlantService,
        private router: Router,
        private t: TranslationService) {
    }

    // tslint:disable-next-line:typedef
    ngOnInit() {

        let jsonStr: string = sessionStorage["userInfo"];
        let userInfo: any = JSON.parse(jsonStr);

        this.svrplants.getAllPlants().then((data) => {
            this.userData = data
            sessionStorage.setItem("userData", JSON.stringify(data));
            this.userData.plants.forEach((obj) => {
                //  obj.selected = false;
                obj['plantAddress'] = `${obj.address.streetName}, ${obj.address.cityDesc}, ${obj.address.regionCode}, ${obj.address.postalCode}`;
                // obj.businessLine.businessLineDesc= "ready mix";
            });
            this.plantsSearch = this.userData.plants;
            this.plantsForFilters = this.plantsSearch;
            this.isCheckAll = false;
            let isall: boolean = true;
            for (var plants of this.plantsSearch) {
                if (plants.monitored) {
                    this.isCheckAll = true;
                    this.isDisable = false;
                } else {
                    isall = false;
                }
            }
            if (!isall) {
                this.isCheckAll = false;
            }
        }
        ).catch((error) => {
            console.error("Error message", error.err);
        });
    }
    // tslint:disable-next-line:typedef
    public addPlants() {
        if (this.isDisable) {
            return;
        }
        this.userData.plants = this.plantsSearch;
        sessionStorage.setItem("userData", JSON.stringify(this.userData));


        this.svrplants.updateMonitoredPlants(this.userData).subscribe((data) => {
            if (data.status == "200") {
                this.router.navigate(["app/map/tracking"]);
                return true;
            }
        });
    }

    // tslint:disable-next-line:typedef
    public allChecked(event) {
        this.plantsSearch.forEach(x => x.monitored = event);
        this.isCheckAll = event;
        this.isDisable = !event;
    }

    // tslint:disable-next-line:typedef
    public checkPlant(plant, event) {
        plant.monitored = event;
        this.isDisable = true;
        let isall: boolean = true;
        for (var plants of this.plantsSearch) {
            if (plants.monitored) {
                this.isCheckAll = true;
                this.isDisable = false;
            } else {
                isall = false;
            }
        }
        if (!isall) {
            this.isCheckAll = false;
        }

    }

    // tslint:disable-next-line:typedef
    public getFilterData(data) {
        this.plantsSearch = data;
        let isall: boolean = true;
        for (var plants of this.plantsSearch) {
            if (plants.monitored) {
                this.isCheckAll = true;
                this.isDisable = false;
            } else {
                isall = false;
            }
        }
        if (!isall) {
            this.isCheckAll = false;
        }
    }

    public isFiltered(event) {
        this.filtericonplant = event;
    }
}