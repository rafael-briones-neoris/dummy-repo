import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';

import { CmxMapModule } from '@cemex/cmx-map-v1/dist';
import { CmxDropdownViewModule } from '@cemex/cmx-dropdown-view-v1/dist';
import { CmxTabsModule } from '@cemex/cmx-tabs-v1/dist';
import { CmxListModule } from '@cemex/cmx-list-v1/dist';

import { HomeComponent } from './plant-selection/home.component';
import {
    TrackingMapComponent, TruckFilterComponent,
    TruckDetailComponent, SearchMapComponent
} from './tracking-map/index';
import { DriversSharedModule } from '../../shared/components/index';
import { StatusTruckPipe } from '../../shared/pipes/truck-status.pipe';

import { FilterPlants } from './../../shared/pipes/plants-filter';
import { PointInterestService, SearchMapService } from './../../shared/services';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CmxInputAutoCompleteModule } from '../../shared/components/cmx-input-autocomplete';

const ROUTES: Routes = [
    { path: '', redirectTo: 'plant-selection', pathMatch: 'full' },
    { path: 'plant-selection', component: HomeComponent },
    { path: 'tracking', component: TrackingMapComponent }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTES),
        DriversSharedModule,
        CmxMapModule.forRoot(),
        FlexLayoutModule,
        CmxDropdownViewModule,
        CmxTabsModule,
        CmxListModule,
        CmxInputAutoCompleteModule
    ],
    declarations: [
        HomeComponent,
        TrackingMapComponent,
        TruckFilterComponent,
        TruckDetailComponent,
        StatusTruckPipe,
        SearchMapComponent
    ],
    providers: [
        PointInterestService,
        SearchMapService
    ],
    entryComponents: []
})
export class MapDriversModule { }
