import {
    Component, OnInit, Input, Output,
    EventEmitter, OnChanges, SimpleChanges, ViewChild,
    ChangeDetectorRef
} from '@angular/core';
import * as moment from 'moment';
import { sprintf } from "sprintf-js";
import { Subscription } from 'rxjs/Subscription';

import { CmxMapComponent, MAP_STYLES } from '@cemex/cmx-map-v1/dist';
import { TranslationService } from '@cemex-core/angular-services-v2/dist';
import { DriversCoreService } from '../../../../shared/services';
import { InfoBuilder } from './../tracking-map.utils';
import { ICacheAlert } from './../../../../shared/types/index.interface'
import { error } from 'util';
import { SimpleModalComponent, ISimpleDialogResult } from '../../../../shared/components';

export interface ITicketData {
    ticketId: number;
    ticketObject: any;
}

interface SimpleDict {
    [simpleKey: string]: string;
}

@Component({
    selector: 'truck-detail',
    styleUrls: ['truck-detail.component.scss', '../../../../scss/_drivers.scss'],
    templateUrl: './truck-detail.component.html',
})
export class TruckDetailComponent implements OnInit, OnChanges {
    private _truckDetail: any;
    private _ticket: any = null;
    private _ticketStatusList: any[];
    private _ticketDict: {
        [ticketCode: string]: ITicketData;
    } = {};

    private _ticketList: string[] = [];
    private _businessLine: string;
    private _currentMarker: google.maps.Marker = null;
    private _mapComponent: CmxMapComponent;
    private _allAlerts: ICacheAlert = {};
    private _showUnlinkModal: boolean = false;
    private _messageModal: string = '';
    private _titleModal: string = '';
    @ViewChild(SimpleModalComponent)
    private _modalComponent: SimpleModalComponent;
    private _maxStatusLog: string;
    private _ticketProducts:any[] = [];

    constructor(
        private _driversCore: DriversCoreService,
        private _translation: TranslationService,
        private cdRef: ChangeDetectorRef
    ) {
        this._businessLine = _driversCore.getBusinessLine();
        _driversCore.allAlertsObservable.subscribe(result => {
            this._allAlerts = result;
        });
    }

    private get ticketAlerts(): any[] {
        if (this._ticket) {
            return this._ticket.ticketAlerts;
        }

        return [];
    }

    @Input()
    public set truckDetail(value: any) {
        this._truckDetail = value;
    }

    public get plantName(): string {
        if (!this._truckDetail || this._truckDetail.event === undefined) {
            return undefined;
        }

        const allPlants = this._driversCore.plants;
        const tmpPlants = allPlants.filter(elem => {
            return elem.plantId = this._truckDetail.event.plant.plantId;
        });
        return tmpPlants.length > 0 ? tmpPlants[0].plantDesc : undefined;
    }

    public get deliveryPointAddress(): string {
        if (!this._ticket || this._ticket.order === undefined || this._ticket.order.jobsite === undefined) {
            return undefined;
        }
        const loc = this._ticket.order.jobsite.address
        return `${loc.streetName} ${loc.domicileNum}, ${loc.settlementDesc} ${loc.cityDesc} ${loc.regionCode}, ${loc.postalCode} ${loc.countryCode.toUpperCase()}`;
    }

    public get statusLogList(): string[] {
        if (this._ticket && this._ticket.statusLogDict !== undefined) {
            return Object.keys(this._ticket.statusLogDict);
        }

        return [];
    }

    public ngOnInit(): void {

    }

    private onMapReady(mapComponent: CmxMapComponent) {
        const mapOptions = {
            styles: MAP_STYLES,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
        };

        mapComponent.mapInstance.setOptions(mapOptions);

        //mapComponent.centerCurrentLocation();
        this._mapComponent = mapComponent;
        if (this._truckDetail) {
            this.updateMarkerMap(this._truckDetail);
            this._mapComponent.mapInstance.setCenter(this._currentMarker.getPosition());
        }

    }

    public ngOnChanges(changes: SimpleChanges) {
        if (changes['truckDetail'].currentValue) {
            this.clearTicketData();
            const localTruck = changes['truckDetail'].currentValue;
            this.updateMarkerMap(localTruck);
            this.initTicketTruck(localTruck);
        }
    }

    private clearTicketData(): void {
        this._ticketDict = {};
        this._ticket = null;
        this._ticketList = [];
        this._ticketProducts = [];
    }

    private initTicketTruck(truck: any): void {
        if (truck.event !== undefined && truck.event.ticket !== undefined) {
            const ticketId = truck.event.ticket.ticketId;
            this.setTicketData(ticketId, true);
        }
    }

    private getTicketItem(ticketAPI: any): any {
        const ticketItem: any[] = ticketAPI.ticketItem;
        if (ticketItem !== undefined && ticketItem.length > 0) {
            return ticketItem[0];
        }

        return null;
    }

    private setTicketData(ticketId: number, isConsolidation: boolean): void {
        this._driversCore.getTicket(ticketId).subscribe(result => {
            const localTicket = result.json();
            this._ticketStatusList = localTicket.ticketStatusLogs;
            // adding field statusLogDict
            localTicket.statusLogDict = this.buildMapLogStatus(localTicket.ticketStatusLogs);
            //Object.keys(localTicket.statusLogList);
            this.findMaxStatusTime(localTicket.statusLogDict);
            // adding field ticketAlerts
            localTicket.ticketAlerts = this.getAlertsByTicket(localTicket.ticketId);
            // adding fields of volume & unit code
            /*const ticketItem = this.getTicketItem(localTicket);
            if (ticketItem) {
                localTicket.volume = ticketItem.deliveryQuantity;
                localTicket.unitCode = ticketItem.unit.unitCode;
            }
            */
           this._ticketProducts = localTicket.ticketItem;

            this._ticketDict[localTicket.ticketCode] = {
                ticketId: localTicket.ticketId,
                ticketObject: localTicket
            };
            this._ticketList = Object.keys(this._ticketDict);

            if (isConsolidation && localTicket.consolidationCode != undefined) {
                this._driversCore.getConsolidationTicket(localTicket.consolidationCode).subscribe(
                    response => {
                        for (const ticketC of response.ticketsConsolidated) {
                            if (ticketC.ticketId !== ticketId) {
                                this._ticketDict[ticketC.ticketCode] = {
                                    ticketId: ticketC.ticketId,
                                    ticketObject: undefined
                                }
                            }
                        }

                        this._ticketList = Object.keys(this._ticketDict);
                    }
                );
            }

            this._ticket = localTicket;
        });
    }

    private onClickTicket(ticketCode: string): void {
        if (this._ticketDict[ticketCode] !== undefined && this._ticketDict[ticketCode].ticketObject === undefined) {
            const ticketId = this._ticketDict[ticketCode].ticketId;
            this._ticket = undefined;
            this._ticketProducts = [];
            this.setTicketData(ticketId, false);
        } else {
            this._ticket = this._ticketDict[ticketCode].ticketObject;
        }
    }

    private addTruckMarker(truck: any): google.maps.Marker {
        const marker = new google.maps.Marker({
            position: { lat: parseFloat(truck.latitude), lng: parseFloat(truck.longitude) },
            icon: InfoBuilder.truckIcon(truck, this._driversCore.typeTruck(this._truckDetail)),
        });

        return marker;
    }


    private updateMarkerMap(truck: any): void {
        if (this._currentMarker) {
            this._currentMarker.setMap(null);
            this._currentMarker = null;
        }

        if (this._mapComponent && this._mapComponent.mapInstance !== undefined) {
            this._currentMarker = this.addTruckMarker(truck);
            this._currentMarker.setMap(this._mapComponent.mapInstance);
            google.maps.event.addListenerOnce(this._mapComponent.mapInstance, 'resize', (event) => {
                this._mapComponent.mapInstance.setZoom(15);
                this._mapComponent.setCenter({
                    latitude: this._currentMarker.getPosition().lat(),
                    longitude: this._currentMarker.getPosition().lng()
                });

            });
            google.maps.event.trigger(this._mapComponent.mapInstance, 'resize');

        }
    }

    private buildMapLogStatus(statusLogList: any[]): SimpleDict {

        const result: SimpleDict = {};
        for (const aStatusLog of statusLogList) {
            if (aStatusLog.time !== undefined) {
                const str: string = aStatusLog.time;
                const tokens = str.split('T');
                result[aStatusLog.statusLog] = tokens.length > 1 ? tokens[1] : 'error';
            } else {
                result[aStatusLog.statusLog] = '-';
            }
        }
        /*
        for (const aLog of this._readyMixStatuses) {
            const tmpStatusList = statusLogList.filter(aStatus => {
                return aStatus.statusLog === aLog && aStatus.time !== undefined;
            });

            if (tmpStatusList.length > 0) {
                const statusLog = tmpStatusList[0];
                const str: string = statusLog.time;
                const tokens = str.split('T');
                result[aLog] = tokens.length > 1 ? tokens[1] : 'error';
            } else {
                result[aLog] = '-';
            }
        }*/

        return result;
    }

    private getStatusTime(statusLog: string): string {
        return this._ticket && this._ticket.statusLogDict !== undefined ? this._ticket.statusLogDict[statusLog] : undefined;
    }

    private getStatusTranslation(status: string): string{
        status = status.replace(/\s/g, '_');
        status = status.toLowerCase();
        status = "views.progress-chart." + status;
        return this._translation.pt(status);
    }

    private getAlertsByTicket(ticketId: number): any[] {
        const alertKeys = Object.keys(this._allAlerts);
        const result: any[] = [];
        for (const alertKey of alertKeys) {
            const alertItem = this._allAlerts[alertKey];
            if (alertItem && alertItem.ticket !== undefined && alertItem.ticket.ticketId === ticketId) {
                result.push(alertItem);
            }
        }

        return result;
    }

    private formatDateTime(value: string): string {
        return moment.utc(value).format("MMM DD,YYYY - HH:mm");
    }

    private tabLocationActive(active: boolean): void {
        if (active && this._mapComponent !== undefined) {
            this.cdRef.detectChanges();
            google.maps.event.trigger(this._mapComponent.mapInstance, 'resize');

        }
    }

    private unlikDriver(): void {
        const cancelLabel = this._translation.pt('map.tracking.ticket-detail.button.cancel');
        const confirmLabel = this._translation.pt('map.tracking.ticket-detail.button.confirm');
        const okLabel = this._translation.pt('map.tracking.ticket-detail.button.ok');
        this._modalComponent.buttonOptions = {}
        this._modalComponent.buttonOptions[cancelLabel] = {
            color: "blue-inverse",
            value: false
        }

        this._modalComponent.buttonOptions[confirmLabel] = {
            color: "green",
            value: true
        }

        this._messageModal = this._translation.pt('map.tracking.ticket-detail.unlink.confirm');
        this._titleModal = this._translation.pt('map.tracking.ticket-detail.unlink.title');

        const subscription = this._modalComponent.openModal().subscribe(
            (result: ISimpleDialogResult) => {
                if (result.value) {
                    this._modalComponent.buttonOptions = {}
                    this._modalComponent.buttonOptions[okLabel] = {
                        color: "green",
                        value: true
                    };

                    this._driversCore.unlinkTruckDriver(this._truckDetail.truckId).subscribe(
                        result => {
                            this._messageModal = sprintf(this._translation.pt('map.tracking.ticket-detail.unlink.success'), this._truckDetail.truckPlate);
                            this._modalComponent.openModal();
                        },
                        error => {
                            console.error('An exception has occurred at unlinking driver:', error);
                            this._messageModal = this._translation.pt('map.tracking.ticket-detail.unlink.fail');
                            this._modalComponent.openModal();
                        });

                }

                subscription.unsubscribe();
            }
        )


    }

    private resendTicket(): void {
        const cancelLabel = this._translation.pt('map.tracking.ticket-detail.button.cancel');
        const confirmLabel = this._translation.pt('map.tracking.ticket-detail.button.confirm');
        const okLabel = this._translation.pt('map.tracking.ticket-detail.button.ok');
        this._modalComponent.buttonOptions = {}
        this._modalComponent.buttonOptions[cancelLabel] = {
            color: "blue-inverse",
            value: false
        }

        this._modalComponent.buttonOptions[confirmLabel] = {
            color: "green",
            value: true
        }

        this._messageModal = this._translation.pt('map.tracking.ticket-detail.resend.confirm');
        this._titleModal = this._translation.pt('map.tracking.ticket-detail.resend.title');
        const subscription = this._modalComponent.openModal().subscribe(
            (result: ISimpleDialogResult) => {
                if (result.value) {
                    this._modalComponent.buttonOptions = {}
                    this._modalComponent.buttonOptions[okLabel] = {
                        color: "green",
                        value: true
                    };

                    const timestamp = new Date().toISOString().substr(0, 19);
                    const ticketId = this._ticket.ticketId;
                    const driverId = this._truckDetail.event.driver.driverId;
                    this._driversCore.resendTicket({
                        driverId: driverId,
                        ticketId: ticketId,
                        timestamp: timestamp
                    }).subscribe(
                        result => {
                            this._messageModal = sprintf(this._translation.pt('map.tracking.ticket-detail.resend.success'), this._truckDetail.truckPlate);
                            this._modalComponent.openModal();
                        },
                        error => {
                            this._messageModal = sprintf(this._translation.pt('map.tracking.ticket-detail.resend.fail'), this._truckDetail.truckPlate);
                            this._modalComponent.openModal();
                        }
                        );
                }
                subscription.unsubscribe();
            }
        );
    }

    private findMaxStatusTime(statusLogDict: SimpleDict): void {
        let maxTime = '';
        const arrStatus = Object.keys(statusLogDict);
        for (const statusLog of arrStatus) {
            const localTime = statusLogDict[statusLog];
            if (localTime !== '-' && localTime > maxTime) {
                maxTime = localTime;
                this._maxStatusLog = statusLog;
            }
        }
    }

    private getClassStatusLog(statusLog: string): string {
        if (this._ticket) {
            const valueStatusLog = this._ticket.statusLogDict[statusLog];
            if (valueStatusLog !== '-') {
                if (statusLog === this._maxStatusLog) {
                    return 'current-status';
                } else {
                    return 'done-status';
                }
            }

            return 'pending-status';
        }

        return 'pending-status';
    }

    private isEmptyTicket(): boolean {
        return !this._ticket || this._ticketList.length === 0;
    }

}