import { IMapDictionary } from '@cemex/cmx-map-v1/dist';


export interface TruckStatus {
    name: string; // translation.pt
    value: number; // de aqui viene el status value del ETruckStatus original
}

export interface INumberTruckStatus {
    name: string;
    value: ETruckStatus;
    numberOfTrucks: string;
}

export const TRUCK_STATUSES: ITypeTruckFilter[] = [
    {
        name: 'sidebar.trucks.units',
        value: 1
    },
    {
        name: 'sidebar.trucks.outService',
        value: 2 
    },
    {
        name: 'sidebar.trucks.ticketed',
        value: 3
    },
    {
        name: 'sidebar.trucks.toJob',
        value: 4
    },
    {
        name: 'sidebar.trucks.onSite',
        value: 5
    },
    {
        name: 'sidebar.trucks.toPlant',
        value: 6
    },
    {
        name: 'sidebar.trucks.available',
        value: 7
    }
];

export enum ETypeFilter {
    BY_STATUS = 1,
    BY_PLANT = 2,
    BY_ORDER = 3,
    BY_CARRIER = 4,
    BY_BUSINESS = 5
}

export interface ITypeTruckFilter {
    name: string;
    value: number;
}

export enum ETruckStatus {
    ALL_UNITS = 1,
    OUT_OF_SERVICE = 2,
    TICKETED = 3,
    TO_JOB = 4,
    ON_SITE = 5,
    TO_PLANT = 6,
    AVAILABLE = 7
}

export const TYPE_FILTERS: ITypeTruckFilter[] = [
    {
        name: 'map.filter.by-status',
        value: ETypeFilter.BY_STATUS
    },
    {
        name: 'map.filter.by-plant',
        value: ETypeFilter.BY_PLANT
    },
    {
        name: 'map.filter.by-order',
        value: ETypeFilter.BY_ORDER
    },
    {
        name: 'map.filter.by-carrier',
        value: ETypeFilter.BY_CARRIER
    },
    {
        name: 'map.filter.by-business',
        value: ETypeFilter.BY_BUSINESS
    }
];

export class FilteredTrucks {
    constructor(public selectedStatus: any, public trucksDict: IMapDictionary) { }
}

export class TruckItem {
    constructor(public truck: any) { }
}