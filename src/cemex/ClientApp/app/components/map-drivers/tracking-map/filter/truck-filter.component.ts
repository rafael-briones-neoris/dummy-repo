import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { TranslationService } from '@cemex-core/angular-services-v2/dist'
import { IMapDictionary } from '@cemex/cmx-map-v1/dist';
import {
    ETruckStatus, ITypeTruckFilter, TYPE_FILTERS, TRUCK_STATUSES,
    ETypeFilter, INumberTruckStatus, FilteredTrucks, TruckItem
} from './filter.dto';
import { DriversCoreService, SearchSortService, DriversCompareFn } from '../../../../shared/services/index'
import { DriverOrder } from '../../../../shared/types/index.interface';
import { TruckStatus } from './filter.dto';

@Component({
    selector: 'truck-filter',
    styleUrls: ["truck-filter.component.scss"],
    templateUrl: "./truck-filter.component.html",
})
export class TruckFilterComponent implements OnInit {
    private truckStatuses: TruckStatus[] = [];

    private _ETruckStatus = ETruckStatus;
    private _truckStatusList: INumberTruckStatus[] = [];
    private _filteredTrucks: any[];
    private _typeFilterList = TYPE_FILTERS;
    private _activeTypeFilter: ITypeTruckFilter;
    private _activeTruckStatus: INumberTruckStatus = null;
    private _placeholderTypeFilter: string;
    private _placeholderTruckStatus: string = null;
    private _placeholderPlant: string;
    private _placeholderOrder: string;
    private _allPlants: any[];
    private _ETypeFilter = ETypeFilter;
    private _searchOrderId: string;
    private _orders: any[] = [];
    private _isFirstLoad = true;

    @Output()
    private onSelectedTrucksEvent: EventEmitter<FilteredTrucks> = new EventEmitter<FilteredTrucks>();
    @Output()
    private onSelectedTruckItem: EventEmitter<TruckItem> = new EventEmitter<TruckItem>();
    @Input()
    public allTrucks: any[];

    constructor(
        private translation: TranslationService,
        private _driversCore: DriversCoreService,
        private _heapservice: SearchSortService
    ) {
        this._activeTypeFilter = TYPE_FILTERS[0];
        this._allPlants = this._driversCore.monitoredPlants;

        this._driversCore.ordersSubject.subscribe(
            (result: DriverOrder[]) => {
                this._orders = result;
            }
        )

        this._placeholderTypeFilter = this._typeFilterList[0].name;
    }

    public ngOnInit(): void {
        this._placeholderPlant = this.translation.pt('map.filter.all-plants');
        this.truckStatuses = TRUCK_STATUSES;     
    }


    private compareTruck: DriversCompareFn = function (truckLeft: any, truckRight: any): boolean {
        return truckLeft.truckPlate > truckRight.truckPlate;
    }

    private selectTypeFilter(typeFilter: ITypeTruckFilter) {
        this._activeTypeFilter = typeFilter;
        this._placeholderTypeFilter = this._activeTypeFilter.name;//this.translation.pt(this._activeTypeFilter.name);
        this._filteredTrucks = this.allTrucks.slice(0, this.allTrucks.length);
        // set placeholders of "by status", "by plant" 
        this._placeholderPlant = this.translation.pt('map.filter.all-plants');
        this._placeholderTruckStatus = this._truckStatusList.length > 0 ? this._truckStatusList[0].name : '';
        this._placeholderOrder = this.translation.pt('map.filter.orderId');

        const mapDict: IMapDictionary = {};
        for (const truck of this._filteredTrucks) {
            mapDict[truck.truckId] = truck;
        }

        this.onSelectedTrucksEvent.emit({
            selectedStatus: this._activeTruckStatus.value,
            trucksDict: mapDict
        })
    }

    private getFilteredTrucksByStatus(selectedStatus: any, theTrucks: any[]): any[] {
        if (!theTrucks) {
            return [];
        }

        const filterstatus: number[] = [];
        let result: any[] = [];

        if (selectedStatus === ETruckStatus.OUT_OF_SERVICE) {
            filterstatus.push(11);
            filterstatus.push(12);
        } else if (selectedStatus === ETruckStatus.TICKETED) {
            filterstatus.push(2);
            filterstatus.push(3);
            filterstatus.push(4);
        } else if (selectedStatus === ETruckStatus.ON_SITE) {
            filterstatus.push(6);
            filterstatus.push(7);
            //if(!this.isthirdParty){
            filterstatus.push(8);
            //}
        } else if (selectedStatus === ETruckStatus.TO_PLANT) {
            filterstatus.push(9);
        } else if (selectedStatus === ETruckStatus.TO_JOB) {
            filterstatus.push(5);
        } else if (selectedStatus === ETruckStatus.AVAILABLE) {
            filterstatus.push(1);
            filterstatus.push(10);
        }

        if (selectedStatus === ETruckStatus.ALL_UNITS) {
            result = theTrucks.slice(0, theTrucks.length);
        } else {
            result = theTrucks.filter(item => {
                return filterstatus.indexOf(item.event.truckEvent.truckEventCode) > -1;
            });
        }

        return result;
    }


    private getLabelStatusTruck(statusValue: string, theTrucks: any[]): string {
        // const statusName = ETruckStatus[statusValue].toLowerCase().split("_").join(" ");
        //let statusName;
        //this.truckStatuses.forEach(($status) => {
        //    if($status.value.toString() == statusValue){
        //        statusName = $status.name
        //    }
        //})
        const numTrucks: number = this.getFilteredTrucksByStatus(+statusValue, theTrucks).length;
        return ` (${numTrucks})`;
        // return `${statusName} (${numTrucks})`;
    }

    private getTruckStatusKeys(): string[] {
        const tmpKeys = Object.keys(ETruckStatus);
        return tmpKeys.slice(0, tmpKeys.length / 2);
    }


    public ngOnChanges(changes: SimpleChanges) {

        if (changes['allTrucks'].currentValue && !this._isFirstLoad ) {
            const localTrucks = changes['allTrucks'].currentValue as any[];
            const truckStatusKeys = this.getTruckStatusKeys();
            this._truckStatusList = [];
            
            for (const key of truckStatusKeys) {
                const labelTruckStatus = this.getLabelStatusTruck(key, localTrucks);
                let statusName;
                this.truckStatuses.forEach(($status) => {
                   if($status.value.toString() == key){
                       statusName = $status.name
                   }
                });
                this._truckStatusList.push({
                    name: statusName,
                    value: +key,
                    numberOfTrucks: labelTruckStatus
                });
            }

            if (this._placeholderTruckStatus) {
                this._placeholderTruckStatus = this._placeholderTruckStatus
            } else {
                this._placeholderTruckStatus = this.translation.pt(this._truckStatusList[0].name);
            }
            if (!this._activeTruckStatus) {
                this._activeTruckStatus = this._truckStatusList[0];
            }

            this._filteredTrucks = this.getFilteredTrucksByStatus(this._activeTruckStatus.value, localTrucks);
            this._heapservice.heapSort(this._filteredTrucks, this.compareTruck);
        }

        this._isFirstLoad = false;
    }

    private onSelectTruckStatus(selectedStatus: INumberTruckStatus): void {
        this._filteredTrucks = this.getFilteredTrucksByStatus(selectedStatus.value, this.allTrucks);
        this._heapservice.heapSort(this._filteredTrucks, this.compareTruck);
        this._activeTruckStatus = selectedStatus;
        this._placeholderTruckStatus = this.translation.pt(this._activeTruckStatus.name);
        const mapDict: IMapDictionary = {};
        for (const truck of this._filteredTrucks) {
            mapDict[truck.truckId] = truck;
        }

        this.onSelectedTrucksEvent.emit({
            selectedStatus: selectedStatus.value,
            trucksDict: mapDict
        })
    }

    private isTrucksEmpty(): boolean {
        return this.allTrucks === undefined || (this.allTrucks && this.allTrucks.length === 0)
    }

    private onSelectTruckPlant(plant: any): void {
        this._filteredTrucks = this.getFilteredTrucksByPlant(plant);
        this._heapservice.heapSort(this._filteredTrucks, this.compareTruck);
        this._placeholderPlant = plant ? plant.plantDesc : this.translation.pt('map.filter.all-plants');
        const mapDict: IMapDictionary = {};
        for (const truck of this._filteredTrucks) {
            mapDict[truck.truckId] = truck;
        }

        this.onSelectedTrucksEvent.emit({
            selectedStatus: this._activeTruckStatus.value,
            trucksDict: mapDict
        })
    }

    private getFilteredTrucksByPlant(plantFilter: any): any[] {

        if (plantFilter) {
            const filteredTrucks = this.allTrucks.filter(truck => {
                return truck.event && truck.event.plant && truck.event.plant.plantId === plantFilter.plantId;
            });

            return filteredTrucks;
        }

        return this.allTrucks.slice(0, this.allTrucks.length);
    }

    private onSelectTruckOrder(order: DriverOrder): void {
        this._filteredTrucks = this.getFilteredTrucksByOrder(order);
        this._heapservice.heapSort(this._filteredTrucks, this.compareTruck);
        this._placeholderOrder = order.orderCode;
        const mapDict: IMapDictionary = {};
        for (const truck of this._filteredTrucks) {
            mapDict[truck.truckId] = truck;
        }

        this.onSelectedTrucksEvent.emit({
            selectedStatus: this._activeTruckStatus.value,
            trucksDict: mapDict
        })
    }

    private getFilteredTrucksByOrder(orderFilter: DriverOrder): any[] {
        const filteredTrucks = this.allTrucks.filter(truck => {
            return truck.event != undefined && truck.event.ticket != undefined &&
                truck.event.ticket.order != undefined && truck.event.ticket.order.orderId === orderFilter.orderId;
        });

        return filteredTrucks;
    }

    private onSelectedTruckClick(truck: any): void {
        this.onSelectedTruckItem.emit(new TruckItem(truck));
    }
}