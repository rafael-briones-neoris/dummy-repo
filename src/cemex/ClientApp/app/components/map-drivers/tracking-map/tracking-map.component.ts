import {
    Component, OnInit, ViewChild, Renderer,
    ChangeDetectorRef
} from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/delay';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import * as moment from 'moment';
import * as HttpStatus from 'http-status-codes'

import { TranslationService, SessionService } from '@cemex-core/angular-services-v2/dist';
import { CmxMapComponent, ApiLoaderService, MAP_STYLES } from '@cemex/cmx-map-v1/dist';
import { DriversCoreService, PointInterestService, SearchSortService, DriversCompareFn } from '../../../shared/services';
import {
    IDriverApiResponse, ITruckList, IOriginDestinationTruck,
    IChangeByTruck,
    ESearchMapType
} from '../../../shared/types/index.interface';
import {
    MarkerItem, IMarkerDictionary, ETruckStatus,
    EMarkerType, IFilterMarker
} from './tracking-map.dto';
import { InfoBuilder } from './tracking-map.utils';
import { FilteredTrucks } from './filter/filter.dto';
import { IPointInterest, IChangesTruck, EStatusChange } from '../../../shared/types/index.interface';
import { ITicketData } from "./index";
import { error } from "util";
import { TimeoutError } from "rxjs/util/TimeoutError";
import { ISearchElement } from '../../../shared/components/cmx-input-autocomplete'
import { DriversConstants } from "../../../shared/constants/drivers.constants";
import { ParametersService } from "../../../shared/services/parameters.service";

const MarkerClusterer = require('node-js-marker-clusterer');

export enum ETrackingTab {
    TRACKING_MAP,
    TRUCK_DETAIL
}

@Component({
    styleUrls: ['tracking-map.component.scss', '../../../scss/_drivers.scss'],
    templateUrl: './tracking-map.component.html',
})
export class TrackingMapComponent implements OnInit {

    private _subscription: Subscription = null;
    private _driverMap: CmxMapComponent;
    private _dictTrucks: IMarkerDictionary = {};
    private _dictPoI: IMarkerDictionary = {};
    private _dictPlant: IMarkerDictionary = {};
    private _infoWindow: google.maps.InfoWindow;
    private _directionsToTruckDisplay: google.maps.DirectionsRenderer;
    private _directionsFromTruckDisplay: google.maps.DirectionsRenderer;

    private _selectedTruck: any = null; // Current selected truck
    private _selectedPlant: any = null; // Current selected plant
    private _businessLine: string;

    private _trucksClusterer: any;
    private _plants: any[];
    private _poiList: IPointInterest[];
    private _trucks: any[] = [];
    private _checkAllPlant: boolean = true;

    private _poIMarkerList: google.maps.Marker[] = [];
    private _currentMarker: {
        typeMarker: EMarkerType;
        marker: google.maps.Marker;
    }
    private _hidePlantPoIMarkers = true;
    private _checkPlants: IFilterMarker[] = [];
    private _activeTab: ETrackingTab = ETrackingTab.TRACKING_MAP;
    private _ETrackingTab = ETrackingTab;
    private _truckDetail: any;
    private _isThirdParty: boolean;
    private _isSecureProtocol: boolean = false;
    private _isAlreadyCentered: boolean = false;
    private comparePlant: DriversCompareFn = function (plantLeft: any, plantRight: any): boolean {
        return plantLeft.plantId > plantRight.plantId;
    }

    private comparePoI: DriversCompareFn = function (pointILeft: any, pointIRight: any): boolean {
        return pointILeft.pointId > pointIRight.pointId;
    }

    constructor(
        private _driversCore: DriversCoreService,
        private _poIService: PointInterestService,
        private renderer: Renderer,
        private loaderService: ApiLoaderService,
        private translation: TranslationService,
        private _cdRef: ChangeDetectorRef,
        private _sessionService: SessionService,
        private _router: Router,
        private _searchSortService: SearchSortService,
        private _parametersService: ParametersService
    ) {
        this._businessLine = _driversCore.getBusinessLine();
        this._isThirdParty = _driversCore.isThirdParty;
        if (loaderService.isApiLoaded()) {
            this._infoWindow = new google.maps.InfoWindow();
            this._directionsToTruckDisplay = new google.maps.DirectionsRenderer();
        } else {
            loaderService.scriptConfig.subscribe(() => {
                this._infoWindow = new google.maps.InfoWindow();
                this._directionsToTruckDisplay = new google.maps.DirectionsRenderer();
            });
        }

    }

    public ngOnInit(): void {
        this._isSecureProtocol = window.location.protocol.indexOf('https') > -1;
    }

    private onMapReady(mapComponent: CmxMapComponent) {
        this._driverMap = mapComponent;
        this.initializeBindings();
        this.customizeMap();
    }

    ngOnDestroy() {
        if (this._subscription) {
            console.log("unsubscribing from driver core!!");
            this._subscription.unsubscribe();
        }
        this._driversCore.clearCache();
    }

    private initializeBindings(): void {
        this._driversCore.newLoad = true;
        const PULL_TRUCKS_TIME: number = Number(this._parametersService.get("DRIVER_CONSOLE_REFRESH_FREQUENCY_IN_MINUTES")) * 60000;

        // launch & get the subscription to get the trucks 
        this._subscription = this._driversCore.fetchAllTrucks()
            //retry if there is an error every 30 secs
            .retryWhen(errors => {
                return errors.mergeMap((error) => {
                    if (error instanceof TimeoutError) {
                        // handle error and subscription continues
                        this.handleTruckMapError(error);
                    } else if (error.status === HttpStatus.UNAUTHORIZED) {
                        // this stops the subscription
                        return Observable.throw(error);
                    } else {
                        // handle error and subscription continues
                        this.handleTruckMapError(error);
                    }
                    return Observable.of(error);
                })
                    .delay(PULL_TRUCKS_TIME)
            })
            .subscribe(
                data => {
                    // trucks are sorted by truckId from the core service
                    this._trucks = data.trucks;
                    const containsMarkers: boolean = Object.keys(this._dictTrucks).length > 0;
                    if (this._trucks.length === 0 && containsMarkers) {
                        this.deleteAllMarks();
                    } else if (this._trucks.length > 0 && !containsMarkers) {
                        this.drawingTruckMarkers(data);
                    } else {
                        this.drawingChangesTruckMarkers(data, containsMarkers);
                    }

                    if (this._driversCore.newLoad) {
                        this.centerMapWithTrucks();
                    }

                    this._driversCore.newLoad = false;
                },
                error => {
                    console.error(`console detecting error!!:`, error);
                    this.handleTruckMapError(error);
                });

        if (!this._isThirdParty) {
            this.drawingPlantMarkers();
            this.drawingPointInterest();
        }

    }

    private deleteAllMarks(): void {
        const keyTrucks = Object.keys(this._dictTrucks);
        for (var key of keyTrucks) {
            const marker: google.maps.Marker = this._dictTrucks[key];
            marker.setMap(null);
        }

        this._dictTrucks = {};
        if (this._trucksClusterer) {
            this._trucksClusterer.clearMarkers();
        }

    }

    private drawingTruckMarkers(truckList: ITruckList): void {
        this.addTrucksToMap(truckList.trucks);
    }

    private drawingChangesTruckMarkers(truckList: ITruckList, containsMarkers: boolean): void {
        const keyChanges: string[] = Object.keys(truckList.changedTrucks);
        if (keyChanges.length > 0 && containsMarkers) {
            for (let keyChange of keyChanges) {
                let truckChange = truckList.changedTrucks[keyChange].truck;
                switch (truckList.changedTrucks[keyChange].status) {
                    case EStatusChange.DELETED:
                        const marker: google.maps.Marker = this._dictTrucks[truckChange.truckId];
                        this._trucksClusterer.removeMarker(marker);
                        delete this._dictTrucks[keyChange];
                        break;
                    case EStatusChange.NEW:
                        const newMarker = this.addTruckMarker(truckChange);
                        this._dictTrucks[truckChange.truckId] = newMarker;
                        this._trucksClusterer.addMarker(newMarker);
                        break;
                    case EStatusChange.MODIFIED_LOCATION:
                        //remove old marker from cluster
                        const markerUpdte: google.maps.Marker = this._dictTrucks[truckChange.truckId];
                        const opacity = markerUpdte.getOpacity();
                        this._trucksClusterer.removeMarker(markerUpdte);
                        //add modified marker to cluster
                        const newMarkerUpdate = this.addTruckMarker(truckChange);
                        newMarkerUpdate.setOpacity(opacity);
                        this._dictTrucks[truckChange.truckId] = newMarkerUpdate;
                        this._trucksClusterer.addMarker(newMarkerUpdate);
                        break;
                }
            }
        }
    }

    private addTrucksToMap(trucks: any[]) {
        const truckMarkers = this.makeTruckMarkers(trucks) || [];
        this._trucksClusterer = new MarkerClusterer(this._driverMap.mapInstance, truckMarkers, {
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
            maxZoom: 18
        });

    }

    private makeTruckMarkers(trucks: any[]): google.maps.Marker[] {
        const markers: google.maps.Marker[] = []
        if (!trucks) {
            return markers;
        }

        for (const truck of trucks) {
            const marker = this.addTruckMarker(truck);
            markers.push(marker);
            this._dictTrucks[truck.truckId] = marker;
        }
        return markers;
    }

    private addTruckMarker(truck: any): google.maps.Marker {
        const marker = new google.maps.Marker({
            position: { lat: parseFloat(truck.latitude), lng: parseFloat(truck.longitude) },
            icon: InfoBuilder.truckIcon(truck, this._driversCore.typeTruck(truck)),
            opacity: 1
        });

        marker.addListener('click', () => {
            if (marker.getOpacity() === 1) {
                this._currentMarker = {
                    marker: marker,
                    typeMarker: EMarkerType.TRUCK
                }

                this.clearDirections();
                if (truck.event.truckEvent.truckEventCode == ETruckStatus.ToJobsite) {
                    this.showPath(truck, marker);
                    //this.showTruckInfoWithETA(truck, marker);
                }
                else {
                    this.showTruckInfo(truck, marker);
                }
            }
        });

        return marker;
    }

    private clearDirections(): void {
        this._directionsToTruckDisplay.set('directions', null);
        this._directionsFromTruckDisplay.set('directions', null);
    }

    private showTruckInfoWithETA(truck: any, marker: any): void {
        const matrixService = new google.maps.DistanceMatrixService();
        const originDestination: IOriginDestinationTruck = this._driversCore.getOriginDestinationTruck(truck);
        if (originDestination) {
            const origin = new google.maps.LatLng(originDestination.origin.lat, originDestination.origin.lng);
            const destination = new google.maps.LatLng(originDestination.destination.lat, originDestination.destination.lng);
            matrixService.getDistanceMatrix({
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING
            }, (response, status) => {
                if (status == google.maps.DistanceMatrixStatus.OK &&
                    response.rows[0].elements[0].status !== google.maps.DistanceMatrixElementStatus.ZERO_RESULTS) {
                    // ETA
                    const duration = response.rows[0].elements[0].duration.value;
                    const wait = response.rows[0].elements[0].duration.text;
                    if (duration) {
                        const eta = moment().add(duration, 'seconds').format('LTS');
                        // Set ETA to truck object
                        truck.event.ticket.order.eta = eta;
                        truck.event.ticket.order.wait = wait;
                    }

                    this.showTruckInfo(truck, marker, true);
                }
            });
        }
    }

    private showTruckInfo(truck: any, marker: google.maps.Marker, centerMarker?: boolean) {
        this._infoWindow.setContent(InfoBuilder.buildTruckInfoHTML(truck, this._businessLine, this.translation));
        this._infoWindow.open(this._driverMap.mapInstance, marker);
        this.onMarkerSelected(new MarkerItem(truck, EMarkerType.TRUCK));

        if (centerMarker) {
            this._driverMap.centerCurrentLocation([marker.getPosition()], 20).subscribe(
                result => { }
            );
        }
    }

    private onMarkerSelected(markerItem: MarkerItem) {
        // If unselect
        if (markerItem == null) {
            this._selectedTruck = null;
            // this.cdRef.detectChanges();
            return;
        }
        // If truck
        else if (markerItem.type == EMarkerType.TRUCK) {
            this._selectedTruck = markerItem.item;
            this._selectedTruck.event.plant.plantDesc = this.getPlantName(this._selectedTruck.event.plant.plantId);
            this._selectedTruck.event['deliveryQuantity'] = '';
            this._selectedTruck.event['batchedTime'] = '';
            if (this._selectedTruck.event.ticket) {
                this._driversCore.getTicket(this._selectedTruck.event.ticket.ticketId).subscribe(data => {
                    if (data && data.order !== undefined) {
                        const loc = data.order.jobsite.address;
                        this._selectedTruck.event.ticket['deliveryPoint'] = (loc.streetName + ' ' + loc.domicileNum + ', ' +
                            loc.settlementDesc + ' ' + loc.cityDesc + ' ' + loc.regionCode + ', ' + loc.postalCode + ' ' + loc.countryCode).toUpperCase();
                        this._selectedTruck.event.ticket['client'] = data.order.customer.customerDesc;
                        this._selectedTruck.event.deliveryQuantity = data.ticketItem[0].deliveryQuantity;
                        this._selectedTruck.event.batchedTime = data.batchedDateTime.split('T')[1];
                    }
                });
            }
        }
        // Else whatever other marker type
        else {
            this._selectedPlant = markerItem.type == EMarkerType.PLANT ? markerItem.item : this._selectedPlant;
        }
    }

    private getPlantName(plantId: number): string {
        let tmp: any[] = this._driversCore.plants.filter(plant => {
            return plant.plantId == plantId;
        });
        return tmp.length > 0 ? tmp[0].plantDesc : "";
    }

    private drawPathOnMap(originDestination: IOriginDestinationTruck, directionsRender: google.maps.DirectionsRenderer): void {
        const directionsService = new google.maps.DirectionsService();
        const origin = new google.maps.LatLng(originDestination.origin.lat, originDestination.origin.lng);
        const destination = new google.maps.LatLng(originDestination.destination.lat, originDestination.destination.lng);
        directionsService.route({
            origin: origin,
            destination: destination,
            avoidTolls: true,
            avoidHighways: false,
            travelMode: google.maps.TravelMode.DRIVING
        }, (response, status) => {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsRender.setDirections(response);
            } else {
                console.warn(`Route couldn't be shown, google services respond with ${status}`);
            }
        });
    }

    private showPath(truck: any, marker: any): void {
        //const directionsService = new google.maps.DirectionsService;
        const originDestination: IOriginDestinationTruck = this._driversCore.getOriginDestinationTruck(truck);
        if (originDestination) {

            const originDestinationToTruck: IOriginDestinationTruck = {
                origin: originDestination.origin,
                destination: {
                    lat: truck.latitude,
                    lng: truck.longitude
                }
            }

            const originDestinationFromTruck: IOriginDestinationTruck = {
                origin: {
                    lat: truck.latitude,
                    lng: truck.longitude
                },
                destination: originDestination.destination
            }

            this.drawPathOnMap(originDestinationToTruck, this._directionsToTruckDisplay);
            google.maps.event.addListenerOnce(this._driverMap.mapInstance, 'bounds_changed', () => {
                this.showTruckInfoWithETA(truck, marker);
            });

            this.drawPathOnMap(originDestinationFromTruck, this._directionsFromTruckDisplay);
        }
    }

    // Map customizations
    // Ugly JQuery and JS vanilla cause no other way to handle it
    // ===============================================================
    private customizeMap() {
        // Construct services
        this._directionsToTruckDisplay = new google.maps.DirectionsRenderer({
            map: this._driverMap.mapInstance,
            suppressMarkers: true,
            polylineOptions: {
                strokeColor: '#CCCCCC',
                strokeOpacity: 1,
                strokeWeight: 7
            }

        });

        this._directionsFromTruckDisplay = new google.maps.DirectionsRenderer({
            map: this._driverMap.mapInstance,
            suppressMarkers: true,
            polylineOptions: {
                strokeColor: '#00B3FF',
                strokeOpacity: 1,
                strokeWeight: 7
            }

        });


        google.maps.event.addListener(this._infoWindow, 'closeclick', () => {

            // Clear directions
            this.clearDirections();
        });

        google.maps.event.addListener(this._infoWindow, 'domready', ($event) => {
            // Reference to the DIV that wraps the bottom of infowindow
            // const iwOuter = $('.gm-style-iw');
            const iwOuter = document.querySelector('.gm-style-iw');
            const iwOuterDiv = document.querySelector('.gm-style-iw>div');
            this.renderer.setElementStyle(iwOuterDiv, 'overflow', 'hidden');
            // Remove prev as soon as possible
            iwOuter.previousElementSibling.remove();
            const iwParent = iwOuter.parentElement;
            this.renderer.setElementStyle(iwOuter, 'top', '50px');
            // Close button
            const closeDiv = document.querySelector('.gm-style-iw').nextElementSibling;
            this.renderer.setElementStyle(closeDiv, 'display', 'none');
            this.renderer.setElementStyle(iwOuter, 'visibility', 'visible')

            const buttonDetail = document.querySelector('.button-ghost');
            if (buttonDetail) {
                buttonDetail.addEventListener('click', () => {
                    switch (this._currentMarker.typeMarker) {
                        case EMarkerType.TRUCK: {
                            // this._infoWindow.close();
                            this.showTruckDetail(this._selectedTruck);
                            break;
                        }

                    }
                });
            }

            const buttonClose = document.querySelector('.close-popup');
            buttonClose.addEventListener('click', () => {
                this._infoWindow.close();
                this.clearDirections();
            });
        });

        /* Change markers on zoom */
        google.maps.event.addListener(this._driverMap.mapInstance, 'zoom_changed', () => {
            const zoom = this._driverMap.mapInstance.getZoom();
            const factor = 7;
            // iterate over markers and call setVisible
            if (zoom < factor && this._hidePlantPoIMarkers) {
                this._hidePlantPoIMarkers = false;
                this.setVisibilityMarkers(false)
            } else if (zoom >= factor && !this._hidePlantPoIMarkers) {
                this._hidePlantPoIMarkers = true;
                this.setVisibilityMarkers(true);
            }
        });

        const mapOptions = {
            styles: MAP_STYLES,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
        };

        this._driverMap.mapInstance.setOptions(mapOptions);
    }

    private setVisibilityMarkers(visible: boolean): void {
        for (const key in this._dictPoI) {
            this._dictPoI[key].setVisible(visible);
        }

        for (const key in this._dictPlant) {
            this._dictPlant[key].setVisible(visible);
        }
    }

    private centerMapWithTrucks(): void {
        const latlLngBounds: google.maps.LatLng[] = [];
        for (const key in this._dictTrucks) {
            const marker: google.maps.Marker = this._dictTrucks[key];
            latlLngBounds.push(marker.getPosition());
        }

        /*google.maps.event.addListenerOnce(this._driverMap.mapInstance, 'bounds_changed', (event) => {
            this._driverMap.centerCurrentLocation(latlLngBounds);
            this._isAlreadyCentered = true;
        });*/

        if (this._isSecureProtocol) {
            //this._driverMap.centerCurrentLocation();
            this._driverMap.centerCurrentLocation(latlLngBounds).subscribe(result => {
                this._isAlreadyCentered = true;
            });

        } else {
            this.centerMapWithPlants();
        }
    }

    private drawingPlantMarkers(): void {
        this._plants = this._searchSortService.heapSort(this._driversCore.monitoredPlants, this.comparePlant);
        for (let localPlant of this._plants) {
            const marker = this.addPlantMarker(localPlant);
            this._dictPlant[localPlant.plantId] = marker;
            this._checkPlants.push({
                checked: false,
                valueData: localPlant
            });
        }

        this.checkAllPlants(this._checkAllPlant);
    }

    private addPlantMarker(plant: any): google.maps.Marker {
        const marker = new google.maps.Marker({
            position: { lat: parseFloat(plant.address.geoPlace.latitude), lng: parseFloat(plant.address.geoPlace.longitude) },
            icon: InfoBuilder.plantIcon(plant),
            map: this._driverMap.mapInstance
        });

        marker.addListener('click', () => {
            this.showPlantInfo(plant, marker);
        });

        return marker;
    }

    private drawingPointInterest(): void {
        const countryCode = sessionStorage['country'];

        this._poIService.getPoIByCountry(countryCode).subscribe(
            data => {
                this._poiList = this._searchSortService.heapSort(data, this.comparePoI);
                this._driversCore.pointInterestList = this._poiList.slice(0, this._poiList.length);
                for (const poI of data) {
                    const markerPoI = this.poIMarker(poI);
                    markerPoI.setMap(this._driverMap.mapInstance);
                    this._dictPoI[poI.pointId] = markerPoI;
                    this._poIMarkerList.push(markerPoI);
                }

            }
        );
    }

    private poIMarker(poI: any): google.maps.Marker {
        const marker = new google.maps.Marker({
            position: { lat: parseFloat(poI.address.geoPlace.latitude), lng: parseFloat(poI.address.geoPlace.longitude) },
            icon: InfoBuilder.poiIcon()
        });

        marker.addListener('click', () => {
            this.showPoIInfo(poI, marker);
        });

        return marker;
    }

    private showPoIInfo(poI: any, marker: any) {
        this._infoWindow.setContent(InfoBuilder.buildPoIInfoHTML(poI));
        this._infoWindow.open(this._driverMap.mapInstance, marker);
    }

    private showTruckDetail(truck: any): void {
        this._activeTab = ETrackingTab.TRUCK_DETAIL;
        this._truckDetail = truck;
    }

    private showPlantInfo(plant: any, marker: any) {
        this._infoWindow.setContent(InfoBuilder.buildPlantInfoHTML(plant, this.translation));
        this._infoWindow.open(this._driverMap.mapInstance, marker);
    }

    private onSelectedTrucks(selectedTrucks: FilteredTrucks) {
        this.doTruckFilter(selectedTrucks);
    }

    private doTruckFilter(filterTruck: FilteredTrucks) {
        for (const localTruck of this._trucks) {
            const marker: google.maps.Marker = this._dictTrucks[localTruck.truckId];
            const mOpacity: number = filterTruck.trucksDict[localTruck.truckId] ? 1 : 0.25;
            marker.setOpacity(mOpacity);
        }
    }

    private checkAllPlants(check: boolean): void {
        for (const plant of this._checkPlants) {
            plant.checked = check;
        }

        this.doPlantFilter(this._checkPlants);
    }

    private checkPlant(check: boolean, plant: IFilterMarker): void {
        plant.checked = check;
        this.doPlantFilter([plant]);
    }

    private doPlantFilter(plantFilter: IFilterMarker[]): void {
        for (const filter of plantFilter) {
            const marker: google.maps.Marker = this._dictPlant[filter.valueData.plantId];
            const mOpacity = filter.checked ? 1 : 0.25;
            marker.setOpacity(mOpacity);
        }
    }

    private onSelectedTruckItem(event: any): void {
        this.showMarkerMap(EMarkerType.TRUCK, event.truck);
        this._selectedTruck = event.truck;

    }


    public showMarkerMap(type: EMarkerType, data: any) {
        const marker = this.getMarker(type, data);
        if (marker) {
            const latlLngBounds: google.maps.LatLng[] = [];
            const latLngMarker = marker.getPosition();
            // calculate new to fix lat in order to get center 
            // to show correctly the popup message
            const latMarker = latLngMarker.lat();
            const centerLat = latMarker + 0.0002;
            const latLngCenter = new google.maps.LatLng(centerLat, latLngMarker.lng());

            latlLngBounds.push(latLngCenter);
            this._driverMap.centerCurrentLocation(latlLngBounds, 20).subscribe(
                result => {
                    this.showMarkerPopup(type, marker, data);
                });


        } else {
            console.warn(`could not find marker!! ${data}`);
        }

    }

    private showMarkerPopup(type: EMarkerType, marker: google.maps.Marker, data: any) {
        const infoHTML: string = InfoBuilder.buildInfoHTML(type, data, this._businessLine, this.translation);
        const infoOptions: google.maps.InfoWindowOptions = {
            disableAutoPan: true
        }
        this._infoWindow.setOptions(infoOptions);
        this._infoWindow.setContent(infoHTML);
        this._infoWindow.open(this._driverMap.mapInstance, marker);

        if (type === EMarkerType.TRUCK)
            new google.maps.event.trigger(marker, 'click');

    }

    private getMarker(type: EMarkerType, data: any): google.maps.Marker {
        if (type === EMarkerType.PLANT) {
            return this._dictPlant[data.plantId];
        } else if (type === EMarkerType.POINT_INTEREST) {
            return this._dictPoI[data.pointId];
        } else if (type === EMarkerType.TRUCK) {
            return this._dictTrucks[data.truckId];
        }

        return null;
    }

    private onPoIClick(poI: IPointInterest): void {
        this.showMarkerMap(EMarkerType.POINT_INTEREST, poI);
    }

    private goToMap(): void {
        this._activeTab = ETrackingTab.TRACKING_MAP;
        this._cdRef.detectChanges();
        google.maps.event.addListenerOnce(this._driverMap.mapInstance, 'resize', (event) => {
            if (this._currentMarker) {
                this._driverMap.setCenter({
                    latitude: this._currentMarker.marker.getPosition().lat(),
                    longitude: this._currentMarker.marker.getPosition().lng()
                });
            }
        });

        google.maps.event.trigger(this._driverMap.mapInstance, 'resize');
    }

    private isActiveTab(tab: ETrackingTab): boolean {
        return this._activeTab === tab;
    }

    private buildSearchValuePoI(poI: any): string {
        return `${poI.pointName} ${poI.pointDesc}`;
    }

    private handleTruckLocationError(): void {
        if (this._trucks.length === 0 && !this._isAlreadyCentered) {

            if (this._driversCore.isThirdParty) {
                this._driverMap.centerCurrentLocation().subscribe(
                    result => {
                        this._isAlreadyCentered = true;
                    });
            } else {
                this.centerMapWithPlants();
            }

        }
    }

    private handleTruckMapError(error: Response | any): void {
        if (error instanceof TimeoutError) {
            console.debug('Catch timeout error:', error);
            this.handleTruckLocationError();
        } else if (error instanceof Response) {
            if (error.status === 401) {
                this._subscription.unsubscribe();
                this._sessionService.logout();
                this._router.navigate(['/login']);
            } else {
                this.handleTruckLocationError();
            }
        }
    }

    private onFetchResult(item: ISearchElement): void {
        const typeSearch: number = item.objectData.type;
        switch (typeSearch) {
            case ESearchMapType.TRUCK:
            case ESearchMapType.DRIVER:
            case ESearchMapType.ORDER:
                const localTruck = this.getTruckById(item.objectData.objectId);
                this.showMarkerMap(EMarkerType.TRUCK, localTruck);
                break;
            case ESearchMapType.PLANT:
                const localPlant = this.getPlantById(item.objectData.objectId);
                this.showMarkerMap(EMarkerType.PLANT, localPlant);
                break;
            case ESearchMapType.POI:
                const localPoI = this.getPoIById(item.objectData.objectId);
                this.showMarkerMap(EMarkerType.POINT_INTEREST, localPoI);
                break;
        }
    }

    private getObjectById(objectList: any[], objectId: number, objectField: string): any {
        const index = this._searchSortService.binaryIndexOf(objectList, objectId, objectField);
        if (index > -1) {
            return objectList[index];
        }

        return null;
    }
    private getTruckById(truckId: number): any {
        // trucks are sorted by truckId from core service
        return this.getObjectById(this._trucks, truckId, 'truckId');
    }

    private getPlantById(plantId: number): any {
        return this.getObjectById(this._plants, plantId, 'plantId');
    }

    private getPoIById(pointId: number): any {
        return this.getObjectById(this._poiList, pointId, 'pointId');
    }

    private centerMapWithPlants(): void {
        const localPlants = this._driversCore.monitoredPlants;
        if (localPlants.length > 0) {
            const localPlant = localPlants[0];
            const lat = parseFloat(localPlant.address.geoPlace.latitude);
            const lng = parseFloat(localPlant.address.geoPlace.longitude);
            this._driverMap.centerCurrentLocation([new google.maps.LatLng(lat, lng)]).subscribe(
                result => {

                });
        }
    }

}
