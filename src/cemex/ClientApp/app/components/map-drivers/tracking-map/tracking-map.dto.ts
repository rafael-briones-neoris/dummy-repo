
export enum ETruckStatus {
    ToPlant = 9,
    ToJobsite = 5,
    Downloading = 7
}

export enum EMarkerType {
    TRUCK = 0,
    PLANT = 1,
    POINT_INTEREST = 2
}

export class MarkerItem {
    item: any;
    type: EMarkerType;

    constructor(item: any, type: EMarkerType) {
        this.item = item;
        this.type = type;
    }
}

export interface IMarkerDictionary {
    [index: number]: google.maps.Marker;
}

export enum EStatusChange {
    NEW,
    MODIFIED,
    DELETED,
    NONE
}

export interface IFilterMarker {
    checked: boolean;
    valueData: any
}