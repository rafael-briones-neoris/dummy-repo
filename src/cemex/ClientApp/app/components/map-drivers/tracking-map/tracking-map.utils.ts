import { EMarkerType } from './tracking-map.dto';
import { MapConstants } from '../../../shared/constants/maps.constants';
import { Utils } from '../../../shared/helpers/utils';
import { TranslationService } from '@cemex-core/angular-services-v2/dist';

export class TruckHelper {
    static hasETA(truck: any): boolean {
        return truck.event.ticket
            && truck.event.ticket.order
            && truck.event.ticket.order.eta
            && truck.event.ticket.order.eta !== undefined;
    }

    static getTicket(truck: any) {
        return truck.event.ticket != undefined ? truck.event.ticket.ticketCode : '';
    }

    static getOrder(truck: any) {
        return truck.event.ticket != undefined && truck.event.ticket.order ? truck.event.ticket.order.orderCode : '';
    }
}

export class InfoBuilder {

    constructor(private t: TranslationService) { }

    static buildInfoHTML(type: EMarkerType, data: any, businessLine: string, translation: any): string {
        if (type == EMarkerType.PLANT) {
            return InfoBuilder.buildPlantInfoHTML(data, translation);
        }

        if (type == EMarkerType.POINT_INTEREST) {
            return InfoBuilder.buildPoIInfoHTML(data);
        }

        if (type == EMarkerType.TRUCK) {
            return InfoBuilder.buildTruckInfoHTML(data, businessLine, translation);
        }

        return "";
    }

    static buildTruckInfoHTML(truck: any, businessLine: string, translation: any): string {
        console.log('pritning truck data:', truck);
        const contentETA = TruckHelper.hasETA(truck) ? `
        <div class="section font-dark-grey">
        <div class="title">${translation.pt('map.tracking.infowindow.eta')}:</div>
        <div class="desc">${truck.event.ticket.order.eta}</div>
        </div>` : '';

        const content =
            `   <div class="content-infobox">
                    <span class="close-popup"></span>
                    <div class="header">
                        <label class="title"> ${translation.pt('map.tracking.infowindow.truck')} ${truck.truckPlate}</label>
                    </div>
                    <div class="infowindow-body">
                        <div class="section font-dark-grey">
                            <div class="title">${translation.pt('map.tracking.infowindow.status')}:</div>
                            <div class="desc">${truck.event.truckEvent.truckEventDesc}</div>
                        </div>
                        <div class="section font-dark-grey">
                            <div class="title">${translation.pt('map.tracking.infowindow.driver')}:</div>
                            <div class="tooltip-custom">
                                <div class="desc">${truck.event.driver.driverDesc.substring(0, 15)}</div>
                                <div class="tooltiptext">${truck.event.driver.driverDesc}
                                    <div class="arrow-down"></div>
                                </div>
                            </div>
                        </div>
                        ${contentETA}
                        <div class="section font-dark-grey">
                            <div class="title">${translation.pt('map.tracking.infowindow.ticket')}:</div>
                            <div class="desc">${TruckHelper.getTicket(truck)}</div>
                        </div>
                        <div class="section font-dark-grey">
                            <div class="title">${translation.pt('map.tracking.infowindow.order')}:</div>
                            <div class="desc">${TruckHelper.getOrder(truck)}</div>
                        </div>
                    </div>
                    <div class="button-container">
                        <button class="button-ghost">${translation.pt('map.tracking.infowindow.truck-details')}</button>
                    </div>
                    <div class="arrow-down"></div> 
                </div>
            `

        return content;
    }

    static buildPlantInfoHTML(plant: any, translation: any): string {
        const content =
            `<div class="content-infobox">
                <span class="close-popup"></span>    
                <div class="header">
                    <label class="title">${plant.plantDesc}</label>
                </div>
                <div class="infowindow-body">
                    <div class="section font-dark-grey">    
                        <div class="title">${translation.pt('views.plantselection.plantId')}:</div>
                        <div class="desc">${plant.plantId}</div>
                    </div>
                    <div class="section font-dark-grey">
                        <div class="title">${translation.pt('views.profile.filter_region')}:</div>
                        <div class="desc">${plant.address.regionCode}</div>
                    </div>
                    <div class="section font-dark-grey">
                        <div class="title">${translation.pt('views.delivery_info.address')}:</div>
                        <div class="desc">${Utils.getShortFormatAddress(translation, plant.address)}</div>
                    </div>
                </div>
                <div class="arrow-down"></div>
            </div>
            `
        return content;
    }

    static buildPoIInfoHTML(poI: any): string {
        const content =
            `<div class="content-infobox">
                <span class="close-popup"></span>
                <div class="header">
                    <label class="title">${poI.pointName}</label>
                </div>
                <div class="infowindow-body">
                    <div class="section font-dark-grey">                
                        <div class="desc">${poI.pointDesc}</div>
                    </div>
                </div>
                <div class="arrow-down"></div>
            </div>
            `
        return content;
    }

    static truckIcon(truck: any, typeTrucks: string) {
        let iconTruck = MapConstants.DictTruckIcon[typeTrucks];
        return iconTruck.icon;
    }

    static plantIcon(truck: any) {
        return MapConstants.ICONS.POD.icon;
    }

    static poiIcon() {
        return MapConstants.ICONS.POI.icon;
    }
}