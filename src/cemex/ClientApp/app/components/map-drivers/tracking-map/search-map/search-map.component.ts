import { Component, Output, EventEmitter } from '@angular/core';

import { TranslationService } from '@cemex-core/angular-services-v2/dist';
import { ISearchElement } from '../../../../shared/components/cmx-input-autocomplete'
import { SearchMapService } from '../../../../shared/services';
import { ESearchMapType } from './../../../../shared/types/index.interface';

@Component({
    selector: 'search-map',
    styleUrls: ['./search-map.component.scss', '../../../../scss/_drivers.scss'],
    templateUrl: './search-map.component.html'
})
export class SearchMapComponent {

    @Output()
    private fetchResult: EventEmitter<ISearchElement> = new EventEmitter<ISearchElement>();

    private _resultData: ISearchElement[] = [];
    private _searchValue: string;

    private _typeSearchDict: {
        [index: number]: string
    } = {};

    constructor(
        private _searchMapService: SearchMapService,
        private _translation: TranslationService) {
        this._typeSearchDict[ESearchMapType.TRUCK] = "map-search.type.truck";
        this._typeSearchDict[ESearchMapType.DRIVER] = "map-search.type.driver";
        this._typeSearchDict[ESearchMapType.ORDER] = "map-search.type.order";
        this._typeSearchDict[ESearchMapType.PLANT] = "map-search.type.plant";
        this._typeSearchDict[ESearchMapType.POI] = "map-search.type.poi";
    }

    private getSearchResults(data: ISearchElement[]): void {
        this._resultData = data;
    }

    private onSelectElementEvent(item: ISearchElement): void {
        console.log('printing selected item by enter key:', item);
        this.fetchResult.emit(item);
    }

    private onClickItem(item: ISearchElement): void {
        console.log('printing clicked item:', item);
        this._searchValue = item.displayData;
        this.fetchResult.emit(item);
    }

    private getTypeSearchLabel(value: string): string {
        return this._typeSearchDict !== undefined ? this._typeSearchDict[value] : undefined;
    }

}