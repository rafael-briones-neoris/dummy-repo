import { Component, Input } from '@angular/core';

import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { CmxMapComponent } from '@cemex/cmx-map-v1';
import { GenericPointNew } from '../_shared/generic-point-new.component';
import { MapConstants } from '../../../shared/constants/maps.constants';
import { GenericPointManager } from '../_shared/generic-point-manager.component';
import { Utils } from '../../../shared/helpers/utils';


@Component({
    selector: 'points-interest-edit',
    templateUrl: './edit-new/points-interest-edit-new.component.html',
    styleUrls: ['../_shared/plants-points-delivery-edit/plants-points-delivery-edit.component.scss', './edit-new/points-interest-edit-new.component.scss'],
    providers: [HttpCemex, TranslationService]
})

export class PointsInterestNewComponent extends GenericPointNew {

    protected _headerEditName: string = 'management.points.interest';
    protected _localStorageKey: string = 'lastEditedPointInterest';
    protected _editDialogDefinedTitle: string = 'management.saved.poi';
    protected _pointIconPath: string = MapConstants.ICONS.POI.icon;
    protected _showMap: boolean = false;

    private _labelName: string;
    private _labelDescription: string;

    validateData() {
        super.validateData();
        this._dataIsValid = this._labelName && this._labelName.length > 0 && this._labelDescription && this._labelDescription.length > 0 && this._showMap;
        this.changeDetectorRef.detectChanges();
    }

    getRequestBody() {
        let request = super.getRequestBody();
        if (this._lastSearchResult) {
            request = Utils.getLastSearchAddressValues(this._lastSearchResult, request);
            request.pointType = 1;
            request.pointName = this._labelName;
            request.pointDesc = this._labelDescription;
        }
        return request;
    }

    APIRequest(request: any) {
        return this.geofenceService.addPointInterestGeofence(request);
    }

}