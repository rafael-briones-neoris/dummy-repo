import { Component, Input } from '@angular/core';

import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { CmxMapComponent } from '@cemex/cmx-map-v1';
import { GenericPointEdit } from '../_shared/generic-point-edit.component';
import { MapConstants } from '../../../shared/constants/maps.constants';
import { Utils } from '../../../shared/helpers/utils';


@Component({
    selector: 'points-interest-edit',
    templateUrl: './edit-new/points-interest-edit-new.component.html',
    styleUrls: ['../_shared/plants-points-delivery-edit/plants-points-delivery-edit.component.scss', './edit-new/points-interest-edit-new.component.scss'],
    providers: [HttpCemex, TranslationService]
})

export class PointsInterestEditComponent extends GenericPointEdit {

    protected _headerEditName: string = null;
    protected _localStorageKey: string = 'lastEditedPointInterest';
    protected _editDialogDefinedTitle: string = 'management.saved.poi';
    protected _pointIconPath: string = MapConstants.ICONS.POI.icon;

    private _labelName: string;
    private _labelDescription: string;

    public APIRequest(request: any, pointInterestId: string) {
        return this.geofenceService.updatePointInterestGeofence(request, pointInterestId);
    }

    public setGenericPointDetails() {
        super.setGenericPointDetails();
        this.loadPoIDetails();
    }

    private loadPoIDetails() {
        this._labelName = this._genericPoint._dd_pointDesc;
        this._labelDescription = this._genericPoint.pointDesc;
    }

    getRequestBody() {
        let request = super.getRequestBody();
        if (this._lastSearchResult) {
            request = Utils.getLastSearchAddressValues(this._lastSearchResult, request);
        } else {
            request = this.getCurrentPointInterestValues(request);
        }
        request.pointType = 1;
        request.pointName = this._labelName;
        request.pointDesc = this._labelDescription;
        return request;
    }

    getCurrentPointInterestValues(partialRequest: any) {
        partialRequest.address.cityDesc = this._genericPoint.address.cityDesc;
        partialRequest.address.countryCode = this._genericPoint.address.countryCode;
        partialRequest.address.domicileNum = this._genericPoint.address.domicileNum;
        partialRequest.address.postalCode = this._genericPoint.address.postalCode;
        partialRequest.address.regionCode = this._genericPoint.address.regionCode;
        partialRequest.address.regionDesc = this._genericPoint.address.regionDesc;
        partialRequest.address.settlementDesc = this._genericPoint.address.settlementDesc;
        partialRequest.address.streetName = this._genericPoint.address.streetName;
        return partialRequest;
    }

}