import { Component, Input, ViewChild } from '@angular/core';

import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { PointInterestService } from "../../../shared/services/point-interest.service";
import { CmxMapComponent, ApiLoaderService, MAP_STYLES } from '@cemex/cmx-map-v1/dist';
import { ActivatedRoute, Router } from '@angular/router';
import { GeofencesService } from '../../../shared/services/geofence.service';
import { CmxDialogV3 } from '@cemex/cmx-dialog-v3';
import { Utils } from '../../../shared/helpers/utils';

@Component({
    selector: 'points-interest-list',
    templateUrl: './points-interest-list.component.html',
    styleUrls: ['./points-interest-list.component.scss'],
    providers: [HttpCemex, TranslationService, PointInterestService]
})

export class PointsInterestListComponent {

    @ViewChild('removeDialog')
    public _removeDialog: CmxDialogV3;
    public _selectedPoI: any;
    public listPoI = [];

    constructor(public POIService: PointInterestService, private t: TranslationService, private router: Router, private route: ActivatedRoute, private geofenceService: GeofencesService) {
        this.listPoI = [];
        this.getPOI();
    }

    private getPOI() {
        let jsonStr: string = sessionStorage["userData"];
        let userInfo: any = JSON.parse(jsonStr);
        let countryCode = userInfo.countryCode.trim();

        this.POIService.getPoIByCountry(countryCode).subscribe(PoIData => {
            this.listPoI = PoIData;
            this.listPoI.forEach(poi => {
                poi.complete = Utils.getStandardFormatAddress(this.t, poi.address); //this.IsUndefined(poi.address.streetName) + ',' + this.IsUndefined(poi.address.cityDesc) + ',' + this.IsUndefined(poi.address.regionCode) + ',' + this.IsUndefined(poi.address.countryCode)
            });
        })
    }

    private IsUndefined(item: string): string {
        let cad: string;
        cad = (item != undefined) ? item : "";
        return cad;
    }

    private deletedPoI(poi: any) {
        this._selectedPoI = poi;
        this.openModal();
    }

    private openModal() {
        this._removeDialog.open();
    }

    private acceptRemoval() {
        this.geofenceService.deletePointInterestGeofence(this._selectedPoI.pointId).subscribe(result => {
            if (!result)
                return;

            this.getPOI();
            this.closeModal();
        });
    }

    private cancelRemoval() {
        this._selectedPoI = null;
        this.closeModal();
    }

    private closeModal() {
        this._removeDialog.close();
    }

    private editRow(poi: any) {
        this.geofenceService.setPoint(poi, GeofencesService.eGenericPoint.POINT_INTEREST);
        this.router.navigate(['edit'], { relativeTo: this.route });
    }

    private addPoI() {
        this.router.navigate(['new'], { relativeTo: this.route });
    }
}