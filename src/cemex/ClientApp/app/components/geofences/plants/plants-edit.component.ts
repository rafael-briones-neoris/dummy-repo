import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { CmxMapComponent, ApiLoaderService, IPositionMap } from "@cemex/cmx-map-v1/dist";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { GenericPointEdit } from "../_shared/generic-point-edit.component";
import { MapConstants } from "../../../shared/constants/maps.constants";

@Component({
    selector: 'plants-edit',
    templateUrl: '../_shared/plants-points-delivery-edit/plants-points-delivery-edit.component.html',
    styleUrls: ['../_shared/plants-points-delivery-edit/plants-points-delivery-edit.component.scss']
})
export class PlantsEditComponent extends GenericPointEdit {

    protected _headerEditName: string = 'management.plants';
    protected _localStorageKey: string = 'lastEditedPlant';
    protected _editDialogDefinedTitle: string = 'management.saved.plant';
    protected _pointIconPath: string = MapConstants.ICONS.JOBSITE.icon;

    public APIRequest(request: any, plantId: string) {
        return this.geofenceService.updatePlantGeofence(request, plantId);
    }

}