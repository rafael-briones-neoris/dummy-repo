import { PlantService } from "./../../../shared/services/api/PlantService";
import { Component, Input, ViewChild, OnInit, PipeTransform, Pipe, Inject } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { GeofencesService } from "../../../shared/services/geofence.service";
import { Utils } from "../../../shared/helpers/utils";
@Component({
    selector: 'plants-list',
    templateUrl: './plants-list.component.html',
    styleUrls: ['./plants-list.component.scss'],
    providers: [HttpCemex, TranslationService, PlantService]
})
export class PlantsListComponent{
    plants =[];
    constructor(private t:TranslationService, private plantService:PlantService, private geofenceService:GeofencesService, private router: Router, private route: ActivatedRoute){
        this.getAllPlants();
    }
    public getAllPlants(){
        this.plants = [];
        this.plantService.getAllPlants().then(data => {
            this.plants = data.plants;
            this.plants.forEach(plant => {
                plant.complete = Utils.getStandardFormatAddress(this.t, plant.address); //this.IsUndefined(poi.address.streetName) + ',' + this.IsUndefined(poi.address.cityDesc) + ',' + this.IsUndefined(poi.address.regionCode) + ',' + this.IsUndefined(poi.address.countryCode)
            });
        })
    }
    private isConfirmedPlant(plant: any): boolean {
        let confirmed: boolean = Utils.isValidGeoplace(plant);
        return confirmed;
    }
    private editRow(plant: any) {
        this.geofenceService.setPoint(plant, GeofencesService.eGenericPoint.PLANT);
        console.log(plant);
        this.router.navigate(['edit'], { relativeTo: this.route });
    }
}