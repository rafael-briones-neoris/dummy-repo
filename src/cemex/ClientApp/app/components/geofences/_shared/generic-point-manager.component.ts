import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, Injectable } from "@angular/core";
import { CmxMapComponent, ApiLoaderService, IPositionMap } from "@cemex/cmx-map-v1/dist";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { GeofencesService } from "../../../shared/services/geofence.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription, Subject, BehaviorSubject } from "rxjs";
import { MapConstants } from "../../../shared/constants/maps.constants";
import { CmxDialogV3 } from "@cemex/cmx-dialog-v3";
import { ChangeDetectorRef } from '@angular/core';
import * as sprintjs from 'sprintf-js';
import { ISearchElement } from "../../../shared/components/cmx-input-autocomplete/index";
import { MapService } from "../../../shared/services/map.service";
import { Utils } from "../../../shared/helpers/utils";
import { ParametersService } from "../../../shared/services/parameters.service";
import { DriversConstants } from '../../../shared/constants/drivers.constants';

let _ = require("underscore");

@Injectable()
export abstract class GenericPointManager implements OnDestroy, OnInit {

    // Enums
    public _geofenceTypes = EGeofenceType;
    public _dialogType = EDialogType;

    // Map options
    public _driverMap: CmxMapComponent;
    public _subscription: Subscription;
    public _searchSubscription: Subscription;
    public _requestSubcription: Subscription;

    // Google maps drawings & configs
    public _defaultCircleZoom: number = 16;
    public _defaultPolygonZoom: number = 16;
    public _defaultZoom: number = 16;
    public _defaultCircleRadius: number;
    public _selectedType: EGeofenceType;
    public _selectedModalType: EDialogType;
    public _shapeCircle: google.maps.Circle = null;
    public _shapePolygon: google.maps.Polygon = null;
    public _drawingManager: google.maps.drawing.DrawingManager;
    public _geocoder: google.maps.Geocoder;
    public _subject = new BehaviorSubject<any>(null);
    public _posPoint: google.maps.LatLng;
    public _isDrawingModeOn = true;
    public _genericPointMarker: google.maps.Marker;
    public _mapOptions: any;
    public _drawingManagerOptions: any;
    public _hasChanges: boolean = false;
    public _originalRadial: number;
    public _autocompleteService: google.maps.places.AutocompleteService;
    public _resultData: any[];
    // Form settings
    public _addressToSearch: string;
    public _lastAddressToSearch: string;
    public _dataIsValid: boolean = false;
    public _lastSearchResult: any;

    // View details
    public _genericPointAddressPlaceholder: string;
    public _genericPoint: any;
    private _leaveByCancel:boolean = false;

    // Dialog
    @ViewChild('editDialog')
    public _editDialog: CmxDialogV3;
    public _editDialogTitle: string;
    public _editDialogBody: string;
    public _saveChanges: boolean = false;
    public _confirmLeaving: boolean = false;
    public _subjectConfirmLeaving: Subject<boolean> = new Subject();

    // Override variables
    protected _headerEditName: string;
    protected _localStorageKey: string;
    protected _editDialogDefinedTitle: string;
    protected _pointIconPath: string;
    protected _showMap: boolean = true;

    constructor(
        public changeDetectorRef: ChangeDetectorRef, 
        public t: TranslationService, 
        public service: ApiLoaderService, 
        public geofenceService: GeofencesService, 
        public mapService: MapService, 
        public parametersService: ParametersService, 
        public router: Router, 
        public route: ActivatedRoute) {
            const unitRadius: string = parametersService.get("UNIT_OF_LENGTH_FOR_RADIUS");
            const radius: number =  Number(parametersService.get("DEFAULT_SIZE_GEOFENCE_RADIUS"));

            this._defaultCircleRadius = unitRadius.toLowerCase() == 'yd' ? radius * 0.9144 : radius;
    }

    abstract ngOnInit();
    abstract onMapReady(mapComponent: CmxMapComponent);
    abstract APIRequest(request: any, genericId: string);

    ngOnDestroy() {
        if (this._subscription)
            this._subscription.unsubscribe();
    }

    public setGenericPointDetails() {
        let address = this.formattedAddress(this._genericPoint.address);
        this._selectedType = this._genericPoint.address.geoPlace.geoPlaceType;
        this._genericPointAddressPlaceholder = address;
        this._addressToSearch = address;
    }

    public formattedAddress(address: any) {
        return Utils.getShortFormatAddress(this.t, address);;
    }

    public searchAddress(searchAddress: string) {
        // Calling google api to search for address
        this._hasChanges = true;
        this._driverMap.searchPlaceByAddress(searchAddress).subscribe(data => {
            if (!data) {
                return false;
            }

            let result: any = data;
            let latLng = new google.maps.LatLng(result.latitude, result.longitude);
            if (result.found) {
                this._genericPointMarker.setPosition(new google.maps.LatLng(result.latitude, result.longitude));
                let latlLngBounds = new google.maps.LatLngBounds();
                latlLngBounds.extend(this._genericPointMarker.getPosition());
                this._driverMap.mapInstance.setOptions(this._mapOptions);
                this._driverMap.mapInstance.setCenter(latlLngBounds.getCenter());
                this._driverMap.mapInstance.fitBounds(latlLngBounds);
                this._driverMap.mapInstance.setZoom(this._defaultZoom);

                if (this._selectedType == EGeofenceType.POLYGON) {
                    this._shapePolygon.setPath([]);
                    this._drawingManager.setMap(this._driverMap.mapInstance);
                    this._drawingManager.setDrawingMode(null);
                }

                this.validateData();

                var normalizedLatLng = { lat: latLng.lat(), lng: latLng.lng() };
                this.searchPlaceByProperty({ 'location': normalizedLatLng }).subscribe(result => {
                    if (!result)
                        return false;

                    if (!this._showMap)
                        this._showMap = true;
                    this._lastSearchResult = result;
                    this.validateData();
                });
            }
        });
    }

    public searchPlaceByProperty(property: any) {
        if (!this._geocoder) {
            this._geocoder = new google.maps.Geocoder();
        }

        this._geocoder.geocode(property,
            (geoCodeResults: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
                const foundStatus = status === google.maps.GeocoderStatus.OK;
                const result: any = {
                    found: foundStatus,
                    message: status.toString(),
                    formatted_address: foundStatus ? geoCodeResults[0].formatted_address : undefined,
                    latitude: foundStatus ? geoCodeResults[0].geometry.location.lat() : undefined,
                    longitude: foundStatus ? geoCodeResults[0].geometry.location.lng() : undefined,
                    results: geoCodeResults,
                };
                this._subject.next(result);
            });
        return this._subject.asObservable();
    }

    // Auto Complete Google Map Service
    public getSearchResults(result: ISearchElement[]): void {
        this._resultData = _.clone(result);
        this.changeDetectorRef.detectChanges();
    }

    public onSelectElementEvent(item) {
        this.elementSelected(item);
    }

    public onClickItem(item) {
        this.elementSelected(item);
    }

    public elementSelected(item) {
        this._addressToSearch = item.displayData;
        this.searchAddress(item.displayData);
    }

    public triggerGeofenceType(_selectedgGeofenceType: EGeofenceType) {
        // If user selects same type, do nothing
        if (this._selectedType === _selectedgGeofenceType) {
            return false;
        }

        // Change selected type
        this._hasChanges = true;
        this._selectedType = _selectedgGeofenceType;

        if (this._shapePolygon) {
            this._shapePolygon.setMap(null);
        }

        if (this._shapeCircle) {
            this._shapeCircle.setMap(null);
        }

        // Change shape properties depending on which one is active
        switch (this._selectedType) {
            case EGeofenceType.RADIAL:
                this._shapeCircle = this.drawRadial(this._genericPointMarker.getPosition(), this._defaultCircleRadius);
                break;
            case EGeofenceType.POLYGON:
                this._drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
                if (this._shapePolygon) {
                    this._shapePolygon.setPath([]);
                }
                break;
        }

        // Show drawing manager options if polygon is selected
        this._drawingManager.setMap(this._selectedType === EGeofenceType.POLYGON ? this._driverMap.mapInstance : null);
        this.validateData();
    }

    public cancelSubscription(subscription: any) {
        // cancel subscriptions send by parameter
        subscription.unsubscribe();
    }

    public drawGeofence(geoPlace: any) {
        // Create a circle and polygon shape
        let geoRadius = this._genericPoint && this._genericPoint.address && this._genericPoint.address.geoPlace &&
            this._genericPoint.address.geoPlace.geoFenceRadius ?
            this._genericPoint.address.geoPlace.geoFenceRadius : this._defaultCircleRadius;
        let geoData = geoPlace.geoData ? geoPlace.geoData : [];

        let radius = geoRadius;
        let points: any[] = [];
        for (let point of geoData) {
            points.push({
                lat: point.latitude, lng: point.longitude
            })
        }


        // Disable both shapes and compare which type is selected.
        //this._shapeCircle.setVisible(false);
        //this._shapePolygon.setVisible(false);
        switch (this._selectedType) {
            case EGeofenceType.POLYGON:
                this._shapePolygon = this.drawPolygon(points);
                this._defaultZoom = this._defaultPolygonZoom;
                if (this._shapeCircle) {
                    this._shapeCircle.setMap(null);
                }
                break;

            case EGeofenceType.RADIAL:
                this._defaultZoom = this._defaultCircleZoom;
                this._shapeCircle = this.drawRadial(this._posPoint, radius > 0 ? radius : 300);
                this._originalRadial = this._shapeCircle.getRadius();
                if (this._shapePolygon) {
                    this._shapePolygon.setMap(null);
                }

                //this._shapeCircle.setVisible(true);
                break;
        }
    }

    public resetGeofence() {
        // Reset paths for polygon and change circle radius
        this._hasChanges = true;
        
        if (this._shapeCircle) {
            this._shapeCircle.setMap(null);
        }

        switch (this._selectedType) {
            case EGeofenceType.RADIAL:
                this._shapeCircle = this.drawRadial(this._posPoint, this._defaultCircleRadius);
                this._drawingManager.setMap(null);
                break;
            case EGeofenceType.POLYGON:
                if (this._shapePolygon)
                    this._shapePolygon.setPath([]);
                this._drawingManager.setMap(this._driverMap.mapInstance);
                this._drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
                break;
        }

        this.validateData();
    }

    public initializeDrawingManager(options: google.maps.drawing.DrawingManagerOptions): google.maps.drawing.DrawingManager {
        // Set drawing manager if not e
        let vm = this;

        this._drawingManager = new google.maps.drawing.DrawingManager(options);
        this._drawingManager.setDrawingMode(null);
        google.maps.event.addListener(this._drawingManager, "overlaycomplete", (event) => {
            vm._hasChanges = true;
            // vm._shapePolygon.setPath([]);
            vm._shapePolygon = event.overlay;
            vm._drawingManager.setDrawingMode(null);
            vm._drawingManager.setMap(null);
            vm.validateData();
        });

        return this._drawingManager;
    }

    public drawMarker(geoPlace: any, vm: any) {
        // Draw marker on given geoPlace.
        let geoPosition = new google.maps.LatLng(geoPlace.latitude, geoPlace.longitude);
        vm._posPoint = geoPosition;
        vm._genericPointMarker = new google.maps.Marker({
            position: geoPosition,
            icon: vm._pointIconPath,
            draggable: true,
            zIndex: 15
        });
        this._genericPointMarker.addListener("dragend", (event) => {
            vm.validateData();
        });

        vm._driverMap.addMarker("genericPoint", vm._genericPointMarker);
        return geoPosition;
    }

    // Custom draw radial to enable editable and draggable properties
    public drawRadial(center: google.maps.LatLng, radius: number): google.maps.Circle {
        let circle = new google.maps.Circle({
            center: this._genericPointMarker.getPosition(),
            radius: radius,
            strokeColor: '#2e8af5',
            strokeOpacity: 0.8,
            fillColor: '#139DF2',
            fillOpacity: 0.35,
            editable: true,
            draggable: false,
        });

        circle.setMap(this._driverMap.mapInstance);
        circle.bindTo('center', this._genericPointMarker, 'position');

        return circle;
    }

    // Custom draw polygon to add to custom map.
    public drawPolygon(points: any[]): google.maps.Polygon {
        const newShape = new google.maps.Polygon({
            paths: points,
            strokeColor: '#2e8af5',
            strokeOpacity: 0.8,
            strokeWeight: 2.5,
            fillColor: '#139DF2',
            fillOpacity: 0.35,
        });

        newShape.setMap(this._driverMap.mapInstance);
        return newShape;
    }

    public saveGenericPoint() {
        if (!this._dataIsValid)
            return;

        const request: any = this.getRequestBody();

        // API call
        const pointId = this._genericPoint ? this._genericPoint.plantId || this._genericPoint.destinationId || this._genericPoint.pointId : null;
        this._requestSubcription = this.APIRequest(request, pointId).subscribe(response => {
            if (!response) {
                return false;
            }
            if (response.ok && response.status == 200) {
                this.handleResponseOK();
            } else {
                this.handleResponseError();
            }
        }, err => {
            this.handleResponseError();
        }
            , this);
    }

    public handleResponseError() {
        // Error during the request, show default error
        this._editDialogTitle = 'management.detail.error';
        this._editDialogBody = '';
        this.handleModalResults();
    }

    public handleResponseOK() {
        // Server response is valid, show appropiate message
        this._editDialogTitle = this._editDialogDefinedTitle;
        this._editDialogBody = '';
        this.handleModalResults();
    }

    public handleModalResults() {
        // Show modal and cancel subscription of this request.
        this._selectedModalType = EDialogType.REQUEST_RESPONSE;
        this.showResultModal();
        this.cancelSubscription(this._requestSubcription);
    }

    public userLeaving() {
        // set leaving modal with appropiate translations/messages
        this._selectedModalType = EDialogType.LEAVING;
        this._editDialogTitle = 'management.leaving.confirmation';
        this._editDialogBody = 'management.leaving.message';
    }

    public showResultModal() {
        // open dialog
        this._editDialog.open();
    }

    public canDeactivateGuard() {
        // if the user has changed something in the screen and hasn't saved the changes, open modal and wait for user answer.
        if (this.hasChanges() && !this._saveChanges) {
            this._leaveByCancel = false;
            this.userLeaving();
            this.showResultModal();
            return this._subjectConfirmLeaving.asObservable();
        }
        else {
            return true;
        }
    }

    public userLeavingResponse(saveChanges: boolean, redirectParent?: boolean) {
        // Modal options
        this._saveChanges = saveChanges;
        this._confirmLeaving = saveChanges;
        if (this._editDialog) {
            this._editDialog.close();
        }
        // Set selected option, if true: complete subscription.
        this._subjectConfirmLeaving.next(saveChanges);
        if (saveChanges) {
            this._subjectConfirmLeaving.complete();
        }
        // If function contains parameter redirectToParent, redirect if needed.
        if (redirectParent) {
            this.router.navigate(["../"], { relativeTo: this.route });
        }
    }

    public getRequestBody() {
        // function that returns request body for plant/PoD edit.
        let request: any;
        switch (this._selectedType) {
            case EGeofenceType.RADIAL:
                request = {
                    address: {
                        geoPlace: {
                            geoPlaceType: "R",
                            latitude: this._genericPointMarker.getPosition().lat(),
                            longitude: this._genericPointMarker.getPosition().lng(),
                            geoFenceRadius: Math.round(this._shapeCircle.getRadius()),
                            points: []
                        }
                    }
                }
                break;
            case EGeofenceType.POLYGON:
                let polyMaps: google.maps.Polygon = this._shapePolygon;
                let polyArr: any[] = [];
                for (let point of polyMaps.getPath().getArray()) {
                    polyArr.push({
                        latitude: point.lat(),
                        longitude: point.lng()
                    })
                }
                polyArr.push({
                    latitude: polyArr[0].latitude,
                    longitude: polyArr[0].longitude
                })
                request = {
                    address: {
                        geoPlace: {
                            geoData: polyArr,
                            geoPlaceType: "P",
                            latitude: this._genericPointMarker.getPosition().lat(),
                            longitude: this._genericPointMarker.getPosition().lng(),
                        }
                    }
                }
                break;
        }
        return request;
    }

    public validateData() {
        // function that sets up the validity of our edit.
        if (!this._shapePolygon && !this._shapeCircle) {
            this._dataIsValid = false;
            this.changeDetectorRef.detectChanges();
            return;
        }
        // if polygon, check for length > 2 && marker inside polygon and if circle, check radius > 0
        switch (this._selectedType) {
            case EGeofenceType.POLYGON:
                this._dataIsValid = this._shapePolygon && this._shapePolygon.getPath().getLength() > 2 && this.markerInsidePolygon();
                break;

            case EGeofenceType.RADIAL:
                this._dataIsValid = this._shapeCircle.getRadius() > 0
                break;
            default:
                this._dataIsValid = false;
                break;
        }
        this.changeDetectorRef.detectChanges();
    }

    public hasChanges() {
        // function that detects if the user has modified or dragged the geofence/marker.
        if (!this._genericPointMarker || !this._shapeCircle) {
            return false;
        }
        return this._posPoint != this._genericPointMarker.getPosition() ||
            this._originalRadial != this._shapeCircle.getRadius() ||
            this._hasChanges;
    }

    public markerInsidePolygon() {
        // function that returns if the marker is inside the polygon
        if (!this._genericPointMarker || !this._shapePolygon) {
            return false;
        }
        return google.maps.geometry.poly.containsLocation(this._genericPointMarker.getPosition(), this._shapePolygon);
    }

    public setMapAndDrawingManagerOptions() {
        // Set drawing manager options and map options
        this._drawingManagerOptions = {
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [google.maps.drawing.OverlayType.POLYGON]
            },
            polygonOptions: {
                strokeColor: '#2e8af5',
                strokeOpacity: 0.8,
                strokeWeight: 3,
                fillColor: '#139DF2',
                fillOpacity: 0.35,
                clickable: false,
                editable: false
            },
        }

        this._mapOptions = {
            mapTypeControl: true,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE],
                position: google.maps.ControlPosition.BOTTOM_LEFT,
                style: google.maps.MapTypeControlStyle.DEFAULT
            },
            streetViewControl: false,
            scaleControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            fullscreenControl: true,
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_RIGHT
            }
        }
    }

    private doLeaving():void {
        this.userLeaving();
        this._leaveByCancel = true;
        this.showResultModal();       
    }
}

export enum EGeofenceType {
    RADIAL = 'R',
    POLYGON = 'P'
}

export enum EDialogType {
    LEAVING = 1,
    REQUEST_RESPONSE = 2
}