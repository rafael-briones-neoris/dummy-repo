import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, Injectable } from "@angular/core";
import { CmxMapComponent, ApiLoaderService, IPositionMap } from "@cemex/cmx-map-v1/dist";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { GeofencesService } from "../../../shared/services/geofence.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription, Subject } from "rxjs";
import { MapConstants } from "../../../shared/constants/maps.constants";
import { CmxDialogV3 } from "@cemex/cmx-dialog-v3";
import { ChangeDetectorRef } from '@angular/core';
import * as sprintjs from 'sprintf-js';
import { ISearchElement } from "../../../shared/components/cmx-input-autocomplete/index";
import { MapService } from "../../../shared/services/map.service";
import { GenericPointManager, EGeofenceType } from "./generic-point-manager.component";
import { Utils } from "../../../shared/helpers/utils";

let _ = require("underscore");

@Injectable()
export abstract class GenericPointNew extends GenericPointManager {

    // On Init override for New
    // When we initialize this component, we will set default shapes (radial).
    // Since it's a new generic point, there's no previous information to be setup.
    ngOnInit() {
        this._selectedType = EGeofenceType.RADIAL;
    }

    // On Map Ready override for New
    // For this case, we will load the map with default shapes and default geolocations.
    public onMapReady(mapComponent: CmxMapComponent) {
        let vm = this;
        // Establish driverMap reference to CmxMapComponent
        this._driverMap = mapComponent;

        // Set Map Options and Drawing Manager Options in local variables
        this.setMapAndDrawingManagerOptions();

        // Initialize drawing manager
        this._drawingManager = this.initializeDrawingManager(this._drawingManagerOptions);
        this._drawingManager.setMap(null);

        // Establish and set map options
        this._driverMap.mapInstance.setOptions(this._mapOptions);

        // Auto complete function loading
        this._autocompleteService = new google.maps.places.AutocompleteService();
        this.mapService.setAutoCompleteService(this._autocompleteService);

        mapComponent.getCurrentLocation().subscribe((position: google.maps.LatLng) => {
            if (!position)
                return;
            let geoPlace: any = { latitude: 0, longitude: 0 };
            geoPlace.latitude = position.lat();
            geoPlace.longitude = position.lng();

            vm.drawMarker(geoPlace, vm);
            vm.drawGeofence(geoPlace);
        });

        mapComponent.mapOptions = this._mapOptions;
        mapComponent.centerCurrentLocation().subscribe(
            result => {}
        );
    }

}