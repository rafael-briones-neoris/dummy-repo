import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, Injectable } from "@angular/core";
import { CmxMapComponent, ApiLoaderService, IPositionMap } from "@cemex/cmx-map-v1/dist";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { GeofencesService } from "../../../shared/services/geofence.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription, Subject } from "rxjs";
import { MapConstants } from "../../../shared/constants/maps.constants";
import { CmxDialogV3 } from "@cemex/cmx-dialog-v3";
import { ChangeDetectorRef } from '@angular/core';
import * as sprintjs from 'sprintf-js';
import { ISearchElement } from "../../../shared/components/cmx-input-autocomplete/index";
import { MapService } from "../../../shared/services/map.service";
import { GenericPointManager, EGeofenceType } from "./generic-point-manager.component";
import { Utils } from "../../../shared/helpers/utils";

let _ = require("underscore");

@Injectable()
export abstract class GenericPointEdit extends GenericPointManager {

    // On Init override for Edit
    // When we initialize this component, we need to try and find the generic point to edit
    // This point can be in our local storage or directly sent from a previous route.
    // Since this is an existing point, we need data to fill in the details of the page.
    ngOnInit() {
        this._subscription = this.geofenceService.getPoint().subscribe(event => {
            let genericPoint = event || JSON.parse(localStorage.getItem(this._localStorageKey));
            // If we cannot find a point in service or in local storage, redirect to parent
            if (!genericPoint) {
                this._confirmLeaving = true;
                this.userLeavingResponse(true, true);
            } else {
                this.loadGenericPoint(genericPoint);
            }
        });
    }

    private loadGenericPoint(genericPoint: any) {
        // If Generic Point has destination attribute, assume it's a Point of Delivery
        if (genericPoint.destination) {
            this._genericPoint = genericPoint.destination;
            this._genericPoint._dd_pointDesc = genericPoint.destination.destinationDesc;
        }
        // If not, assume it's a Plant / Point of Interest
        else {
            this._genericPoint = genericPoint;
            this._genericPoint._dd_pointDesc = genericPoint.plantDesc || genericPoint.pointName;
        }

        // Set view details
        this.setGenericPointDetails();
    }

    // On Map Ready override for Edit
    // For this case, we will load automatically the map to the address of the
    // Selected generic point, try and find the address or use the default address.
    public onMapReady(mapComponent: CmxMapComponent) {
        let vm = this;
        // Establish driverMap reference to CmxMapComponent
        this._driverMap = mapComponent;

        // Set Map Options and Drawing Manager Options in local variables
        this.setMapAndDrawingManagerOptions();

        // Initialize drawing manager
        this._drawingManager = this.initializeDrawingManager(this._drawingManagerOptions);
        this._drawingManager.setMap(null);

        // Establish and set map options
        this._driverMap.mapInstance.setOptions(this._mapOptions);

        // Auto complete function loading
        this._autocompleteService = new google.maps.places.AutocompleteService();
        this.mapService.setAutoCompleteService(this._autocompleteService);

        // Load map on appropiate location
        let drvMap = this._driverMap;

        // Add marker to the selected generic point
        // If generic point does not have a valid latitude or longitude, search.
        if (!Utils.isValidGeoplace(this._genericPoint)) {
            let idleListener = google.maps.event.addListenerOnce(this._driverMap.mapInstance, 'idle', function () {
                // On Map Ready, search
                // Make sure selected type is radial if geoplace is invalid
                vm._selectedType = EGeofenceType.RADIAL;
                vm._searchSubscription = vm._driverMap.searchPlaceByAddress(vm._addressToSearch).subscribe(data => {
                    if (!data) {
                        return false;
                    }
                    let result: any = data;
                    let newGeoPlace = { latitude: 0, longitude: 0 };
                    if (result && result.found) {
                        newGeoPlace.latitude = result.latitude;
                        newGeoPlace.longitude = result.longitude;
                        vm.drawShapes(vm, newGeoPlace, drvMap);
                    }
                    else {
                        vm._driverMap.getCurrentLocation().subscribe(position => {
                            if (!position)
                                return;
                            newGeoPlace.latitude = position.lat();
                            newGeoPlace.longitude = position.lng();
                            vm.drawShapes(vm, newGeoPlace, drvMap);
                        });
                    }
                });
            });
        } else {
            let geoPlace = this._genericPoint.address.geoPlace;
            this.drawMarker(geoPlace, this);
            this.drawGeofence(geoPlace);
            mapComponent.centerCurrentLocation([vm._posPoint], 18).subscribe(
                result => { 
                    console.debug('edit component:', result.lat(), result.lng());
                }
            );

        }


    }

    private drawShapes(vm: any, newGeoPlace: any, drvMap: any) {
        vm.drawMarker(newGeoPlace, vm);
        vm.drawGeofence(newGeoPlace);
        let latlLngBounds = new google.maps.LatLngBounds();
        latlLngBounds.extend(vm._posPoint);
        drvMap.mapInstance.setCenter(latlLngBounds.getCenter());
        drvMap.mapInstance.fitBounds(latlLngBounds);
        drvMap.mapInstance.setZoom(vm._defaultZoom);

        vm.cancelSubscription(vm._searchSubscription);
        vm.validateData();
    }

}