import { Component, Input } from '@angular/core';

import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { DeliveryPointService } from '../../../shared/services/delivery-point.service';
import { GeofencesService } from '../../../shared/services/geofence.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'points-delivery-list',
    templateUrl: './points-delivery-list.component.html',
    styleUrls: ['./points-delivery-list.component.scss']
})

export class PointsDeliveryListComponent {

    listPoD = [];
    constructor(public DOPService: DeliveryPointService, private t: TranslationService, private geofenceService: GeofencesService, private router: Router, private route: ActivatedRoute) {
        console.log('constructor');

        this.listPoD = [];
        this.DOPService.getAllPOD().subscribe(data => {
            this.listPoD = data;
        })
    }

    private isConfirmedPOD(pod: any): boolean {

        let confirmed: boolean =
            pod.destination.address.geoPlace != undefined && pod.destination.address.geoPlace.geoPlaceType != undefined &&
            pod.destination.address.geoPlace.geoPlaceType != "" && pod.destination.address.geoPlace.latitude != 0 &&
            pod.destination.address.geoPlace.longitude != 0;
        return confirmed;
    }

    private editRow(plant: any) {
        this.geofenceService.setPoint(plant, GeofencesService.eGenericPoint.POINT_DELIVERY);
        this.router.navigate(['edit'], { relativeTo: this.route });
    }
}