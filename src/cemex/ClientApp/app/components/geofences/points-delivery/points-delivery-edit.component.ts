import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { CmxMapComponent, ApiLoaderService, IPositionMap } from "@cemex/cmx-map-v1/dist";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { GenericPointEdit } from "../_shared/generic-point-edit.component";
import { MapConstants } from "../../../shared/constants/maps.constants";

@Component({
    selector: 'points-delivery-edit',
    templateUrl: '../_shared/plants-points-delivery-edit/plants-points-delivery-edit.component.html',
    styleUrls: ['../_shared/plants-points-delivery-edit/plants-points-delivery-edit.component.scss']
})
export class PointsDeliveryEditComponent extends GenericPointEdit {

    protected _headerEditName: string = 'management.points.delivery';
    protected _localStorageKey: string = 'lastEditedPointDelivery';
    protected _editDialogDefinedTitle: string = 'management.saved.pod';
    protected _pointIconPath: string = MapConstants.ICONS.POD.icon;

    public APIRequest(request: any, deliveryPointId: string) {
        if (this._genericPoint.isDeliverypoint) {
            return this.geofenceService.updateDeliveryPointGeofence(request, deliveryPointId);
        }
        else {
            return this.geofenceService.updateJobsiteGeofence(request, deliveryPointId);
        }
    }

}