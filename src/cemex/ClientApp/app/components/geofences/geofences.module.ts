import { NgModule, Injectable, HostListener } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Resolve } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DriversSharedModule } from '../../shared/components/index';
import { CmxListModule } from '@cemex/cmx-list-v1'
import { PointsInterestListComponent } from './points-interest/points-interest-list.component';
import { PlantsListComponent } from './plants/plants-list.component';

import { CmxTextAreaModule } from "@cemex/cmx-textarea-v1/dist/";
import { CmxMapModule } from '@cemex/cmx-map-v1/dist';
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v1';
import { GeofencesService } from '../../shared/services/geofence.service';
import { DriversConstants } from '../../shared/constants/drivers.constants';
import { PointsDeliveryListComponent } from './points-delivery/points-delivery-list.component';
import { DeliveryPointService } from '../../shared/services/delivery-point.service';
import { PlantsEditComponent } from './plants/plants-edit.component';
import { Observable } from 'rxjs/Observable';
import { CmxInputAutoCompleteModule } from '../../shared/components/cmx-input-autocomplete/index';
import { MapService } from '../../shared/services/map.service';
import { PointsDeliveryEditComponent } from './points-delivery/points-delivery-edit.component';
import { GenericPointManager } from './_shared/generic-point-manager.component';
import { PointsInterestEditComponent } from './points-interest/points-interest-edit.component';
import { PointsInterestNewComponent } from './points-interest/points-interest-new.component';

@Injectable()
class CanDeactiveGenericPoint implements CanDeactivate<GenericPointManager> {

    constructor(private router: Router) { }

    canDeactivate(component: GenericPointManager, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return component.canDeactivateGuard();
    }
}

const ROUTES: Routes = [
    {
        path: 'delivery-list', children: [
            { path: '', component: PointsDeliveryListComponent },
            { path: 'edit', component: PointsDeliveryEditComponent, canDeactivate: [CanDeactiveGenericPoint] }
        ]
    },
    {
        path: 'poi-list', children: [
            { path: '', component: PointsInterestListComponent },
            { path: 'edit', component: PointsInterestEditComponent, canDeactivate: [CanDeactiveGenericPoint] },
            { path: 'new', component: PointsInterestNewComponent, canDeactivate: [CanDeactiveGenericPoint] }

        ]
    },
    {
        path: 'plant-list', children: [
            { path: '', component: PlantsListComponent },
            { path: 'edit', component: PlantsEditComponent, canDeactivate: [CanDeactiveGenericPoint] }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        CmxMapModule.forRoot(),
        DriversSharedModule,
        CmxListModule,
        CmxMapModule.forRoot(),
        CmxFormFieldModule,
        CmxInputAutoCompleteModule,
        CmxTextAreaModule
    ],
    declarations: [
        PointsDeliveryListComponent,
        PointsInterestListComponent,
        PlantsListComponent,
        PlantsEditComponent,
        PointsDeliveryEditComponent,
        PointsInterestEditComponent,
        PointsInterestNewComponent
    ],
    providers: [
        GeofencesService,
        MapService,
        DeliveryPointService,
        CanDeactiveGenericPoint,
    ],
    entryComponents: []
})
export class GeofencesModule { }
