import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DriversSharedModule } from '../../shared/components/index';
import { SchedulingComponent } from './scheduling.component';
import { CmxDropdownModule } from "@cemex/cmx-dropdown-v1/dist";
import { CmxDatepickerModule } from "@cemex/cmx-datepicker-v1/dist";
import { CmxInputModule } from "@cemex/cmx-input-v2/dist";
import { CmxListModule } from '@cemex/cmx-list-v1/dist'
import { CmxInputAutoCompleteModule } from '../../shared/components/cmx-input-autocomplete/index';
import { DriversService } from "../../shared/services/drivers.service";

const ROUTES: Routes = [
    { path: '', component: SchedulingComponent },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        DriversSharedModule,
        CmxDropdownModule,
        CmxDatepickerModule,
        CmxInputModule, 
        CmxInputAutoCompleteModule,
        CmxListModule
    ],
    declarations: [
        SchedulingComponent,
    ],
    providers: [DriversService],
    entryComponents: []
})
export class SchedulingModule { }
