import * as moment from 'moment';
import { Component, Input, ViewChild, OnInit, PipeTransform, Pipe, Inject, ChangeDetectorRef } from '@angular/core';
import { SchedulingDTO } from '@cemex-core/types-v1/dist';
import { PlantService } from "../../shared/services/api/PlantService";
import { DriversService } from "../../shared/services/drivers.service";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { SchedulingService } from "../../shared/services/api/SchedulingService";
import { FilterPipe } from '../../utils/filter.pipe';
import { fail } from 'assert';
import { Observable } from 'rxjs/Observable';
import { ISearchElement, ISearchInputService } from '../../shared/components/cmx-input-autocomplete/index';
import { METHODS } from 'http';
import { Enums } from '../../shared/helpers/enums';
import { SorterCompareFn, SearchFilterFn } from '../../shared/components/cemex-filter-content/filter-sorter.component';

@Component({
    selector: 'scheduling',
    templateUrl: './scheduling.component.html',
    styleUrls: ['./scheduling.component.scss'],
    providers: [PlantService, HttpCemex, SchedulingService, TranslationService]
})

export class SchedulingComponent {

    plantSelected: string = "";
    schedulingId: number = 0;
    selectedDriver: string = null;
    plantSelectedId: string;
    countCheck: number = 0;
    pendingSchedule: boolean = false;
    //     /*DATES */
    month: string;
    intMonth: number;
    intDay: number;
    day: string;
    dayDate: number;
    date = new Date();
    dateAux = new Date(this.date);
    lastDayMonth: number;
    arrayDays = ["scheduling.SUNDAY", "scheduling.MONDAY", "scheduling.TUESDAY",
        "scheduling.WEDNESDAY", "scheduling.THRUSDAY", "scheduling.FRIDAY",
        "scheduling.SATURDAY"];
    arrayMonth = ["scheduling.JUN", "scheduling.FEB", "scheduling.MAR", "scheduling.APR", "scheduling.MAY", "scheduling.JUN",
        "scheduling.JUL", "scheduling.AGO", "scheduling.SEP", "scheduling.OCT", "scheduling.NOV", "scheduling.DIC"]

    plantsSearch: any[];
    plants = [];

    //     /*EDIT*/
    editDriverId: number;
    editDriverCode: string;

    editDriverTruck: string;
    editDriverTime: string;
    editSchedulingDetail: number;
    isEdit: boolean = false;
    editDriverStatus: number;
    isInvalid: boolean = false;
    isSameDay: boolean = true;

    //     /*Add*/
    addDriverTime: string = null;
    addDriverTruck: string = null;
    addDriverId: number = null;
    addDriverCode: string = null;

    private _schedulePlantDate: any;

    complete: boolean = false;
    searchValue: string = null;
    editSearchValue: string = "";
    _openList: boolean = false;
    hasModifications: boolean = false;
    firstEntry: boolean = true;
    private _resultData: ISearchElement[] = [];
    private _currentSchedules: any;
    private _canEditSchedule: boolean = true;
    private _allDriversRows: any[] = []

    private sorterDriversFn: SorterCompareFn = function (objectLeft: any, objectRight: any): number {
        if (objectLeft.driver.driverDesc < objectRight.driver.driverDesc) {
            return -1;
        } else if (objectLeft.driver.driverDesc > objectRight.driver.driverDesc) {
            return 1;
        }
        return 0;
    }

    private searchDriversFn: SearchFilterFn = function (drivers: any[], searchValue: string): any[] {
        return drivers.filter((driver) => {
            return driver.driver.driverDesc.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0;
        });
    };

    private get isValidNewHour(): boolean {
        return this.validateTime(this.addDriverTime);
    }

    private get isValidTruckId(): boolean { //validates truck id length
        return this.validateTruckId(this.addDriverTruck);
    }

    private get isValidHour(): boolean {
        return this.validateTime(this.editDriverTime);
    }

    constructor(public cd: ChangeDetectorRef,
        public svrplants: PlantService,
        private t: TranslationService,
        public svrScheduling: SchedulingService,
        public driversService: DriversService
    ) {
        this.addDriverTime = "00:00";
        this.intDay = this.date.getDay();
        this.intMonth = this.date.getMonth();
        this.day = this.arrayDays[this.intDay];
        this.month = this.arrayMonth[this.intMonth];
        this.dayDate = this.date.getDate();
        this.lastDayMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();

        this.svrplants.getAllPlants().then((data) => {
            this.cd.detectChanges();
            this.plants = data.plants;
            if (this.plants != undefined) {
                this.plantSelected = this.plants[0].plantDesc;
                this.plantSelectedId = this.plants[0].plantId
            }
            let today: string = this.getToday();
            let isselected: boolean = false;
            this.svrScheduling.getScheduling(today).then((data) => {
                this.cd.detectChanges();
                let arr = data;
                this._currentSchedules = data;
                this._schedulePlantDate = undefined;
                if (data != undefined) {
                    data.schedules.forEach(schedule => {
                        if (schedule.plant.plantId == this.plantSelectedId) {
                            this.schedulingId = schedule.scheduleId;
                            this.getScheduleDetail(this.plantSelectedId);
                        }

                    });
                }
            });

        }).catch((error) => {
            console.error("Error message", error.err);
        });

    }

    private getInputObservable(event) {
        if (this.plantSelectedId !== event.plantId) {
            this.plantSelected = event.plantDesc;
            this.plantSelectedId = event.plantId;
            this._openList = false;
            if (this._currentSchedules) {
                let plantHasSchedule = false;
                for (let schedule of this._currentSchedules.schedules) {
                    if (schedule.plant.plantId == this.plantSelectedId) {
                        this.schedulingId = schedule.scheduleId;
                        this.getScheduleDetail(this.plantSelectedId);
                        plantHasSchedule = true;
                        break;
                    }
                }

                if (!plantHasSchedule) {
                    if (this._schedulePlantDate)
                        this._schedulePlantDate.scheduleDetails = [];
                    this.schedulingId = 0;
                    this.isEdit = false;
                }
            } else {
                if (this._schedulePlantDate)
                    this._schedulePlantDate.scheduleDetails = [];
                this.complete = false;
            }
        }
    }

    private getScheduleDetail(idplant) {
        this.svrScheduling.getSchedulingDetail(this.schedulingId).subscribe((data) => {

            let minDate = moment().subtract(1, 'days');
            let actualDate = moment(this.dateAux);

            // canEditSchedule off when it's a previous day
            this._canEditSchedule = this.getDateDiff(minDate, actualDate) >= 1;

            if (this.firstEntry && !this._schedulePlantDate)
                this.firstEntry = false;

            this._schedulePlantDate = data;
            this._schedulePlantDate.originDetails = [];
            this._allDriversRows = [];
            if (this._schedulePlantDate && this._schedulePlantDate.scheduleDetails) {
                this.pendingSchedule = false;
                this._schedulePlantDate.originDetails = data.scheduleDetails.slice(0, data.scheduleDetails.length);
                this._schedulePlantDate.scheduleDetails.forEach(detail => {
                    detail.scheduleDetailTime = this.formatUTCToLocalTime(detail.scheduleDetailTime);
                    if (detail.scheduleStatus.scheduleStatusId == 1) {
                        this.pendingSchedule = true;
                    }
                    this.complete = true;
                });

                const scheduleDetails: any = this._schedulePlantDate.scheduleDetails;
                this._allDriversRows = scheduleDetails.slice(0, scheduleDetails.length);
            }

            this.cd.detectChanges();
        });

    }

    getDateDiff(minDate: any, actualDate: any) {
        return actualDate.startOf('day').diff(minDate.startOf('day'), 'days');
    }

    private decreaseDate() {
        let minDate = moment().subtract(1, 'days');
        let actualDate = moment(this.dateAux);

        //console.info('begin decrease: ', this.dateAux)
        let date = new Date();
        if (Date.parse(this.dateAux.toDateString()) == Date.parse(date.toDateString())) {


            this.isSameDay = true;
            return;
        }
        this.isSameDay = false;
        let year = this.dateAux.getFullYear();
        this.intDay -= 1;
        if (this.intDay < 0) {
            this.intDay = 6
        }
        this.day = this.arrayDays[this.intDay];
        this.dayDate = this.dateAux.getDate() - 1;
        if (this.dayDate < 1) {
            this.intMonth = this.dateAux.getMonth() - 1;
            if (this.intMonth < 0) {
                this.intMonth = 11;
                year -= 1;
            }
            this.dayDate = new Date(year, this.intMonth + 1, 0).getDate();

        }
        this.day = this.arrayDays[this.intDay];
        this.month = this.arrayMonth[this.intMonth];
        this.dateAux = new Date(year, this.intMonth, this.dayDate)
        this.lastDayMonth = new Date(this.dateAux.getFullYear(), this.dateAux.getMonth() + 1, 0).getDate()

        this.plants.forEach(plant => {
            plant.selected = "";
            plant.pending = "";
            plant.wiSchedule = false;

            plant.scheduleId = undefined;
        });
        this.getSchedule();
    }

    private incrementDate() {
        this.isSameDay = false;
        this.countCheck = 0;

        let year = this.dateAux.getFullYear();
        this.intDay += 1;
        if (this.intDay > 6) {
            this.intDay = 0
        }
        this.day = this.arrayDays[this.intDay];
        this.dayDate = this.dateAux.getDate() + 1;
        if (this.dayDate > this.lastDayMonth) {
            this.intMonth = this.dateAux.getMonth() + 1;
            this.dayDate = 1;
            if (this.intMonth > 11) {
                this.intMonth = 0;
                year += 1;
            }
        }
        this.day = this.arrayDays[this.intDay];
        this.month = this.arrayMonth[this.intMonth];
        this.dateAux = new Date(year, this.intMonth, this.dayDate)
        this.lastDayMonth = new Date(this.dateAux.getFullYear(), this.dateAux.getMonth() + 1, 0).getDate()
        this.getSchedule();
    }

    private getSchedule() {
        let today = this.getToday()
        let isselected: boolean = false;
        //this.plantSelected = ""
        this.svrScheduling.getScheduling(today).then((data) => {

            let arr = data;
            this._currentSchedules = data;
            this.complete = false;
            this._schedulePlantDate = undefined;
            this._canEditSchedule = this.validateDateSchedule();
            if (data != undefined) {
                data.schedules.forEach(schedule => {
                    if (schedule.plant.plantId === this.plantSelectedId) {
                        this.schedulingId = schedule.scheduleId;
                        this.getScheduleDetail(schedule.plant.plantId)
                    }
                });
            }
        });
    }


    private getToday(): string {
        if (this.firstEntry && !this._schedulePlantDate)
            this.firstEntry = false;
        const result = moment(this.dateAux).format("YYYY-MM-DD");
        return result;
    }

    private selectPlant(plant: any) {
        this.plantSelected = plant.plantDesc;
        this.plantSelectedId = plant.plantId;
    }

    private addSchedule(): void {
        if (!this.addDriverCode || !this.selectedDriver || !this.addDriverId || !this.validationSaveTime() || !this.addDriverTime || !this.searchValue || !this._canEditSchedule)
            return;

        let strTime = this.addDriverTime;
        if (this._schedulePlantDate === undefined || this._schedulePlantDate.length === 0) {
            //  let strTime = this.editDriverTime;
            let schedule = {
                scheduleDateTime: this.getToday(),
                plant:
                    {
                        plantId: this.plantSelectedId
                    },

                scheduleDetails: [],
                originDetails: []
            };
            // tslint:disable-next-line:typedef
            let scheduleDetails = {
                editing: false,
                scheduleDetailId: this.editSchedulingDetail,
                scheduleDetailTime: strTime,//  this.convertLocalToUTCFormat(strTime),
                scheduleStatus: {
                    scheduleStatusId: 1,
                    scheduleStatusDesc: "Pending"
                },
                driver: {
                    driverCode: this.addDriverCode,
                    driverDesc: this.selectedDriver,
                    driverId: this.addDriverId
                },
                scheduleNote: this.addDriverTruck,
                crudOperation: "C"
            }

            schedule.scheduleDetails.push(scheduleDetails);
            this._schedulePlantDate = schedule;
        } else {
            let scheduleDetails = {
                editing: false,
                scheduleDetailId: this.editSchedulingDetail,
                scheduleDetailTime: strTime,// this.convertLocalToUTCFormat(strTime),
                scheduleStatus: {
                    scheduleStatusId: 1,
                    scheduleStatusDesc: "Pending"
                },
                driver: {
                    driverCode: this.addDriverCode,
                    driverDesc: this.selectedDriver,
                    driverId: this.addDriverId,
                },
                scheduleNote: this.addDriverTruck,
                crudOperation: "C"
            }

            this._schedulePlantDate.scheduleDetails.push(scheduleDetails);
        }

        this.complete = true;
        this.hasModifications = true;
        const scheduleDetails: any[] = this._schedulePlantDate.scheduleDetails;
        this._allDriversRows = scheduleDetails.slice(0, scheduleDetails.length);
        this.cd.detectChanges();
        this.cleanVariables();
    }

    private convertLocalToUTCFormat(time?: string, update?: boolean, _delete?: boolean): string {

        let result = moment(this.dateAux);
        if (time) {
            let strTime: string[] = time.split(':');
            result.hour(+strTime[0]);
            result.minute(+strTime[1]);
        }
        else {
            let strTime: string[] = this.isEdit ? this.editDriverTime.split(':') : this.addDriverTime.split(':');
            result.hour(+strTime[0]);
            result.minute(+strTime[1]);
        }

        return result.utc().format();
    }

    private formatTimeUTC(time: string): string {
        const dateStr = moment(this.dateAux).format("YYYY-MM-DD");
        return moment.utc(`${dateStr}T${time}`).format();
    }

    private formatUTCToLocalTime(time: string): string {
        const dateStr = moment(this.dateAux).format("YYYY-MM-DD");
        return moment.utc(`${dateStr}T${time}`).local().format("HH:mm");
    }

    private editRow(schedule: any) {
        this.isEdit = true;
        this.editDriverStatus = schedule.scheduleStatus.scheduleStatusId;
        schedule.editing = true;

        // this.checkToEdit();
        // if (schedule.scheduleStatus.scheduleStatusId == 2 || !this.isAllowedtoEdit) { return };
        // this.EditOut()
        this.isEdit = true;
        this.editDriverStatus = schedule.scheduleStatus.scheduleStatusId;
        // schedule.editing = true;
        this.editDriverId = schedule.driver.driverId;
        this.editSearchValue = schedule.driver.driverDesc;
        this.editDriverTime = schedule.scheduleDetailTime;
        this.editDriverTruck = (schedule.scheduleNote != undefined) ? schedule.scheduleNote.toUpperCase() : '';
        this.editSchedulingDetail = schedule.scheduleDetailId;
        this.selectedDriver = schedule.driver.driverDesc;


        this.editDriverId = schedule.driver.driverId;
        this.editDriverCode = schedule.driver.driverCode;

        this.convertLocalToUTCFormat();
    }

    private editRowData(scheduleDetail: any) {
        // if there were no changes, no need to update schedulingDetail info.
        if (scheduleDetail.scheduleNote === this.editDriverTruck &&
            scheduleDetail.scheduleDetailTime === this.editDriverTime) {
            this.cancelEdit(scheduleDetail);
            return;
        }

        if (this.schedulingId > 0) {
            if (scheduleDetail.crudOperation != 'C') {
                scheduleDetail.crudOperation = 'U';
                scheduleDetail.scheduleStatus.scheduleStatusId = Enums.ScheduleStatus.Pending.Id;
                scheduleDetail.scheduleStatus.scheduleStatusDesc = Enums.ScheduleStatus.Pending.Title;
            }
        }

        scheduleDetail.scheduleNote = this.editDriverTruck;
        scheduleDetail.scheduleDetailTime = this.editDriverTime;

        scheduleDetail.driver.driverCode = this.editDriverCode;
        scheduleDetail.driver.driverDesc = this.selectedDriver;
        scheduleDetail.driver.driverId = this.editDriverId;

        scheduleDetail.editing = false;
        this.isEdit = false;
        this.hasModifications = true;
        this.cd.detectChanges();
    }

    private cleanVariables() {
        this.addDriverTime = null;
        this.addDriverTruck = null;
        this.addDriverId = null;
        this.addDriverCode = "";

        this.editDriverTruck = "";
        this.editDriverTime = "";
        this.editDriverCode = "";
        this.editSearchValue = null;
        this.selectedDriver = null;
        this.searchValue = "";

    }

    private getSearchResults(result: ISearchElement[]): void {
        this._resultData = result;
    }

    private onClickItem(item: ISearchElement): void {
        if (this.isEdit) {
            this.editSearchValue = item.objectData.text;
            this.editDriverId = item.objectData.value;
            this.editDriverCode = item.objectData.code;
        } else {
            this.addDriverId = item.objectData.value;
            this.searchValue = item.objectData.text;
            this.addDriverCode = item.objectData.code;
        }

        this.selectedDriver = item.objectData.text;
    }

    private onSelectedByArrowKey(item: ISearchElement): void {
        if (this.isEdit) {
            this.editSearchValue = item.objectData.text;
            this.editDriverId = item.objectData.value;
            this.editDriverCode = item.objectData.code;
        } else {
            this.addDriverId = item.objectData.value;
            this.searchValue = item.objectData.text;
            this.addDriverCode = item.objectData.code;
        }
        this.selectedDriver = item.objectData.text;
    }

    public cancelEdit(schedule) {
        schedule.editing = false;
        this.isEdit = false;
    }

    private saveChange() {
        if (!this._canEditSchedule || this.firstEntry || !(this.hasModifications))
            return;

        if ((!this._schedulePlantDate && !this._schedulePlantDate.scheduleDetails) || this._schedulePlantDate.scheduleDetails.length === 0) {
            return;
        }

        this.hasModifications = false;

        if (this.isEdit || (this.schedulingId > 0 && this._schedulePlantDate.scheduleId)) {
            this._schedulePlantDate.scheduleDetails.forEach(schedule => {
                schedule.scheduleDetailTime = this.convertLocalToUTCFormat(schedule.scheduleDetailTime)
            })

            const scheduleRequest = {
                scheduleId: this._schedulePlantDate.scheduleId,
                plant: {
                    plantId: this._schedulePlantDate.plant.plantId
                },
                scheduleDateTime: this._schedulePlantDate.scheduleDateTime,
                scheduleDetails: []

            };

            for (const scheduleDetail of this._schedulePlantDate.scheduleDetails) {
                // just send modified or new rows
                if (scheduleDetail.crudOperation && scheduleDetail.crudOperation != undefined) {
                    const scheduleDetailRequest = {
                        crudOperation: scheduleDetail.crudOperation,
                        driver: {
                            driverId: scheduleDetail.driver.driverId
                        },
                        scheduleDetailTime: scheduleDetail.scheduleDetailTime,
                        scheduleNote: scheduleDetail.scheduleNote,
                        scheduleStatus: {
                            scheduleStatusId: scheduleDetail.scheduleStatus.scheduleStatusId
                        }
                    }

                    if (scheduleDetail.crudOperation !== 'C') {
                        scheduleDetailRequest['scheduleDetailId'] = scheduleDetail.scheduleDetailId;
                    }

                    scheduleRequest.scheduleDetails.push(scheduleDetailRequest);
                }
            }

            this.svrScheduling.updateSchedulingDetail(scheduleRequest).then((data) => {
                this.cleanVariables();
                this.getScheduleDetail(this.plantSelectedId);

            })
        } else {

            this._schedulePlantDate.scheduleDetails.forEach(schedule => {
                schedule.scheduleDetailTime = this.convertLocalToUTCFormat(schedule.scheduleDetailTime)
            })

            this._schedulePlantDate.plant.plantId = this.plantSelectedId;
            this.svrScheduling.saveSchedulingDetail(this._schedulePlantDate).then((data) => {
                this.cleanVariables();
                let plantSelected = this.plantSelected;
                let plantSelectedId = this.plantSelectedId;
                let objplantsend;
                if (data != undefined) {
                    this.schedulingId = data.scheduleHdrId;
                }
                this.getScheduleDetail(this.plantSelectedId);
            });
        }
    }

    public deleteSchedule(schedule: any) {

        if (this.isScheduleDetailExist(schedule.scheduleDetailId)) {
            schedule.crudOperation = 'D';
            this.hasModifications = true;
        } else {
            const index: number = this._schedulePlantDate.scheduleDetails.indexOf(schedule);
            this._schedulePlantDate.scheduleDetails.splice(index, 1);
        }

        this.cd.detectChanges();
    }

    public getLastSchedule() {
        this.svrScheduling.getLastSchedule(this.plantSelectedId).then(schedules => {
            if (schedules != undefined) {
                this.createlastschedule(schedules.schedules[0])
            }

        })
    }

    private createlastschedule(schedules: any): any {
        console.log('create last schedule', schedules)
        let schedule = {
            scheduleId: 0,
            scheduleDateTime: this.getToday(),
            plant: {
                plantId: this.plantSelectedId
            },
            scheduleDetails: [],
            originDetails: []
        };
        schedules.scheduleDetails.forEach(detail => {
            let time;
            if (detail.scheduleDetailTime.indexOf("T") > 0) {
                time = detail.scheduleDetailTime.split('T')[1];
            } else {
                time = detail.scheduleDetailTime;
            }

            let scheduleDetails = {
                editing: false,
                scheduleDetailId: 0,
                driver: {
                    driverCode: detail.driver.driverCode,
                    driverDesc: detail.driver.driverDesc,
                    driverId: detail.driver.driverId
                },
                scheduleNote: (detail.scheduleNote != undefined) ? detail.scheduleNote : "",
                scheduleDetailTime: this.formatUTCToLocalTime(time),
                scheduleStatus: {
                    scheduleStatusId: 1,
                    scheduleStatusDesc: 'Pending'
                },
                crudOperation: 'C'
            }
            schedule.scheduleDetails.push(scheduleDetails);
        });

        if (schedule.scheduleDetails.length > 0) {
            this.hasModifications = true;
            this.cd.detectChanges();
        }

        this._schedulePlantDate = schedule;
        if (this._currentSchedules)
            this._currentSchedules["schedules"] = schedule;
        this.complete = true;
    }

    private sendNotificacion() {
        if (!this._canEditSchedule || this.firstEntry || !(!this.hasModifications && this.pendingSchedule))
            return;

        if (this.complete) {
            this.svrScheduling.sendSchedule(this._schedulePlantDate).then((data) => {

                this.getScheduleDetail(this.plantSelectedId);

            })
        }
    }

    private isDisableAdd(): boolean {
        const result = (!this.addDriverTime || !this.addDriverCode || !this.selectedDriver || !this.addDriverId || !this.isValidNewHour || !this.searchValue) || !this._canEditSchedule;
        return result;
    }

    private validateTime(timeStr: string): boolean {
        const dateStr = moment(this.dateAux).format("YYYY-MM-DD");
        const scheduleTime = moment(`${dateStr} ${timeStr}`, "YYYY-MM-DD HH:mm").toDate();
        return !timeStr ? true : scheduleTime > new Date();
    }

    private validateTruckId(id: string): boolean { // method to validate length of truck id, currently not in use
        return (id.length > 1); 
    }

    private validationSaveTime(): boolean {
        const result = this.isEdit ? this.validateTime(this.editDriverTime) : this.validateTime(this.addDriverTime);
        return result;
    }

    private validateDateSchedule(): boolean {
        const fixedDateSchedule = new Date(this.dateAux);
        fixedDateSchedule.setHours(0, 0, 0, 0);
        const currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);
        return fixedDateSchedule >= currentDate;
    }

    private getFilterData(data: any[]) {
        this._schedulePlantDate.scheduleDetails = data;
    }

    private isScheduleDetailExist(scheduleDetailId: number): boolean {
        const found = this._schedulePlantDate.originDetails.filter(scheduleDetail => {
            return scheduleDetail.scheduleDetailId === scheduleDetailId;
        });

        return found.length > 0;
    }
}
