import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filterPlants',
    pure: false
})

export class FilterPlants implements PipeTransform {
    transform(items:any[], args):any[] {
        if(!args){
            return items;
        }
        if (typeof items === 'object') {
            return items.filter(obj => (obj.plantId).toString().includes(args));
        }
        else {
            return null;
        }

    }

}