import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'statusTruckKeys' })
export class StatusTruckPipe implements PipeTransform {

    transform(value, args1): any {
        let result = [];
        const tmpKeys = Object.keys(value);
        const enumKeys = tmpKeys.slice(0, tmpKeys.length / 2);
        for (const enumMember of enumKeys) {
            const nameKey = value[enumMember].toLowerCase().split("_").join(" ");
            result.push({ value: enumMember, name: nameKey });
        }
        return result;
    }
}
