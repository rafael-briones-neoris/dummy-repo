import { IApplicationMenu } from '@cemex-core/types-v2/dist/index.interface';

export interface IDriversAplicationMenu extends IApplicationMenu {
    menu: {
        menuId: number;
        menuTitle: string;
        menuEndpoint: string;
        menuItems: IDriversMenuItem[];
    }
}

export interface IDriversMenuItem {
    menuId: number;
    menuTitle: string;
    menuEndpoint: string;
    isModalEvent: boolean,
    menuValue: string,
    menuItems: IDriversMenuItem[];
}

