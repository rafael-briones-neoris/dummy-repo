import { IAddressDTO , PlantDTO } from '@cemex-core/types-v1/dist';

export interface ICustomerDTO {
    customerId: number;
    customerDesc: string;
}

export interface IDestinationDTO {
    destinationId: number;
    destinationDesc: string;
    isDeliverypoint:boolean;
    address:IAddressDTO;
}

export interface IDeliveryPointDTO {
    plant: PlantDTO;
    destination: IDestinationDTO;
    customer: ICustomerDTO;
}