export interface IOrderOptions {
    orderId: string;
    orderCode: string;
    ticketId: string;
    ticketCode: string;
    totalQuantity: number;
    um: string;
    volume: number;
    podName: string;
    clientName: string;
}

export class DriverOrder {
    orderId: string;
    orderCode: string;
    ticketId: string;
    ticketCode: string;
    totalQuantity: number;
    um: string;
    volume: number;
    podName: string;
    clientName: string;
    
    trucks:any[] = [];
    latitude: number = null;
    longitude: number = null;
    jobsiteId: string = null;
    jobsiteName: string = null;

    constructor(options?: IOrderOptions){
        if (options) {
            this.orderId = options.orderId;
            this.orderCode = options.orderCode;
            this.ticketId = options.ticketId;
            this.ticketCode = options.ticketCode;
            this.totalQuantity = options.totalQuantity;
            this.um = options.um;
            this.volume = options.volume;
            this.podName = options.podName;
            this.clientName = options.clientName;
        }
    }
}

export interface IDictOrder {
    [orderId: number]: DriverOrder;
}
