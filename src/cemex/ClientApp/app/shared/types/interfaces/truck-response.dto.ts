export enum EStatusChange {
    NEW,
    MODIFIED,
    MODIFIED_LOCATION,
    DELETED,
    NONE
}

export enum EAlertType {
    SPEEDING = 1,
    HOURS_OF_SERVICE = 2,
    WAITING_TIME = 3
}

export enum EStatusAlert {
    NO_VIEWED = 1,
    VIEWED = 2
}



export interface ICacheTruckEvent {
    [truckId: number]: any;
}

export interface ITruck {
    truckId: number;
    truckPlate: string;
    longitude?: number;
    latitude?: number;
}

export interface ICacheOrder {
    [truckId: number]: string;
}

export interface ICacheDetailTruck {
    truckObject: any;
    truckString: string;
}

export interface ICacheTruck {
    [truckId: number]: ICacheDetailTruck;
}

export interface IDictTruck {
    [truckId: number]: any;
}

export interface IChangeByTruck {
    truck: any;
    status: EStatusChange;
}

export interface IChangesTruck {
    [status: string]: IChangeByTruck;
}

export interface ITruckList {
    trucks: any[];
    changedTrucks: IChangesTruck;
}

export interface ITruckLocationOpt {
    basePath: string;
    clientId: string;
    environment: string;
}

export interface IPositionLatLng {
    lat: number;
    lng: number;
}

export interface IOriginDestinationTruck {
    origin: IPositionLatLng;
    destination: IPositionLatLng;
}

export interface IDriverApiResponse {
    ok?: boolean;
    code?: number;
    statusText?: string;
    bodyMessage?: string;
}

export interface ITruckEventResponse extends IDriverApiResponse {
    truckIds: string;
    dictTruck: IDictTruck
}

export interface ICacheAlert {
    [id:number]:any;
}
