import { IAddressDTO  } from '@cemex-core/types-v1/dist';

export interface IPointInterest{
   pointDesc:string;
   address:IAddressDTO;
   pointName:string;
   pointId:number;
}
