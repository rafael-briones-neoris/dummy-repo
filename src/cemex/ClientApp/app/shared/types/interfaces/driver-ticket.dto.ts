

export interface ITicketConsolidation {
    consolidationCode: string;
    totalTicketsConsolidated: number;
    ticketsConsolidated: {
        ticketId: number;
        ticketCode: string;
    }[]
}

export interface IResendTicketRequest {
    ticketId: string;
    driverId: string;
    timestamp: string
}
