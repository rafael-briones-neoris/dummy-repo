export enum ESearchMapType {
    TRUCK = 1,
    DRIVER = 2,
    ORDER = 3,
    POI = 4,
    PLANT = 5
}

export interface ISearchMapResult {
    type: ESearchMapType;
    objectId: number;
    title:string;
}

export interface ISearchTypeDict {
    [type: number]: string;
}
