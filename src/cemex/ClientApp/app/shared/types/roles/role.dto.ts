
export interface IRoleMenu {
    menuId: number;
    route: string;
}

export interface RoleRouteDict {
    [role: string]: IRoleMenu[]
}