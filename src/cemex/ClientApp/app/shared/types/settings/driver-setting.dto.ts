import { ITruckLocationOpt } from '../interfaces'

const APP_KEYS = {
    TRUCK_LOC_HOST_KEY: "TRUCK_LOC_HOST",
    TRUCK_LOC_ENV_KEY: "TRUCK_LOC_ENV",
};

export const TRUCK_LOCATION_OPTIONS: ITruckLocationOpt = {
    basePath : window[APP_KEYS.TRUCK_LOC_HOST_KEY],
    clientId: "",
    environment : window[APP_KEYS.TRUCK_LOC_ENV_KEY]
}
