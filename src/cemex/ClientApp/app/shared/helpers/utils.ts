import { DriversConstants } from '../constants/drivers.constants';
import * as sprintjs from 'sprintf-js';
//export * from './utils';

export class Utils {

    public static getCountryCodeFromUserInfo() {
        const userInfo = sessionStorage.getItem("userInfo");
        if (userInfo) {
            const userObject = JSON.parse(userInfo);
            if (userObject) {
                return userObject.country;
            }
        }
        return "MX";
    }

    public static getCountryCodeStandard(countryCode) {
        if (!countryCode) { return "mx"; }
        countryCode = countryCode.trim().toLowerCase();
        switch (countryCode) {
            case "us" || "usa": return "us";
            case "mx" || "mex": return "mx";
            default: return "mx";
        }
    }

    public static getLanguageByCountryCode(countryCode) {
        switch (this.getCountryCodeStandard(countryCode)) {
            case "us": return "en";
            case "mx": return "es";
            default: return "es";
        }
    }

    public static getLegalDocumentType(document: string): any {
        let doc = {
            type: '',
            label: ''
        };
        switch (document) {
            case 'legal': {
                doc.type = DriversConstants.typeTerms;
                doc.label = 'label.legal.terms_and_conditions';
                return doc;
            }
            case 'privacy': {
                doc.type = DriversConstants.typePolicy;
                doc.label = 'label.legal.privacy_policy';
                return doc;
            }
            case 'copyright': {
                doc.type = DriversConstants.typeCopyRights;
                doc.label = 'label.legal.copyright';
                return doc;
            }
            default: {
                doc.type = DriversConstants.typeTerms;
                doc.label = 'label.legal.terms_and_conditions';
                return doc;
            }
        }
    }

    public static isValidGeoplace(genericPoint: any) {
        return (genericPoint.address.geoPlace ? true : false) &&
            (genericPoint.address.geoPlace.latitude != 0) && (genericPoint.address.geoPlace.longitude != 0)
    }

    public static getDateFormat(): string {
        let format = "";
        let countryCode = this.getCountryCodeFromUserInfo();
        if (countryCode === "us") {
            format += "MMMM D, YYYY";
        }
        else {
            format += "D MMMM, YYYY";
        }
        return format;
    }

    public static getShortFormatAddress(t: any, address: any) {
        return this.getFormatAddress(t, address, 'short', "%(domicileNum)s %(streetName)s, %(postalCode)s");
    }

    public static getStandardFormatAddress(t: any, address: any) {
        return this.getFormatAddress(t, address, '', "%(streetName)s %(domicileNum)s, %(settlementDesc)s %(cityDesc)s %(regionCode)s, %(postalCode)s %(countryCode)s");
    }

    public static getFormatAddress(t: any, address: any, type: string, defaultFormat: string) {
        const userCountry = sessionStorage.getItem("country");
        const translateProperty = type.length > 0 ? '.' + type : '';
        const addressFormatResource = t.pt('views.address' + translateProperty + '.format');
        const localRegex = /\(([^()]*)\)/g;
        const allKeys = Object.keys(address);
        const addressFormat:string = addressFormatResource.startsWith('NOT:') ? defaultFormat : addressFormatResource;
        const groups = addressFormat.match(localRegex);
        
        for (const group of groups) {
            const fixedGroup = group.substr(1, group.length -2);
            const index = allKeys.indexOf(fixedGroup);
            if (allKeys.indexOf(fixedGroup) === -1) {
                address[fixedGroup] = '';
            }
        }

        return sprintjs.sprintf(addressFormat, address);
    }

    public static getLastSearchAddressValues(search: any, partialRequest: any) {
        let addresses: any[] = search.results[0].address_components;

        partialRequest.address.cityDesc = '';
        partialRequest.address.countryCode = '';
        partialRequest.address.domicileNum = '';
        partialRequest.address.postalCode = '';
        partialRequest.address.regionCode = '';
        partialRequest.address.regionDesc = '';
        partialRequest.address.settlementDesc = '';
        partialRequest.address.streetName = '';

        for (var a in addresses) {
            var item = addresses[a],
                t = item.types ? item.types[0] : '',
                long_name = item.long_name ? item.long_name : '',
                short_name = item.short_name ? item.short_name : '';
            switch (t) {
                case 'street_number':
                    partialRequest.address.domicileNum = long_name;
                    break;
                case 'route':
                    partialRequest.address.streetName = long_name;
                    break;
                case 'political':
                    partialRequest.address.settlementDesc = long_name;
                    break;
                case 'locality':
                    partialRequest.address.cityDesc = long_name;
                    break;
                case 'administrative_area_level_2':
                    break;
                case 'administrative_area_level_1':
                    partialRequest.address.regionDesc = long_name;
                    partialRequest.address.regionCode = short_name;
                    break;
                case 'country':
                    partialRequest.address.countryCode = short_name;
                    break;
                case 'postal_code':
                    partialRequest.address.postalCode = long_name;
                    break;
                default:
                    break;
            }
        }

        return partialRequest;
    }
}