import { Enums } from "./enums";

let _ = require("underscore");
export class Structures {

    static getScheduleDesignDetailsById(scheduleId: number) {
        let color = Enums.ScheduleStatus.Pending.Class;
        let tag = Enums.ScheduleStatus.Pending.Translation;
        for (var item of _.values(Enums.ScheduleStatus)) {
            if(scheduleId == item.Id) {
                color = item.Class;
                tag = item.TranslationTag;
                break;
            }
        }
        return {colorClass: color, translationTag: tag};
    }

}