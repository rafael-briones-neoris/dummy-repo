export class Enums {

    static ScheduleStatus : any = {
        Pending:    { Id: 1, Title: 'Pending',  Class: 'warning',   TranslationTag: 'views.schedule.status.pending' },
        Send:       { Id: 2, Title: 'Sent',     Class: 'validate',  TranslationTag: 'views.schedule.status.sent' },
        Viewed:     { Id: 3, Title: 'Viewed',   Class: 'seen',      TranslationTag: 'views.schedule.status.viewed' },
    }

}