import { DriversConstants } from './drivers.constants';
import { RoleRouteDict, IDriversAplicationMenu } from '../types/index.interface';

export  const ALL_MENU_ACCESS: IDriversAplicationMenu[] = [
    {
        applicationIcon: 'plant-selection-icon',
        applicationMenuId: 1,
        applicationPwdRecUrl: '',
        applicationTitle: 'Plan Selection',
        applicationUrl: 'app/map/plantSelection',
        applicationUsrHlpUrl: '',
        applicationUsrRegUrl: '',
        menu: null
    },
    // {
    //     applicationIcon: 'monitoring-icon',
    //     applicationMenuId: 2,
    //     applicationPwdRecUrl: '',
    //     applicationTitle: 'Monitoring',
    //     applicationUrl: 'app/monitoring',
    //     applicationUsrHlpUrl: '',
    //     applicationUsrRegUrl: '',
    //     menu: null
    // },
    {
        applicationIcon: 'tracking-icon',
        applicationMenuId: 3,
        applicationPwdRecUrl: '',
        applicationTitle: 'Tracking',
        applicationUrl: 'app/map/tracking',
        applicationUsrHlpUrl: '',
        applicationUsrRegUrl: '',
        menu: null
    },
    {
        applicationIcon: 'scheduling-icon',
        applicationMenuId: 4,
        applicationPwdRecUrl: '',
        applicationTitle: 'Scheduling',
        applicationUrl: 'app/scheduling',
        applicationUsrHlpUrl: '',
        applicationUsrRegUrl: '',
        menu: null
    },
    {
        applicationIcon: 'reports-icon',
        applicationMenuId: 5,
        applicationPwdRecUrl: '',
        applicationTitle: 'Reports',
        applicationUrl: '',
        applicationUsrHlpUrl: '',
        applicationUsrRegUrl: '',
        menu: {
            menuId: 1,
            menuTitle: "Reports menu",
            menuEndpoint: "",
            menuItems: [
                {
                    menuId: 1,
                    menuTitle: "Delivery Reports",
                    menuEndpoint: "app/reports/",
                    menuItems: null,
                    isModalEvent: false,
                    menuValue: null
                },
                {
                    menuId: 1,
                    menuTitle: "Map History",
                    menuEndpoint: "app/reports/map-history",
                    menuItems: null,
                    isModalEvent: false,
                    menuValue: null
                },
            ],
        }
    },
    {
        applicationIcon: 'geofences-icon',
        applicationMenuId: 6,
        applicationPwdRecUrl: '',
        applicationTitle: 'Geofences',
        applicationUrl: '',
        applicationUsrHlpUrl: '',
        applicationUsrRegUrl: '',
        menu: {
            menuId: 1,
            menuTitle: "Geofences",
            menuEndpoint: "",
            menuItems: [
                {
                    menuId: 1,
                    menuTitle: "Points of Delivery",
                    menuEndpoint: "app/geofences/delivery-list",
                    menuItems: null,
                    isModalEvent: false,
                    menuValue: null
                },
                {
                    menuId: 1,
                    menuTitle: "Points of Interest",
                    menuEndpoint: "app/geofences/poi-list",
                    menuItems: null,
                    isModalEvent: false,
                    menuValue: null
                },
                {
                    menuId: 1,
                    menuTitle: "Plants",
                    menuEndpoint: "app/geofences/plant-list",
                    menuItems: null,
                    isModalEvent: false,
                    menuValue: null
                },
            ],
        }
    }
]



export const RoleAccess: RoleRouteDict = {
    "Y": [{
        menuId: 3,
        route: DriversConstants.ROUTE_TRACKING_MAP
    }]
}

