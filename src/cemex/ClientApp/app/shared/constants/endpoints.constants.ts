export class EndpointsConstants{

    // base path
    protected BASEPATH = 'https://api.us2.apiconnect.ibmcloud.com/';

    // working environment
    protected ORG = 'cnx-gbl-org-development/';
    protected ENV = 'dev';

    // versions
    protected V1 = '/v1/';
    protected V2 = '/v2/';
    protected V3 = '/v3/';
    protected V4 = '/v4/';
    protected V5 = '/v5/';

    // capabilities
    protected armCapability     = 'arm/';
    protected legalCapability   = 'legal/';
    protected dsCapability      = 'ds/';
    protected rcCapability      = 'rc/';
    protected crmCapability     = 'crm/';
    protected cumCapability     = 'cum/';
    protected peCapability      = "pe/";
    protected repCapability     = "rep/";

    constructor(basepath: string, org: string, env: string) {
        if (basepath !== '')
            this.BASEPATH = basepath;

        if (org === 'quality') {
            this.setORG('cnx-gbl-org-quality/');
        } else if (org === 'production') {
            this.setORG('cnx-gbl-org-production/');
        }

        if (env === 'quality') {
            this.setENV('quality');
        } else if (env === 'production') {
            this.setENV('production');
        }
    }

    private setENV( value: string ){
        this.ENV = value;
    }

    private setORG( value: string ){
        this.ORG = value;
    }
}