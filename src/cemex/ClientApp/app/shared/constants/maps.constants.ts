export class MapConstants {
    static ICON_BASE: any = "/vendor/drivers/icon_map/";

    static ICONS: any = {
        TRUCK_RM: {
            icon: MapConstants.ICON_BASE + 'truck_RM.png',
            waiting: MapConstants.ICON_BASE + 'truck_RM/' + '_waiting_time.svg',
            speeding: MapConstants.ICON_BASE + 'truck_RM/' + '_speeding.svg',
            service: MapConstants.ICON_BASE + 'truck_RM/' + '_hours_of_service.svg',
        },
        TRUCK_CEMENT: {
            icon: MapConstants.ICON_BASE + 'truck_Cement.png',
            waiting: MapConstants.ICON_BASE + 'truck_Cement/' + '_waiting_time.svg',
            speeding: MapConstants.ICON_BASE + 'truck_Cement/' + '_speeding.svg',
            service: MapConstants.ICON_BASE + 'truck_Cement/' + '_hours_of_service.svg',
        },
        TRUCK_THIRD_PARTY_RM: {
            icon: MapConstants.ICON_BASE + 'truck_3rd_Party_RM.png',
        },
        TRUCK_THIRD_PARTY_CEMENT: {
            icon: MapConstants.ICON_BASE + 'truck_3rd_Party_Cement.png',
        },
        POD: {
            icon: MapConstants.ICON_BASE + 'POD.svg'
        },
        POI: {
            icon: MapConstants.ICON_BASE + 'POI.png'
        },
        JOBSITE: {
            icon: MapConstants.ICON_BASE + 'JOBSITE.svg'
        },
        HISTORY_ALERTS: {
            speedingIcon: MapConstants.ICON_BASE + 'history_Alerts/' + 'speeding.svg',
            hoursServiceIcon: MapConstants.ICON_BASE + 'history_Alerts/' + 'hours_of_service.svg',
            waitingTimeIcon: MapConstants.ICON_BASE + 'history_Alerts/' + 'waiting_time.svg',
        }
    };

    static DictTruckIcon = {
        "rmx": MapConstants.ICONS.TRUCK_RM,
        "cem": MapConstants.ICONS.TRUCK_CEMENT,
        "rmx-3rd" : MapConstants.ICONS.TRUCK_THIRD_PARTY_RM,
        "cem-3rd" : MapConstants.ICONS.TRUCK_THIRD_PARTY_CEMENT
    }
}