export class DriversConstants {
    
    static versionUrl = "/api/";
    static versionUrlV1 = "/v1/";
    static versionUrlV2 = "/v2/";
    static versionUrlV2_1 = "v2/";
    static versionUrlV3 = "/v3/";
    static versionUrlV4 = "v4/";

    static SCHEDULE_DRIVER_PATH = DriversConstants.versionUrlV4 + 'dtm/schedules?include=summary';    
    static TICKET_PATH = DriversConstants.versionUrlV4 + "dm/tickets?include=ticketitems,statuslog,shipment,driver,order&orderId=";
    //V4
    static JOBSITES_FAVORITE_PATH = DriversConstants.versionUrlV4 + "secm/jobsites/favorites";
    static JOBSITES_PATH = DriversConstants.versionUrlV4 + "sm/summary/jobsites/";
    static MARKER_PATH = DriversConstants.versionUrlV4 + "dtm/trucklocation/order/";
    static MY_PARAMETERS  = DriversConstants.versionUrlV4 + "ce/myparameters";
    
    // static SPEEDING_REP_PATH = PathConstants.envQa + PathConstants.versionUrlV4 + "dtm/schedules";
    static SPEEDING_REP_PATH = DriversConstants.versionUrlV4 + "rep/speedings";
    static WAITING_PATH = DriversConstants.versionUrlV4 + "rep/waitingtimes";
    static HOURS_PATH = DriversConstants.versionUrlV4 + "rep/hourofservices";
    static PERTRIPREP_PATH = DriversConstants.versionUrlV4 + "rep/pertripstatuses";
    static REPORDERS = DriversConstants.versionUrlV4 + 'rep/orders';
    static REPCUSTOMER = DriversConstants.versionUrlV4 + 'rep/customers'
    static EVENTS_REP_PATH = DriversConstants.versionUrlV4 + 'dtm/events'
    static TICKET_DETAIL = DriversConstants.versionUrlV4 + "dm/tickets"

    static SCHEDULE_DETAIL_DRIVER_PATH = DriversConstants.versionUrlV4 + 'dtm/schedules';
    static DRIVERS_PATH = DriversConstants.versionUrlV4 + 'dtm/drivers';
    // Drivers console
    static JUST_USER_INFO = DriversConstants.versionUrlV4 + "secm/myinfo";

    static PLANT_PATH = DriversConstants.versionUrlV4 + "secm/myinfo?include=plant";
    static PLANTS_PATH_PATCH = DriversConstants.versionUrlV4 + "secm/myinfo/plants";
    static NOTIFY_PATH = DriversConstants.versionUrlV4 + "cm/alerts";
    static JUST_NOTIFY_PATH_PATCH = DriversConstants.versionUrlV4 + "cm/alerts/viewed";

    static TERMS = DriversConstants.versionUrlV4 + "legal/terms";
    static PUBLIC_TERMS = DriversConstants.versionUrlV2_1 + "gm/terms";
    static TRUCKS_EVENT = "v4/dtm/trucks?include=event";
    static TRUCKS_LOCATION = "/api/v1/gd/trucklocations";
    static TRUCKS_LAST_LOCATION = "/api/v1/gd/trucklastlocations";
    static POINT_INTEREST_PATH = DriversConstants.versionUrlV4 + "gd/pointsofinterest";
    static POINT_INTEREST_ID_PATH = DriversConstants.versionUrlV4 + "gd/pointsofinterest/{pointInterestId}";
    static POD_PATH = DriversConstants.versionUrlV4 + "gd/deliverypoints";
    static JOBSITE_PATH = DriversConstants.versionUrlV4 + "dm/jobsites/{jobsiteId}/address";
    static DELIVERY_POINTS_PATH = DriversConstants.versionUrlV4 + "gd/deliverypoints/{deliveryPointId}/address";
    static PlANTS_MGMT_PATCH = DriversConstants.versionUrlV4 + "dm/plants/{plantId}/address";
    static TICKET_CONSOLIDATION = "v4/dm/tickets/consolidation";
    static TRUCK_UNLINK_DRIVER = "v4/dtm/trucks/%s/unassign";
    static MESSAGE_PUSH_PATH = "v2/rc/push/messages/%s/topics/%s";


    //GET parameters
    static platformCode = "Drivers_Console";
    static deviceCode = "NA";
    static typeTerms = "Drivers_GeneralUse";
    static typePolicy = "Drivers_PrivacyPolicy";
    static typeCopyRights = "Drivers_Copyright";

    // PUBLIC LEGAL DOCUMENT COUNTRIES
    static COUNTRY_NAME_USA = "USA";
    static COUNTRY_NAME_MEXICO = "Mexico";
    static COUNTRY_ISOCODE_USA = 848;
    static COUNTRY_ISOCODE_MEXICO = 484;

    static RESEND_MSG_CODE = "TKTCH001";
    static RESEND_TOPIC_CODE = "TKTCH";

    static ROLE_THIRD_PARTY = "Y";
    static ROUTE_TRACKING_MAP = "/app/map/tracking";

    static TIMEOUT_TRUCK_EVENT = 3000;
    static TIMEOUT_TRUCK_LOCATIONS = 6000;

    static DEFAULT_UNIT_OF_SPEED   = 123456;

    // MY PARAMETERS
    static UNIT_OF_SPEED                               = "km/h";
    static UNIT_OF_LENGTH_FOR_RADIUS                   = "m";
    static DEFAULT_SIZE_GEOFENCE_RADIUS                = "200";
    static DRIVER_CONSOLE_REFRESH_FREQUENCY_IN_MINUTES = ".5";
}

