import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CmxDialogV3 } from '@cemex/cmx-dialog-v3/dist';
import { TranslationService } from '@cemex-core/angular-services-v2/dist';

@Component({
    selector: 'app-legal-documents-dialog',
    templateUrl: './legal-documents-dialog.html',
    styleUrls: ['./legal-documents-dialog.scss']
})
export class LegalDocumentsDialogComponent implements OnInit {

    // Options to manipulate cmx-dialog
    @Input('options')
    options: any = {
        enableToClose: false,
        height: "550px",
        enableFooter: true
    };

    // Terms Title to display document name accordingly
    // Term Version item to display document
    private termsTitle: string = 'label.legal.terms_and_conditions';
    private termsSubtitle: boolean = false;
    private termVersionItem: any;

    // Accept and cancel event emitters
    @Output('onAcceptDialog') onAcceptDialog = new EventEmitter<any>();
    @Output('onCancelDialog') onCancelDialog = new EventEmitter<any>();

    // Terms Dialog Component
    @ViewChild('termsDialog') private termsDialog: CmxDialogV3;

    constructor(private t: TranslationService) { }

    ngOnInit() {

    }

    initDialog(termVersionItem: any, termsTitle: string): void {
        this.termsTitle = termsTitle;
        this.termsSubtitle = this.termsTitle.indexOf('label.legal.copyright') != -1;
        this.termVersionItem = termVersionItem;
        this.termsDialog.open();
    }

    closeDialog(): void {
        this.termsDialog.close();
    }

    acceptDialog(func: any): void {
        this.onAcceptDialog.emit(func);
    }

    cancelDialog(func: any): void {
        this.onCancelDialog.emit(func);
    }

    hasSubscribers(func: any[]): boolean {
        return func.length > 0;
    }

}