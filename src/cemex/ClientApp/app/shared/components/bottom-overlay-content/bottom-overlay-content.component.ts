import { Component, OnInit, AfterViewInit, ViewChild, Input } from '@angular/core';

@Component({
    selector: 'bottom-overlay-content',
    template: `
        <div class="bottom-overlay-content text-right" [class.opacity]='opacity'>
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ['./bottom-overlay-content.component.scss']

})
export class BottomOverlayContentComponent {

    @Input()
    public opacity: boolean = false;

    constructor() { }

}