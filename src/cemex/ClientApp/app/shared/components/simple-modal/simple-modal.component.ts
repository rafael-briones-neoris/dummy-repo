import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { CmxDialogV3 } from '@cemex/cmx-dialog-v3/dist';
import { TranslationService } from '@cemex-core/angular-services-v2/dist';

export interface ISimpleDialogButton {
    value: any;
    color?: string;
}

export interface ISimpleDialogOptions {
    [text: string]: ISimpleDialogButton;
}

export interface ISimpleDialogResult {
    isCancel: boolean;
    value: any;
}

@Component({
    selector: 'simple-modal',
    templateUrl: './simple-modal.component.html',
    styleUrls: ['./simple-modal.component.scss']
})
export class SimpleModalComponent {

    private _title: string = '';
    private _bodyMessage: string = '';
    private _showModal: boolean = false;
    @ViewChild(CmxDialogV3)
    private _dialogComponent: CmxDialogV3;
    private _resultSubject: Subject<ISimpleDialogResult> = new Subject<ISimpleDialogResult>();

    @Input()
    public get title(): string {
        return this._title;
    }

    public set title(value: string) {
        this._title = value;
    }

    @Input()
    public get bodyMessage(): string {
        return this._bodyMessage;
    }

    public set bodyMessage(value: string) {
        this._bodyMessage = value;
    }

    @Input()
    public buttonOptions: ISimpleDialogOptions = { "OK": { color: "green", value: true } };

    @Output()
    public onResultDialog: EventEmitter<any> = new EventEmitter<any>();

    public openModal(): Observable<ISimpleDialogResult> {
        if (this._dialogComponent !== undefined) {
            this._dialogComponent.open();

            return this._resultSubject.asObservable()
        }

        return null;
    }

    private getKeys() {
        return Object.keys(this.buttonOptions);
    }

    private closeResult(keyOption: string): void {
        this.onResultDialog.emit(this.buttonOptions[keyOption].value);
        this._resultSubject.next({
            isCancel: false,
            value: this.buttonOptions[keyOption].value
        });

        this._resultSubject.complete();
        this._dialogComponent.close();
    }

    private onCancel():void{
        this._resultSubject.next({
            isCancel: true,
            value: null
        });

        this._resultSubject.complete();
    }

}