import { Component, Input, Output, EventEmitter } from "@angular/core";
import { TranslationService } from "@cemex-core/angular-services-v2/dist";


export type SorterCompareFn = (objectLeft: any, objectRight: any) => number;
export type SearchFilterFn = (collection:any[], searchValue:string) => any[];

@Component({
    selector: 'filter-sorter',
    styleUrls: ['./filter-sorter.component.scss'],
    templateUrl: './filter-sorter.component.html',
})
export class FilterSorterComponent {
    private _searchValue: string = "";
    private _filteredCollection: any[] = [];
    private _originalCollection: any[];
    private _compareCustomFn: SorterCompareFn = null;
    private _searchFilterFn:SearchFilterFn = null;
    private _isAscending: boolean = false;
    private _isDescending: boolean = false;

    @Input()
    public get collection(): any[] {
        return this._filteredCollection;
    }

    public set collection(value: any[]) {
        this._filteredCollection = value;
        if (value && value !== undefined) {
            this._originalCollection = value.slice(0, value.length);
        }
    }

    @Input()
    public set sorterCustomFn(value: SorterCompareFn) {
        this._compareCustomFn = value;
    }

    @Input()
    public set searchFilterFn(value: SearchFilterFn) {
        this._searchFilterFn = value;
    }

    @Input() public placeholder: string;
    @Input() public keyTitle: string;
    @Input() public keySubtitle: string;
    @Output() public onSelectedFilterItem: EventEmitter<any> = new EventEmitter<any>();
    @Output() public isfiltered: EventEmitter<any> = new EventEmitter<any>();

    constructor(private translationService: TranslationService) {

    }

    public ngOnChanges(changes) {
    }

    public clickInside(event) {
        event.stopPropagation();
    }

    private reset() {
        this._searchValue = "";
        this._isAscending = false;
        this._isDescending = false;
        this._filteredCollection = this._originalCollection.slice(0, this._originalCollection.length);
        this.onSelectedFilterItem.emit(this._filteredCollection);
        this.isfiltered.emit(false);
    }

    private ascendingItem() {
        this._isAscending = !this._isAscending;
        this._isDescending = false;
        this.orderAZ();
    }

    private descendingItem() {
        this._isAscending = false;
        this._isDescending = !this._isDescending;
        this.orderAZ();
    }

    // tslint:disable-next-line:typedef
    private orderAZ() {
        if (!this._compareCustomFn) {
            return;
        }

        const isAZ = this._isAscending || this._isDescending;
        if (isAZ) {
            this._filteredCollection = this._isAscending ? this._filteredCollection.sort(this._compareCustomFn) : this.reverseSortCollection(this._filteredCollection);
        } else {
            this._filteredCollection = this._searchValue !== '' ? this._filteredCollection.slice(0, this._originalCollection.length) : this._originalCollection.slice(0, this._originalCollection.length);
        }

        this.onSelectedFilterItem.emit(this._filteredCollection);
        this.isfiltered.emit(true);
    }

    public onSearch(event) {
        const isAZ = this._isAscending || this._isDescending;
        this._filteredCollection = this._searchFilterFn(this._originalCollection, event.currentTarget.value);

        if (isAZ) {
            this.orderAZ();
        } else {
            this.onSelectedFilterItem.emit(this._filteredCollection);
        }

        this.isfiltered.emit(this._searchValue !== '');
    }

    private reverseSortCollection(collection: any[]): any[] {
        const result = collection.sort(this._compareCustomFn);
        return result.reverse();
    }

}