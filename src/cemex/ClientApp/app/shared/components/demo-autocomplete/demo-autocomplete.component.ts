import {
    Component, OnInit, Input, Output, Injectable
} from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ISearchElement, ISearchInputService } from '../cmx-input-autocomplete/index';

@Injectable()
export class CustomService implements ISearchInputService {

    executeService(search: string): Observable<ISearchElement[]> {
        //const test: Subject<ISearchElement[]> = new Subject<ISearchElement[]>();
        const serviceElements: ISearchElement[] = [
            {
                displayData: 'service option1',
                objectData: {
                    text: 'service option1',
                    value: 1
                }
            },
            {
                displayData: 'service large option2*******',
                objectData: {
                    text: 'service option2',
                    value: 2
                }
            },
            {
                displayData: 'service option3',
                objectData: {
                    text: 'service option3',
                    value: '003'
                }
            },
            {
                displayData: 'service option_3',
                objectData: {
                    text: 'service option_3',
                    value: '_003'
                }
            },
            {
                displayData: 'service option_4',
                objectData: {
                    text: 'service option_4',
                    value: '_004'
                }
            },
            {
                displayData: 'service option_5',
                objectData: {
                    text: 'service option_5',
                    value: '_005'
                }
            }
        ];

        const result = serviceElements.filter(elem => {
            const lowerData = elem.displayData.toLowerCase();
            const lowerSearchValue = search.toLowerCase();
            return elem.displayData.indexOf(lowerSearchValue) > -1;
        })

        return new BehaviorSubject<ISearchElement[]>(result).asObservable();
    }
}

@Component({
    styleUrls: ['demo-autocomplete.component.scss'],
    templateUrl: './demo-autocomplete.component.html',
})
export class DemoAutocompleteComponent {

    private _resultData: ISearchElement[] = [];
    private _inputValue:string;
    
    constructor(private _myservice: CustomService) {
        console.log('_myservice:', _myservice);
    }

    private getSearchResults(result: ISearchElement[]): void {
        this._resultData = result;
    }

    private onClickItem(item: ISearchElement): void {
        console.log('printing clicked item:', item);
    }

    private onSelectElementEvent(item: ISearchElement): void {
        console.log('printing selected item by enter key:', item);
    }

}
