import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CmxInputAutoCompleteModule } from './../cmx-input-autocomplete';
import { DemoAutocompleteComponent, CustomService } from './demo-autocomplete.component';

const ROUTES: Routes = [
    {
        path: '', component: DemoAutocompleteComponent,
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CmxInputAutoCompleteModule,
        RouterModule.forChild(ROUTES)
    ],
    declarations: [
        DemoAutocompleteComponent
    ],
    exports: [

    ],
    providers: [
        CustomService
    ]
})
export class DemoAutoCompleteModule { }
