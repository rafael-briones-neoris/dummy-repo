import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CmxInputAutoCompleteComponent } from './cmx-input-autocomplete.component';
import { CmxAutoCompleteItemComponet} from './cmx-autocomplete-item.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        CmxInputAutoCompleteComponent,
        CmxAutoCompleteItemComponet
    ],
    exports: [
        CmxInputAutoCompleteComponent,
        CmxAutoCompleteItemComponet
    ],
})
export class CmxInputAutoCompleteModule { }
