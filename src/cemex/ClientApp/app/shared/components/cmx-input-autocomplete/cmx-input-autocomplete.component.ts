import {
    Component, OnInit, Input, Output, EventEmitter,
    ViewChild, ElementRef, Renderer, AfterViewInit,
    forwardRef 
} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { ISearchElement, ISearchInputService } from './cmx-autocomplete-utils';

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CmxInputAutoCompleteComponent),
    multi: true
};

@Component({
    selector: 'cmx-input-autocomplete',
    styleUrls: ['cmx-input-autocomplete.component.scss'],
    templateUrl: './cmx-input-autocomplete.component.html',
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR] 
})
export class CmxInputAutoCompleteComponent implements AfterViewInit, ControlValueAccessor {

    private _showItems: boolean = false;
    private _disabled: boolean;
    private _sourceElements: ISearchElement[] = [];
    private _serviceElements: ISearchElement[] = [];
    private _filteredElements: ISearchElement[] = [];
    private _activeInput: boolean = false;
    private _searchService: ISearchInputService;
    private _searchServiceSubscription: Subscription;
    private _currentIndexSearch: number = -1;
    private _activeSearchResult: ISearchElement;
    public _searchValue: string;
    private _placeholder: string;
    private _numCaracTrigger: number = 4;
    private _maxLength: number = 15;

    //Placeholders for the callbacks which are later providesd
    //by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;


    @ViewChild('searchInput')
    private searchInput: ElementRef;
    // Inputs
    @Input()
    set maxLength(value: number) {
        if (value) {
            this._maxLength = value;
        }
    }
    get maxLength(): number {
        return this._maxLength;
    }

    @Input()
    set placeholder(value: string) {
        if (value !== undefined) {
            this._placeholder = this.truncate(value);
        }
    }

    get placeholder(): string {
        return this._placeholder;
    }

    private set searchValue(value: string) {
        if (value !== this._searchValue) {
            this._searchValue = value;
            this.onChangeCallback(value);
        }
    }

    private get searchValue(): string { 
        return this._searchValue;
    }

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(value: boolean) {
        var coerceBoolean = (value != null && `${value}` !== 'false');
        this._disabled = coerceBoolean;
    }

    @Input()
    public set searchService(value: ISearchInputService) {
        this._searchService = value;
    }

    @Input()
    set numCaracTrigger(value: number) {
        this._numCaracTrigger = value;
    }

    @Output()
    public onSelectElementEvent: EventEmitter<ISearchElement> = new EventEmitter<ISearchElement>();

    @Output()
    public onSearchResults: EventEmitter<ISearchElement[]> = new EventEmitter<ISearchElement[]>();

    @Output()
    public selectedByArrowKey: EventEmitter<ISearchElement> = new EventEmitter<ISearchElement>();

    constructor(private _renderer: Renderer) { }

    public ngAfterViewInit(): void {
        
        if (this.searchInput != undefined) {
            const inputStream = Observable.fromEvent(this.searchInput.nativeElement, 'keyup')
                .debounceTime(1)
                .distinctUntilChanged();

            inputStream.subscribe((event: any) => {
                if (event.code == 'Enter' && event.target.value && event.target.value != '') {
                    if (this._activeSearchResult) {
                        this.onSelectItem(this._activeSearchResult);
                        this._activeSearchResult = null;
                        return;
                    }

                } else if (event.code == 'ArrowDown' && this._filteredElements.length > 0) {
                    this._currentIndexSearch++;
                    this._currentIndexSearch = this._currentIndexSearch >= this._filteredElements.length ? 0 : this._currentIndexSearch;
                    this._activeSearchResult = this._filteredElements[this._currentIndexSearch];
                    this._searchValue = this._activeSearchResult.displayData;
                    this.selectedByArrowKey.emit(this._activeSearchResult);
                } else if (event.code == 'ArrowUp'&& this._filteredElements.length > 0) {
                    this._currentIndexSearch--;
                    this._currentIndexSearch = this._currentIndexSearch < 0 ? this._filteredElements.length - 1 : this._currentIndexSearch;
                    this._activeSearchResult = this._filteredElements[this._currentIndexSearch];
                    this._searchValue = this._activeSearchResult.displayData;
                    this.selectedByArrowKey.emit(this._activeSearchResult);
                } else if (event.code === 'Escape' || event.code === 'Tab') {
                    // this._searchValue = '';
                    this._showItems = false;
                    //this._resultSearchList = [];
                }  else {
                    const searchValue: string = event.target.value;
                    this._searchValue = searchValue;
                    if (searchValue === '') {
                        this._searchValue = null;
                        this._sourceElements = [];
                        this._filteredElements = [];
                        this._showItems = false;
                    } else {
                        const greaterNumTrigger = searchValue.length >= this._numCaracTrigger;
                        //const equalNumTrigger = this._numCaracTrigger > 0 ? searchValue.length === this._numCaracTrigger : true;
                        //const emptySorce =  === 0;

                        if (this._searchService && (greaterNumTrigger && (!this._sourceElements.length) )) {
                            const result = this._searchService.executeService(searchValue);
                            if(result){
                                this._searchServiceSubscription = result.subscribe(
                                    result => {
                                        this._sourceElements = result;
                                        this._currentIndexSearch = -1;
                                        this._filteredElements = this._sourceElements.slice(0, this._sourceElements.length);
                                        this._activeSearchResult = null;
                                        this._showItems = this._filteredElements.length > 0;
                                        this.onSearchResults.emit(this._filteredElements);
                                    }
                                );
                            }
                        } else {
                            this._filteredElements = this._sourceElements.filter(elem => {
                                const lowerData = elem.displayData.toLowerCase();
                                const lowerSearchValue = searchValue.toLowerCase();
                                const position = lowerData.indexOf(lowerSearchValue);
                                return position > -1;
                            });

                        }
                        
                        this.onSearchResults.emit(this._filteredElements);
                        this._showItems = this._filteredElements.length > 0;
                    }

                }
            });
        }
    }

    private toggle(): void {

        if (!this.disabled && this._filteredElements.length > 0) {
            this._showItems = !this._showItems;
            const bodyElement = document.getElementsByTagName('BODY')[0];
            if (this._showItems) {
                this._renderer.setElementStyle(bodyElement, 'overflow', 'hidden');
            }
            else {
                this._renderer.setElementStyle(bodyElement, 'overflow', undefined);
            }
        }
    }

    private onSelectItem(item: ISearchElement): void {
        this._searchValue = item.displayData;
        this.onSelectElementEvent.emit(item);
        this._showItems = false;
    }

    private truncate(value: string): string {
        const trail = '...';
        return (value.length > this._maxLength) ? value.substring(0, this._maxLength) + trail : value;
    }

    private checkClose($event: Event):void{
        $event.stopPropagation();
        this._showItems = false;
    }

     //Set touched on blur
    public onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    public writeValue(value: any) {
        if (value !== this._searchValue) {
            this._searchValue = value;
        }
    }

    //From ControlValueAccessor interface
    public registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    public registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    private onKey(event: any) { // without type info
        if (event.code === 'Tab') {
            this._showItems = false;
        }
    }
    
    public reset(){
        this._searchValue = "";
        this._searchValue = null;
        this._sourceElements = [];
        this._filteredElements = [];
        this._showItems = false;
        this._currentIndexSearch = -1;
    }
}
