import { Observable } from 'rxjs/Observable';

export interface ISearchElement {
    displayData: string;
    objectData: any
}

export interface ISearchInputService {
    executeService(searchValue:string):Observable<ISearchElement[]>;
}

export enum ETypeSource {
    IN_LINE = 1,
    INPUT_SOURCE = 2,
    INPUT_SERVICE = 3
}