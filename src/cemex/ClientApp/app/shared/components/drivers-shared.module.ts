import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CmxSidebarDrivers } from "./cmx-sidebar-drivers";
import { FooterComponent } from "./footer/footer.component";
import { CmxTableModule } from "@cemex/cmx-table-v2/dist";
import { CmxCheckboxModule } from "@cemex/cmx-checkbox-v1/dist";
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v1/dist';

import { CmxInputModule } from '@cemex/cmx-input-v1/dist'
import { CmxDialogV3 } from "@cemex/cmx-dialog-v3/dist";
import { CmxButtonModule } from '@cemex/cmx-button-v1/dist';

import { FilterSorterComponent } from "./cemex-filter-content/filter-sorter.component";
import { LegalDocumentsDialogComponent } from "./legal-documents-dialog/legal-documents-dialog.component";
import { SimpleModalComponent } from "./simple-modal";
import { RouterModule } from "@angular/router";
import { BottomOverlayContentComponent } from "./bottom-overlay-content/bottom-overlay-content.component";

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        CmxTableModule,
        CmxCheckboxModule,
        FlexLayoutModule,
        CmxDropdownModule,
        CmxButtonModule,
        RouterModule
    ],
    declarations: [
        CmxSidebarDrivers,
        FooterComponent,
        FilterSorterComponent,
        LegalDocumentsDialogComponent,
        CmxDialogV3,
        SimpleModalComponent,
        BottomOverlayContentComponent
        
    ],
    exports: [
        FormsModule,
        CmxSidebarDrivers,
        FooterComponent,
        FilterSorterComponent,
        CmxTableModule,
        CmxCheckboxModule,
        FlexLayoutModule,
        CmxDropdownModule,
        CmxInputModule,
        LegalDocumentsDialogComponent,
        CmxDialogV3,
        SimpleModalComponent,
        CmxButtonModule,
        BottomOverlayContentComponent
    ],
    providers: [],
    entryComponents: []
})
export class DriversSharedModule { }