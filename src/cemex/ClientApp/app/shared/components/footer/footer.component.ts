import { Component, OnInit, Input, OnChanges, Output, EventEmitter, ViewChild } from '@angular/core';
import { LegalDocumentsDialogComponent } from '../legal-documents-dialog/legal-documents-dialog.component';
import { DriversCoreService } from '../../services/drivers-core.service';
import { TranslationService } from '@cemex-core/angular-services-v2/dist';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['footer.component.scss'],
})

export class FooterComponent implements OnInit {

    // Legal document event emitters
    @Output('onLegalDocumentClicked') onLegalDocumentClicked = new EventEmitter<any>(); 
    @Input('clickableCopyright') clickableCopyright: boolean = true;

    constructor(private t: TranslationService){
    }

    ngOnInit() {
    }

    documentClicked(type: string){
        const enableFooter = false;
        this.onLegalDocumentClicked.emit({type, enableFooter});
    }
    
}