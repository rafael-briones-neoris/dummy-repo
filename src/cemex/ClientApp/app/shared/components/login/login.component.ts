import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CmxLoginModule } from '@cemex/cmx-login-v1/dist';

@Component({
    selector: 'app-login',
    templateUrl: './login.html',
    styleUrls: ['./login.scss']
})
export class LoginComponent implements OnInit {

    constructor(private router: Router) { }

    ngOnInit() {
        
    }

    private onLegalDocumentClicked(params: any){
        if(params.type === 'legal'){
            this.router.navigate(['legal/terms']);
        }
        else if(params.type === 'privacy'){
            this.router.navigate(['legal/privacy']);
        }
    }

}