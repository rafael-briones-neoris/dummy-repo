import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { CmxCoreCommonModule } from '@cemex-core/angular-services-v2/dist';
import { CmxLoginModule } from '@cemex/cmx-login-v1/dist';
import { DriversSharedModule } from '../index';


const ROUTES: Routes = [
    {
        path: '', component: LoginComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CmxCoreCommonModule,
        CmxLoginModule,
        DriversSharedModule
    ],
    declarations: [
        LoginComponent,
    ],
    providers: [],
    entryComponents: []
})
export class LoginModule { }

