import {
    Component, OnInit, OnChanges, Input, ElementRef,
    Output, EventEmitter, HostListener, Directive
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { Observable } from 'rxjs/Observable';

import { SessionService, TranslationService } from '@cemex-core/angular-services-v2/dist';
import {
    IApplicationMenu, IAppMenuItem,
    IUserProfile, ICustomer, ILegalEntity
} from '@cemex-core/types-v2/dist/index.interface';

import { Broadcaster } from '@cemex-core/events-v1/dist';

@Directive({
    selector: '[clickOutside]',
})
export class ClickOutsideDirective {
    constructor(private _elementRef: ElementRef) { }

    @Output()
    public clickOutside = new EventEmitter();

    @HostListener('document:click', ['$event.target'])
    public onClick(targetElement: any) {
        const clickedInside = this._elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.clickOutside.emit(null);
        }
    }
}

export interface IUserLanguage {
    languageId: string;
    isSelected: boolean;
    displayName: string;
}

export interface ICustomOption {
    iconId: string;
    labelOption: string;
    subOptions: ICustomSubOption[]
}

export interface ICustomSubOption {
    idOption: number;
    labelOption: string;
}

@Component({
    selector: 'cmx-sidebar-drivers',
    templateUrl: './cmx-sidebar-drivers.component.html',
    styleUrls: ['./cmx-sidebar-drivers.component.scss'],
})

export class CmxSidebarDrivers implements OnInit {

    private _clickedLink: IAppMenuItem;
    private _fullNameUser: string = '';
    private _isCollapsed: boolean = true;
    private _watcher: Subscription;
    private _isMobile: boolean;
    private _isMobilePhone: boolean;
    private _menuItems: IApplicationMenu[];
    private _clickedApp: IApplicationMenu;
    private _initials: string;
    private _userProfile: IUserProfile;
    private _legalEntityList: ILegalEntity[] = [];
    private _showLegalEntity: boolean = false;
    private _currentLegalEntity: ILegalEntity;
    private _searchLegalEntity: string;
    private _toggleLanguage: boolean = false;
    private _languages: IUserLanguage[] = [];
    private _currentLang: IUserLanguage;
    private get _showFaded(): boolean {
        return this._isMobilePhone && !this._isCollapsed;
    }
    private _customOption: ICustomOption;
    private _toggleCustomOptions: boolean = false;
    private _mapIcon: {
        [index: string]: string;
    };
    /* new state variables */
    private _customMenuItems: IApplicationMenu[] = [];
    private _offLegalEntity: boolean;

    @Output()
    public collapseEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Input()
    set isCollapsed(value: boolean) {
        this._isCollapsed = value;
        this.collapseEvent.emit(this._isCollapsed);
    }

    get isCollapsed(): boolean {
        return this._isCollapsed;
    }

    @Output()
    private clickMenuOption: EventEmitter<string> = new EventEmitter<string>();

    @Output()
    private customOptionEvent: EventEmitter<ICustomSubOption> = new EventEmitter<ICustomSubOption>();

    // Legal document event emitters
    @Output('onLegalDocumentClicked')
    private onLegalDocumentClicked = new EventEmitter<any>(); 

    @Input()
    public get customOptions(): ICustomOption {
        return this._customOption;
    }
    public set customOptions(value: ICustomOption) {
        this._customOption = value;
    }

    @Input()
    public set customMenuItems(value: IApplicationMenu[]) {
        this._customMenuItems = value;
    }

    @Input()
    public set offLegalEntity(value: boolean) {
        this._offLegalEntity = value;
    }

    public get initials(): string {
        if (this._userProfile && this._userProfile.firstName.length > 0) {
            const lastName = this._userProfile.lastName.length > 0 ?
                this._userProfile.lastName.charAt(0).toUpperCase() : '';
            return `${this._userProfile.firstName.charAt(0).toUpperCase()}${lastName}`;
        }

        return ' ';
    }

    constructor(
        private sessionService: SessionService,
        private eref: ElementRef,
        public media: ObservableMedia,
        private router: Router,
        private translationService: TranslationService,
        private eventBroadcaster: Broadcaster
    ) {
        this._watcher = media.subscribe((change: MediaChange) => {
            this._isMobile = this.media.isActive('lt-md');
            this._isMobilePhone = this.media.isActive('lt-sm');
        });
    }

    public ngOnInit() {
        this._isCollapsed = this.media.isActive('lt-md');
        const currentLang: string = (localStorage.getItem('language') || 'en');
        this._languages = [
            {
                languageId: 'en',
                displayName: 'English',
                isSelected: false
            },
            {
                languageId: 'es',
                displayName: 'Spanish',
                isSelected: false
            }
        ];

        for (let lang of this._languages) {
            if (lang.languageId === currentLang) {
                lang.isSelected = true;
                this._currentLang = lang;
            }
        }

        if (this._customMenuItems.length > 0) {
            this._menuItems = this._customMenuItems;
        } else {
            this.sessionService.menuApplicationItems.subscribe(
                (result: IApplicationMenu[]) => {
                    this._menuItems = result;
                }
            );
        }

        if (!this._offLegalEntity) {
            this.sessionService.getLegalEntities().subscribe(
                (result: ILegalEntity[]) => {
                    this._legalEntityList = result;
                    if (this._legalEntityList.length > 0) {
                        this._currentLegalEntity = this._legalEntityList[0];
                        const sessionLegalEntity: string = sessionStorage.getItem('user_legal_entity');
                        if (sessionLegalEntity && sessionLegalEntity !== undefined) {
                            try {
                                const tmp = JSON.parse(sessionLegalEntity);
                                this._currentLegalEntity = tmp as ILegalEntity;
                            } catch (e) {
                                const message = `Exception at parsing user_legal_entity from sessionStore:`;
                                console.warn(message, e);
                            }
                        }
                        this.sessionService.setLegalEntity(this._currentLegalEntity);
                    }

                },
                (error: any) => {
                    console.error('An error occurred at getting legal entities!', error);
                }
            );
        }

        this._userProfile = this.sessionService.userProfile;
        if (this._userProfile) {
            this._fullNameUser = `${this._userProfile.firstName} ${this._userProfile.lastName}`;
        }
    }

    private onApplicationItemClick(itemClicked: IApplicationMenu) {
        if (this._clickedApp != itemClicked)
            this._clickedApp = itemClicked;

        // if it's using menu items from Input instead of API
        if (this._customMenuItems && this._customMenuItems.length > 0) {
            this.clickMenuOption.emit(itemClicked.applicationTitle);
            if(itemClicked.applicationUrl !== "") { 
                this.router.navigate([itemClicked.applicationUrl]);
            }
        } else {
            // Using menu items from API
            if (this.isCollapsed || !itemClicked.menu || (itemClicked.menu.menuItems.length == 0)) {
                this.clickMenuOption.emit(itemClicked.applicationTitle);
                //console.log("location to:", itemClicked.applicationUrl);
                this.isCollapsed = this._isMobile ? !this.isCollapsed : this.isCollapsed;
                if(itemClicked.applicationUrl !== "") { 
                    window.location.href = itemClicked.applicationUrl;
                }
            }
        }
    }

    private onSubItemClick(subItem: any) {
        if (this._clickedLink != subItem)
            this._clickedLink = subItem;

        if(subItem.isModalEvent) {
            this.documentClicked(subItem.menuValue);
        }
        else {
            this.clickMenuOption.emit(subItem.menuTitle);
            this._isCollapsed = this._isMobile ? !this._isCollapsed : this._isCollapsed;
            const pathSubItem = this.getMainPath(subItem.menuEndpoint);
            const isLocalHost = window.location.hostname.indexOf('localhost') > -1;
            // to know if it's on other application do a redirect
            if (this.getMainPath(window.location.pathname) === pathSubItem || isLocalHost) {
                this.router.navigate([subItem.menuEndpoint]);
            } else {
                window.location.href = subItem.menuEndpoint;
            }
        }

    }

    @HostListener('document:click', ['$event'])
    private onClick(event: any) {
        if (!this.eref.nativeElement.contains(event.target)) {
            // this._clickedApp = null;
        }
    }

    private toggleSidebar() {
        this.isCollapsed = !this._isCollapsed;
        this._showLegalEntity = this._isCollapsed ? false : this._showLegalEntity;
    }

    private getIdIcon(applicationIcon: string): string {
        return `#${applicationIcon}`;
    }

    private getArrowIcon(): string {
        return this._showLegalEntity ? '#icon-down-single' : '#icon-right-single';
    }

    private clickLegalEntity(item: ILegalEntity) {
        this._currentLegalEntity = item;
        this._searchLegalEntity = '';
        this.sessionService.setLegalEntity(item);

    }

    private doSignOut(): void {
        this.sessionService.logout();
        window.location.href = "/";
    }

    private isActiveLegalEntity(item: ILegalEntity): boolean {
        return this._currentLegalEntity.legalEntityId === item.legalEntityId;
    }

    private outsideLanguage(): void {
        this._toggleLanguage = false;
    }

    private setLanguage(lang: IUserLanguage) {
        this._currentLang = lang;
        this._languages.filter(lang => {
            lang.isSelected = lang.languageId == lang.languageId;
        });
        localStorage.setItem('language', lang.languageId);
        this.translationService.lang(lang.languageId);
        this.eventBroadcaster.broadcast(Broadcaster.DCM_LANGUAGE_CHANGE, lang.languageId);
    }

    private getCheckIcon(lang: IUserLanguage): string {
        return lang.languageId === this._currentLang.languageId ? "#icon-check-lang" : "";
    }

    private getMainPath(path: string): string {
        const arr = path.split('/');
        return path.charAt(0) == '/' ? arr[1] : arr[0];
    }

    private outsideCustomOption(): void {

    }

    private clickSubOption(item: ICustomSubOption) {
        this.isCollapsed = true;
        this.customOptionEvent.emit(item);
    }

    public setCurrentLegalEntityById(value: number): boolean {
        const localLegalEntity: ILegalEntity[] = this._legalEntityList.filter(item => {
            return item.legalEntityId === value;
        });

        if (localLegalEntity.length > 0) {
            this.clickLegalEntity(localLegalEntity[0]);
        }

        return localLegalEntity.length > 0;
    }

    public getLegalEntityById(value: number): ILegalEntity {
        //console.log('searching for:', value);
        const localLegalEntity: ILegalEntity[] = this._legalEntityList.filter(item => {
            return item.legalEntityId == value;
        });

        return localLegalEntity.length > 0 ? localLegalEntity[0] : null;
    }

    private getToggleIcon(isCollapsed:boolean):string {
        return isCollapsed ? '#right-double-icon' : '#left-double-icon';
    }

    private documentClicked(type: string){
        const enableFooter = true;
        this.onLegalDocumentClicked.emit({type, enableFooter});
    }
}
