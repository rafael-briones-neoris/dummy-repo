import { Injectable } from '@angular/core';
export interface IServiceOptions {
    basePath: string;
    contextPath: string;
    envPath: string;
    clientId?: string;
    appCode: string;
    app_Code: string;
    countlyKey?:string;
    countlyUrl?:string;
}

export class SecServiceOptions implements IServiceOptions {
    basePath: string;
    contextPath: string;
    envPath: string;
    clientId?: string;
    appCode: string;
    app_Code: string;


    constructor(options?: IServiceOptions) {
        if (options) {
            this.basePath = options.basePath;
            this.app_Code = options.app_Code;
            this.appCode = options.appCode;
            this.clientId = options.clientId;
            this.contextPath = options.contextPath;
            this.envPath = options.envPath;
        }


    }
}