import { Injectable } from '@angular/core';
import { IServiceOptions } from './options.service';


export interface IProjectEnvironmentService {
    getWorkingPath(): string;
    getEndPoint(apiUrl: string, host?: string);

    clientId:string;
    appCode:string;
    app_Code:string;
    countlyKey:string;
    countlyUrl:string;
}


@Injectable()
export class ProjectEnvironmentService implements IProjectEnvironmentService {
    constructor(private serviceOptions: IServiceOptions) {

    }

    public getWorkingPath(): string {
        console.log("Path:.... " + `${this.serviceOptions.basePath}${this.serviceOptions.contextPath}${this.serviceOptions.envPath}`);
        return `${this.serviceOptions.basePath}${this.serviceOptions.contextPath}${this.serviceOptions.envPath}`;
    }

    public getEndPoint(apiUrl: string, host?: string): string {
        return host ? `${host}${apiUrl}` : `${this.getWorkingPath()}${apiUrl}`;
    }

    get clientId():string {
        return this.serviceOptions.clientId;
    }

    get countlyKey():string {
        return this.serviceOptions.countlyKey;
    }

    get countlyUrl():string {
        return this.serviceOptions.countlyUrl;
    }

    get appCode():string{
        return this.serviceOptions.appCode;
    }

    get app_Code():string{
        return this.serviceOptions.app_Code;
    }
}