import { Injectable } from '@angular/core';
import { HttpModule, RequestOptions, Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { DriversConstants } from '../constants/drivers.constants';
import { ISearchElement, ISearchInputService } from '../components/cmx-input-autocomplete/index';
import { forEach } from '@angular/router/src/utils/collection';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DriversService implements ISearchInputService {

    private endpoint: string = '';
    private _options = new RequestOptions();

    constructor(private http: HttpCemex) { }
    executeService(search: string): Observable<ISearchElement[]> {
        const serviceElements: ISearchElement[] = []
        this.endpoint = this.http.generateEndpoint(DriversConstants.DRIVERS_PATH);
       
        return this.http.get(this.endpoint + '?driver=' + search).map(response => {
                response.json().drivers.forEach(element => {
                    var insert = {
                        displayData: element.driverDesc + ' (' + element.driverCode + ')',
                        objectData: {
                            text: element.driverDesc,
                            code:element.driverCode,
                            value: element.driverId
                        }
                    }
                    serviceElements.push(insert);
                });
               return serviceElements; //new BehaviorSubject<ISearchElement[]>(serviceElements).asObservable();
            });
    }
}