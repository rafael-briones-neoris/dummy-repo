import { Injectable } from '@angular/core';

export type DriversCompareFn = (objectLeft:any, objectRight:any) => boolean;

@Injectable()
export class SearchSortService {

    private arrayLength: number;
    private compareTruck:DriversCompareFn;

    private buildHeap(input: any[]) {
        this.arrayLength = input.length;

        for (var i = Math.floor(this.arrayLength / 2); i >= 0; i -= 1) {
            this.heapify(input, i);
        }
    }

    private heapify(input: any[], i: number) {
        var left = 2 * i + 1;
        var right = 2 * i + 2;
        var largest = i;

        if (left < this.arrayLength && this.compareTruck(input[left], input[largest])) {//input[left].truckId > input[largest].truckId) {
            largest = left;
        }

        if (right < this.arrayLength && this.compareTruck(input[right], input[largest])) {//input[right].truckId > input[largest].truckId) {
            largest = right;
        }

        if (largest != i) {
            this.swap(input, i, largest);
            this.heapify(input, largest);
        }
    }

    private swap(input: any[], index_A: number, index_B: number) {
        var temp = input[index_A];

        input[index_A] = input[index_B];
        input[index_B] = temp;
    }

    public heapSort(input: any[], compareTruck:DriversCompareFn):any[] {
        this.compareTruck = compareTruck;
        this.buildHeap(input);

        for (let i = input.length - 1; i > 0; i--) {
            this.swap(input, 0, i);
            this.arrayLength--;
            this.heapify(input, 0);
        }
        return input;
    }

    /* Performs a binary search on the host array. This method can either be
    * injected into Array.prototype or called with a specified scope like this:
    * binaryIndexOf.call(someArray, searchElement);
    *
    * @param {*} searchElement The item to search for within the array.
    * @return {Number} The index of the element which defaults to -1 when not found.
    */
    public binaryIndexOf(input: any[], searchElement: number, searchField:string): number {

        let minIndex: number = 0;
        let maxIndex: number = input.length - 1;
        let currentIndex: number;
        let currentElement: number;

        while (minIndex <= maxIndex) {
            currentIndex = Math.floor((minIndex + maxIndex) / 2) | 0;
            currentElement = input[currentIndex][searchField];
            
            if (currentElement < searchElement) {
                minIndex = currentIndex + 1;
            }
            else if (currentElement > searchElement) {
                maxIndex = currentIndex - 1;
            }
            else {
                return currentIndex;
            }
        }

        return -1;
    }

}