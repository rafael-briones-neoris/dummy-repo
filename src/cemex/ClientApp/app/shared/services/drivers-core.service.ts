import { Injectable, Inject } from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/timeout';
import { retry } from 'rxjs/operator/retry';

import { RequestOptions, Headers, URLSearchParams, Response } from '@angular/http';
import * as moment from 'moment';
import { sprintf } from "sprintf-js";

import { HttpCemex } from '@cemex-core/angular-services-v2/dist';

import { DriversConstants } from '../constants/drivers.constants';
import { SearchSortService, DriversCompareFn } from './heap-sort.service';
import {
    EStatusChange, ICacheDetailTruck, ICacheTruck, IChangeByTruck,
    IChangesTruck, ITruckList, IDictTruck,
    ITruckLocationOpt, IPositionLatLng, IOriginDestinationTruck,
    ITruckEventResponse, IDriverApiResponse, DriverOrder, IDictOrder,
    ITicketConsolidation, EStatusAlert, ICacheAlert,
    IResendTicketRequest, IRoleMenu, IDriversAplicationMenu,
    IPointInterest
} from '../types/index.interface';
import { RoleAccess, ALL_MENU_ACCESS } from '../constants';
import { PointInterestService } from './point-interest.service';
import { ParametersService } from './parameters.service';

@Injectable()
export class DriversCoreService {
    private _trucksSubject = new BehaviorSubject<ITruckList>({ trucks: [], changedTrucks: {} });
    private _requestOptions: RequestOptions;
    private _cacheTrucks: ICacheTruck = {};
    private _newLoad: boolean;
    private _plants: any[] = [];
    private _orders: DriverOrder[] = [];
    private _ordersSubject: Subject<DriverOrder[]> = new Subject<DriverOrder[]>();
    private _alertsSubject: Subject<ICacheAlert> = new Subject<ICacheAlert>();
    private _cacheAlert: ICacheAlert = {};
    private _contentTypeOptions: RequestOptions;
    private _userRole: string;
    private _countryCode: string;
    private _pointInterestList: IPointInterest[] = [];

    private compareTruck: DriversCompareFn = function (truckLeft: any, truckRight: any): boolean {
        return truckLeft.truckId > truckRight.truckId;
    }

    public get newLoad(): boolean {
        return this._newLoad;
    }

    public set newLoad(value: boolean) {
        this._newLoad = value;
    }

    public get plants(): any[] {
        this._plants = this.getPlants();
        return this._plants;
    }

    public get monitoredPlants(): any[] {
        return this.getPlants(true);
    }

    public get ordersSubject(): Observable<DriverOrder[]> {
        return this._ordersSubject.asObservable();
    }

    public get isThirdParty(): boolean {
        return this._userRole === DriversConstants.ROLE_THIRD_PARTY;
    }

    public get allAlerts(): ICacheAlert {
        return this._cacheAlert;
    }

    public get allAlertsObservable(): Observable<ICacheAlert> {
        return this._alertsSubject.asObservable();
    }

    public typeTruck(truck: any): string {
        return this.isThirdPartyTruck(truck) ? `${this.getBusinessLine()}-3rd` : this.getBusinessLine();
    }

    public get allTrucks(): any[] {
        return this._trucksSubject.getValue().trucks;
    }

    public get pointInterestList(): IPointInterest[] {
        return this._pointInterestList;
    }

    public set pointInterestList(value: IPointInterest[]) {
        this._pointInterestList = value;
    }

    constructor(
        private httpCemex: HttpCemex,
        private searchSortService: SearchSortService,
        private parametersService: ParametersService,
        @Inject('truckLocations') private options: ITruckLocationOpt
    ) {

        this._countryCode = sessionStorage['country'];
        this._requestOptions = new RequestOptions();
        const mHeaders = new Headers();
        mHeaders.append("Authorization", `Basic ${this.options.environment}`);
        mHeaders.append("Country-Code", this._countryCode);
        mHeaders.append("Content-Type", "application/json");
        this._requestOptions.headers = mHeaders;

        this._contentTypeOptions = new RequestOptions();
        const headerContent = new Headers();
        headerContent.append("Content-Type", "application/json");
        this._contentTypeOptions.headers = headerContent;

        this._userRole = sessionStorage['role'];

    }

    /**
     * @param truck truck object
     * @param truckIds string of truck Ids to be concatenated
     * @param trucksEvent dictionary of type IDictTruck to store the truck objects
     */
    private getTruckEventId(truck: any, truckIds: string, trucksEvent: IDictTruck): string {
        if (truck.event) {
            trucksEvent[truck.truckId] = truck;
            truckIds += `${truck.truckId},`;
        }

        return truckIds;
    }

    /**
     * 
     * @param result json response of api PathConstants.TRUCKS
     * @returns only trucks with event
     */
    private getTruckEvents(result: any): ITruckEventResponse {
        let truckIds: string = "";
        let trucksEvent: IDictTruck = {};

        for (let indexStart = 0; indexStart < result.trucks.length; indexStart++) {
            //get trucks only with events and store its Id's using left index
            truckIds = this.getTruckEventId(result.trucks[indexStart], truckIds, trucksEvent);

        }
        //console.log("truckIds:", truckIds);
        truckIds = truckIds.substr(0, truckIds.length - 1);

        return {
            ok: truckIds != "",
            truckIds: truckIds,
            dictTruck: trucksEvent
        }
    }

    /**
     * method to add a truck only if lat/lng are correct
     * @param truckLocation 
     * @param trucksEvent 
     * @param trucks 
     */
    private addLocationToTruck(truckLocation: any, trucksEvent: ICacheTruck, trucks: any[]) {
        const lat: number = parseFloat(truckLocation.latitude);
        const lng: number = parseFloat(truckLocation.longitude);
        if ((lat >= -90 && lat <= 90)
            && ((lng >= -180 && lng <= 180))) {
            const truckadd: any = trucksEvent[truckLocation.truckId];

            if (truckadd) {
                truckadd.latitude = lat;
                truckadd.longitude = lng;
                trucksEvent[truckLocation.truckId] = truckadd;
                trucks.push(truckadd);
            }
        } else {
            console.debug(`truck ${truckLocation.truckId} lat:${truckLocation.latitude} / lng:${truckLocation.longitude} Omitting invalid coordinate`)
        }
    }

    private getChangesDetected(bufferTrucks: any[]): IChangesTruck {
        let result: IChangesTruck = {};
        let trucks = this._trucksSubject.getValue().trucks;
        if (trucks && trucks.length != bufferTrucks.length) {
            console.log("there is difference in number of trucks!!  from API:", bufferTrucks.length, " and trucks is map:", trucks.length);
        }

        let indexStart = 0;
        let indexEnd = bufferTrucks.length - 1;
        for (let truckInBuffer of bufferTrucks) {
            const truckApi = truckInBuffer;
            const statusChange: EStatusChange = this.getStatusChange(truckApi)
            if (statusChange != EStatusChange.NONE) {
                result[truckApi.truckId] = {
                    truck: truckApi,
                    status: statusChange
                };
            }
        }
        //get deleted trucks
        let arrTruck: string[] = Object.keys(this._cacheTrucks);
        indexStart = 0;
        indexEnd = arrTruck.length - 1;
        for (let truckId of arrTruck) {
            let numFound: number = this.searchSortService.binaryIndexOf(bufferTrucks, parseInt(truckId), 'truckId');
            if (numFound == -1) {
                result[truckId] = {
                    truck: this._cacheTrucks[truckId].truckObject,
                    status: EStatusChange.DELETED
                }
                console.log(`deleting truck ${truckId}`);
            }

        }

        return result;
    }

    private getStatusChange(truckApi: any): EStatusChange {
        let truckCache: ICacheDetailTruck = this._cacheTrucks[truckApi.truckId];
        let truckString = JSON.stringify(truckApi);
        if (!truckCache) {
            console.log(`the truck: ${truckApi.truckId} ,is a new detected truck`);
            return EStatusChange.NEW;
        } else {
            if (truckString != truckCache.truckString) {
                const changeLocation = truckCache.truckObject.latitude !== truckApi.latitude || truckCache.truckObject.longitude !== truckApi.longitude;
                console.log(`the truck: ${truckApi.truckPlate} ,contains new data  change location:${changeLocation}`);
                return changeLocation ? EStatusChange.MODIFIED_LOCATION : EStatusChange.MODIFIED;
            }
        }
        return EStatusChange.NONE;
    }

    /**
     * 
     * @param response of truck locations API
     * @param trucksEvent a dictionary of trucks with events
     * @returns ITruckList all trucks with events and theirs locations
     */
    private getTrucksLocation(response: any, trucksEvent: IDictTruck): ITruckList {
        const result: any = response.json() || [];
        //console.log("****************response from api gps location trucks!****************:", result);
        let timeProcTruck = Date.now();
        let indexEnd = result.length - 1;
        let unsortedTrucks: any[] = []
        for (let indexStart = 0; indexStart < result.length; indexStart++) {
            //consider use binary search
            const truckLocation = result[indexStart];
            this.addLocationToTruck(truckLocation, trucksEvent, unsortedTrucks);
        }

        const trucks = this.searchSortService.heapSort(unsortedTrucks, this.compareTruck);
        const emptyCache: boolean = Object.keys(this._cacheTrucks).length == 0;
        //detect changes between cache trucks and API trucks
        const changesTruck: IChangesTruck = emptyCache ? {} : this.getChangesDetected(trucks);
        if (emptyCache || Object.keys(changesTruck).length > 0) {
            //clear and set cache with trucks from API
            this._cacheTrucks = {};
            for (const truckApi of trucks) {
                this._cacheTrucks[truckApi.truckId] = {
                    truckObject: truckApi,
                    truckString: JSON.stringify(truckApi)
                };
            }
            //  console.log("time get all changes and cache:", Date.now() - timeProcTruck);
            timeProcTruck = Date.now();
            this._trucksSubject.next({ trucks: trucks, changedTrucks: changesTruck });
            this._orders = this.fetchOrders(trucks, this._plants);
            this._ordersSubject.next(this._orders);
            console.log("time get all trucks and orders:", Date.now() - timeProcTruck);
        } else {
            // console.debug("trucks are the same , no changes!!");
            if (this._newLoad) {
                this._trucksSubject.next({ trucks: trucks, changedTrucks: {} });
            }
        }
        //get truck alerts only if there are trucks with event
        this.fetchAllAlerts();
        return this._trucksSubject.value;
    }

    private handleError(error: Response | any): Observable<any> {
        console.debug("response error:", error);
        return Observable.throw(error);
    }

    private fetchOrders(trucks: any[], plants: any[]): DriverOrder[] {
        const result: DriverOrder[] = [];
        const dictOrder: IDictOrder = {};
        const hasModule: boolean = trucks.length / 2 == 1;
        let indexEnd: number = trucks.length - 1;

        for (let indexStart = 0; indexStart < Math.floor(trucks.length / 2); indexStart++) {
            //left index
            this.processOrderTruck(trucks[indexStart], dictOrder, result, plants);
            if (indexStart + 1 == indexEnd && hasModule) {
                break;
            }
            //right index
            this.processOrderTruck(trucks[indexEnd], dictOrder, result, plants);
            indexEnd--;
        }
        // console.log("finishing ordersFromTrucks...");
        return result;
    }

    private processOrderTruck(truck: any, dictOrder: IDictOrder, result: DriverOrder[], plants: any[]) {

        if (truck.event != undefined &&
            truck.event.ticket != undefined &&
            truck.event.ticket.order != undefined) {
            //console.log("ticket found:", JSON.stringify(truck.event.ticket));
            const ticket = truck.event.ticket;
            const orderTmp = ticket.order;
            const order: DriverOrder = dictOrder[orderTmp.orderId];
            //order already exists in orders array
            if (order) {
                //then add truck into order
                order.trucks.push(truck);
            } else {
                //add new order into orders array
                const order = new DriverOrder({
                    orderId: orderTmp.orderId,
                    orderCode: orderTmp.orderCode,
                    ticketCode: ticket.ticketCode,
                    ticketId: ticket.ticketId,
                    totalQuantity: orderTmp.totalQuantity,
                    um: orderTmp.unit.unitCode,
                    volume: orderTmp.volume,
                    podName: orderTmp.podName,
                    clientName: orderTmp.clientName
                });

                order.trucks.push(truck);

                if (orderTmp.jobsite !== undefined) {
                    order.jobsiteId = orderTmp.jobsite.jobsiteId;
                    order.jobsiteName = orderTmp.jobsite.jobsiteDesc;

                    if (orderTmp.jobsite.address.geoPlace !== undefined) {
                        order.latitude = orderTmp.jobsite.address.geoPlace.latitude;
                        order.longitude = orderTmp.jobsite.address.geoPlace.longitude;
                    }
                }

                result.push(order)
                dictOrder[order.orderId] = order;
            }
        }
    }

    private fetchAllAlerts(): void {
        const today = moment(new Date()).format("YYYY-MM-DD");
        const tomorrow = moment(today).add(1, 'days').format("YYYY-MM-DD");

        const endpoint: string = `${DriversConstants.NOTIFY_PATH}?dateFrom=${today}&dateTo=${tomorrow}`;
        this.httpCemex.get(this.httpCemex.generateEndpoint(endpoint)).subscribe(
            result => {
                const rawJSON = result.json();
                if (rawJSON && rawJSON.alerts !== undefined) {
                    const allAlerts = rawJSON.alerts;
                    const tmpAlerts: any[] = [];
                    //const isCacheEmpty:boolean = Object.keys(this._cacheAlert).length === 0;

                    for (const alertAPI of allAlerts) {
                        if (alertAPI.alertStatus.alertStatusId != EStatusAlert.VIEWED && alertAPI.truck) {
                            const alertCache = this._cacheAlert[alertAPI.alertId];
                            if (alertCache === undefined) {
                                this._cacheAlert[alertAPI.alertId] = alertAPI;
                                tmpAlerts.push(alertAPI);
                            }

                        }
                    }

                    if (tmpAlerts.length > 0) {
                        this._alertsSubject.next(this._cacheAlert);
                    }

                } else {
                    console.warn(`Couldn't read alerts from API, response with:${result.json()}`);
                }

            }
        )
    }

    /**
     * @returns an Observer of ITruckList, it contains all trucks with events, and changes on trucks
     */
    public fetchAllTrucks(): Observable<ITruckList> {
        const PULL_TRUCKS_TIME: number = Number(this.parametersService.get("DRIVER_CONSOLE_REFRESH_FREQUENCY_IN_MINUTES")) * 60000;

        return Observable.timer(0, PULL_TRUCKS_TIME)
            .concatMap(v => {
                let endpoint: string = this.httpCemex.generateEndpoint(DriversConstants.TRUCKS_EVENT);
                return this.httpCemex.get(endpoint)
                    .map((response: Response) => {
                        return this.getTruckEvents(response.json());
                    })
                    .timeout(DriversConstants.TIMEOUT_TRUCK_EVENT)
                    .catch(this.handleError)
            })
            .concatMap((trucksEvent: ITruckEventResponse) => {
                if (!trucksEvent.ok) {
                    return Observable.throw(trucksEvent);
                }

                return this.httpCemex.post(`${this.options.basePath}${DriversConstants.TRUCKS_LAST_LOCATION}`, `"${trucksEvent.truckIds}"`, this._requestOptions, true)
                    .map((response: Response) => {
                        return this.getTrucksLocation(response, trucksEvent.dictTruck);
                    })
                    .timeout(DriversConstants.TIMEOUT_TRUCK_LOCATIONS)
                    .catch(this.handleError)
            });
    }

    /**
     * @returns the business Line of the user set in sessionStorage
     */
    public getBusinessLine(): string {
        const tmpJson: any = JSON.parse(sessionStorage["userData"]);
        if (tmpJson && tmpJson.businessLine) {
            return tmpJson.businessLine.toLowerCase();
        }

        return "rmx";
    }

    public getTicket(ticketId: number): Observable<any> {
        const queryparam = 'include=shipment,driver,ticketitems,order,jobsite,weight,plant,proofofdelivery,wateraddedagreement,liabilitywaiver,statuslog';
        const endpoint = `${DriversConstants.TICKET_DETAIL}/${ticketId}?${queryparam}`;
        return this.httpCemex.get(this.httpCemex.generateEndpoint(endpoint))
            .catch(this.handleError);
    }

    public getConsolidationTicket(consolidationCode: string): Observable<ITicketConsolidation> {
        const endpoint = `${DriversConstants.TICKET_CONSOLIDATION}/${consolidationCode}`;
        return this.httpCemex.get(this.httpCemex.generateEndpoint(endpoint))
            .map(response => {
                return response.json() as ITicketConsolidation;
            });
    }

    public getPlants(monitored?: boolean) {
        let tmpJson: any = JSON.parse(sessionStorage["userData"]);
        if (tmpJson == undefined || tmpJson.plants == undefined) {
            return [];
        }
        //getting only plants with monitored == true
        return monitored ? tmpJson.plants.filter(plant => plant.monitored) : tmpJson.plants;
    }

    public getOriginDestinationTruck(truck: any): IOriginDestinationTruck {
        const plantId = truck.event.plant.plantId;
        const localPlants = this.getPlants(true);
        const tmp: any[] = localPlants.filter(plant => { return plant.plantId == plantId });
        const destination = truck.event.ticket.order.jobsite.address.geoPlace;
        
        if (tmp.length > 0 && destination && destination !== undefined) {
            const plantOrigin = tmp[0];
            const result: IOriginDestinationTruck = {
                origin: {
                    lat: plantOrigin.address.geoPlace.latitude,
                    lng: plantOrigin.address.geoPlace.longitude
                },
                destination: {
                    lat: destination.latitude,
                    lng: destination.longitude
                }
            }

            return result;
        } else {
            console.warn(`plantId ${plantId} not found or jobsite's geoplace is empty!!`);
        }

        return null;
    }

    public clearCache(): void {
        this._cacheTrucks = {};
        this._cacheAlert = {};
    }

    public unlinkTruckDriver(truckId): Observable<Response> {
        const endpoint = this.httpCemex.generateEndpoint(sprintf(DriversConstants.TRUCK_UNLINK_DRIVER, truckId));
        return this.httpCemex.patch(endpoint, '{ "assignationReasonId" : 3 }', this._contentTypeOptions);
    }

    public getDocumentsClientHTML(documentType: any) {
        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.TERMS +
            '?type=' + documentType +
            '&deviceCode=' + DriversConstants.deviceCode +
            '&plataformCode=' + DriversConstants.platformCode +
            '&customerCode=' + localStorage.getItem('customerCode'));

        return this.httpCemex.get(apiUrl).map(res => res.json());
    }

    public getPublicDocumentsClientHTML(documentType: string, isoCountryCode: number) {
        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.PUBLIC_TERMS +
            '?type=' + documentType +
            '&deviceCode=' + DriversConstants.deviceCode +
            '&plataformCode=' + DriversConstants.platformCode +
            '&isoCountryCode=' + isoCountryCode);

        return this.httpCemex.get(apiUrl).map(res => res.json());
    }

    public acceptTerms(terms: any) {
        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.TERMS);
        return this.httpCemex.post(apiUrl, terms).map(res => res);
    }

    public resendTicket(request: IResendTicketRequest): Observable<Response> {
        const endpoint = this.httpCemex.generateEndpoint(sprintf(DriversConstants.MESSAGE_PUSH_PATH, DriversConstants.RESEND_MSG_CODE, DriversConstants.RESEND_TOPIC_CODE));
        const bodyRequest = {
            TicketId: request.ticketId,
            DriverId: request.driverId,
            Timestamp: request.timestamp
        }

        return this.httpCemex.patch(endpoint, bodyRequest, this._contentTypeOptions);
    }

    public canAccess(urlResource: string): boolean {
        if (this.isThirdParty) {
            const accessRoute: IRoleMenu[] = RoleAccess[this._userRole];
            for (const aRoute of accessRoute) {
                const result = aRoute.route.indexOf(urlResource) > -1;
                if (result) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }

    public getMenuByRole(): IDriversAplicationMenu[] {
        if (this.isThirdParty) {
            const result: IDriversAplicationMenu[] = [];
            const accessRoute: IRoleMenu[] = RoleAccess[this._userRole];
            for (const aRoute of accessRoute) {
                for (const appMenu of ALL_MENU_ACCESS) {
                    if (appMenu.applicationMenuId === aRoute.menuId) {
                        result.push(appMenu);
                        break;
                    }
                }
            }

            return result;
        }

        return ALL_MENU_ACCESS;
    }

    public isThirdPartyTruck(truck: any): boolean {
        return truck.thirdParty;
    }

}