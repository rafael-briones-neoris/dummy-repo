import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import { Subscription } from 'rxjs/Subscription';
import { Headers, Http, Request, RequestOptions, RequestOptionsArgs, Response } from "@angular/http";

import { Broadcaster } from "@cemex-core/events-v1/dist";
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { SessionServiceConstants } from '@cemex-core/constants-v2/dist';

@Injectable()
export class RefreshTokenService {

    /**
     * interval in secs
     */
    private _intervalRefresh: number = 3600;
    private _subcriptionRefresh: Subscription;
    /**
     * total lifespan in secs
     */
    private _expireSec: number = 360;
    /**
     * expiration date of session
     */
    private _expirationDate: Date;
    private _refreshToken: string;

    constructor(
        private _eventBroadcaster: Broadcaster,
        private _http: Http,
        private _httpCemex: HttpCemex
    ) {

        const strJson = sessionStorage['token_data'];
        if (strJson !== undefined) {
            const tokenJson = JSON.parse(strJson);
            this._refreshToken = tokenJson.oauth2.refresh_token;
            this.setExpirationDate(parseInt(tokenJson.oauth2.expires_in))
        }

        if (sessionStorage['date_expire'] === undefined) {
            this._expirationDate = new Date();
            this._expirationDate.setSeconds(this._expirationDate.getSeconds() + this._intervalRefresh);
            sessionStorage.setItem('date_expire', this._expirationDate.toLocaleString('en-US'));
        } else {
            // this is when occurs a browser's refresh
            this._expirationDate = new Date(Date.parse(sessionStorage['date_expire']));
            const localDate = new Date();
            const diff = this._expirationDate.getTime() - localDate.getTime();
            this._intervalRefresh = diff > 0 ? diff / 1000 : 3600;
        }

        console.log('_intervalRefresh:', this._intervalRefresh);
        console.log('_expirationDate:', this._expirationDate);

        this._eventBroadcaster.on(Broadcaster.DCM_APP_LOGOUT).subscribe(() => {
            if (this._subcriptionRefresh) {
                this.stopAutoRefresh();
            }

            sessionStorage.removeItem('date_expire');
        })

        this.executeAutoRefresh();
    }

    private doExecuteRefresh(): Observable<boolean> {
        const interval = this._intervalRefresh * 1000;
        return Observable.timer(0, interval)
            .concatMap(iteration => {
                return Observable.of(this.isRequestRefreshToken());
            });
    }

    private isRequestRefreshToken(): boolean {
        const currentDate = new Date();
        return currentDate >= this._expirationDate;
    }

    private generateRefreshToken(): void {
        const requestOptions: RequestOptionsArgs = new RequestOptions();
        requestOptions.headers = new Headers();
        requestOptions.headers.append("Content-Type", "application/x-www-form-urlencoded");
        const bodyMessage = `grant_type=refresh_token&refresh_token=${this._refreshToken}&client_id=${this._httpCemex.clientId}`;
        const endpoint = this._httpCemex.generateEndpoint(SessionServiceConstants.LOGIN_TOKEN_DEFAULT);

        this._http.post(endpoint, bodyMessage, requestOptions).subscribe(
            (response: Response) => {
                const resultToken = response.json();
                const strJson = sessionStorage['token_data'];
                const tokenJson = JSON.parse(strJson);

                sessionStorage.setItem('access_token', resultToken.access_token);
                tokenJson.oauth2.access_token = resultToken.access_token;
                tokenJson.oauth2.expires_in = resultToken.expires_in;

                sessionStorage.setItem('token_data', JSON.stringify(tokenJson));
                this._refreshToken = resultToken.refresh_token;
                this.setExpirationDate(parseInt(tokenJson.oauth2.expires_in));
            }
        );
    }

    private setExpirationDate(totalSecs: number): void {
        this._expireSec = totalSecs;
        this._intervalRefresh = this._expireSec * 0.80;
        this._expirationDate = new Date();
        this._expirationDate.setSeconds(this._expirationDate.getSeconds() + this._intervalRefresh);
        sessionStorage.setItem('date_expire', this._expirationDate.toLocaleString('en-US'));
    }

    public executeAutoRefresh(): void {
        this._subcriptionRefresh = this.doExecuteRefresh().subscribe(
            refresh => {
                console.log('doing refresh token?', refresh);
                if (refresh) {
                    console.debug('**********execute refresh token**********', new Date());
                    this.generateRefreshToken();
                }
            }
        );
    }

    public stopAutoRefresh(): void {
        this._subcriptionRefresh.unsubscribe();
        this._subcriptionRefresh = null;
        console.debug('RefreshTokenService unsubscribing.....');
    }
}