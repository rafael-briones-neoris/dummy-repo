import { Injectable } from '@angular/core';
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { DriversConstants } from '../constants/drivers.constants';
import { Broadcaster } from "@cemex-core/events-v1/dist";

@Injectable()
export class ParametersService {

    constructor(private http: HttpCemex, private eventBroadcaster: Broadcaster) { 
        this.getParameters();

        eventBroadcaster.on(Broadcaster.DCM_APP_LOGOUT).subscribe(() => {
            sessionStorage.removeItem('myParameters');
        });
    }

    public getParameters(): void | boolean {
        if(sessionStorage["myParameters"]) return false;
        
        const endpoint:string = this.http.generateEndpoint(DriversConstants.MY_PARAMETERS);
        
        this.http.get(endpoint).subscribe(parameters => {
            sessionStorage.setItem('myParameters', JSON.stringify(parameters.json()));
        });
    }

    public get(parameterDesc:string): any{
        const parameters:any = JSON.parse(sessionStorage.getItem('myParameters')).parameters;
        const parameter:any = parameters.find(parameter => parameter.parameterDesc === parameterDesc) || "";

        if(!parameter.parameterValue){
            return DriversConstants[parameterDesc];
        }else{
            return parameter.parameterValue;
        }
    }
}
