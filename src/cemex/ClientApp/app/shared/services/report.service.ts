import { Injectable, Inject } from '@angular/core';
import { HttpModule, RequestOptions, Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { DriversConstants } from '../constants/drivers.constants';
import { ITruckLocationOpt } from '../types/index.interface';

@Injectable()
export class ReportService {

    private endpoint: string = '';
    private requestOptions = new RequestOptions();

    constructor(private http: HttpCemex,
        @Inject('truckLocations') private options: ITruckLocationOpt) {
    }

    getReportData(reportConstant: string, params: any): Promise<any> {
        // Endpoint
        this.endpoint = this.http.generateEndpoint(reportConstant);

        return this.http.get(this.endpoint + params)
            .toPromise()
            .then(response => {
                return response.json();
            });
    }

    getTruckLocations(reportConstant: string, params: any): Promise<any> {
        // Request Options
        this.requestOptions = new RequestOptions();
        const mHeaders = new Headers();
        mHeaders.append("Authorization", `Basic ${this.options.environment}`);
        mHeaders.append("Country-Code", sessionStorage['country']);
        mHeaders.append("Content-Type", "application/json");
        this.requestOptions.headers = mHeaders;

        // Endpoint
        this.endpoint = `${this.options.basePath}` + reportConstant;

        return this.http.get(this.endpoint + params, this.requestOptions, true)
            .toPromise()
            .then(response => {
                return response.json();
            });
    }
}