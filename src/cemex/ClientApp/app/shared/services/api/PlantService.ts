import { Injectable } from '@angular/core';
import { HttpModule, RequestOptions, Headers, Http, Response, URLSearchParams } from '@angular/http';
import { userDataDTO } from '@cemex-core/types-v1/dist';
import { DriversConstants } from './../../constants/drivers.constants';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { HttpCemex } from '@cemex-core/angular-services-v2/dist';

@Injectable()
export class PlantService {

    private  endpoint:string='';
    private _options = new RequestOptions();

    constructor(private http: HttpCemex) { }

    getAllPlants(): Promise<userDataDTO> {
        const endpoint = this.http.generateEndpoint(DriversConstants.PLANT_PATH);
        return this.http.get(endpoint)
            .toPromise()
            .then(response => response.json() as userDataDTO)
            .catch(this.handleError);

    }

    handleSuccess = (response: Response): Observable<Response> => {
        let body;

        if (response.text()) {
            body = response.json();
        }

        return body || {};
    }

     updateMonitoredPlants(arrayPlantIds:userDataDTO): Observable<any>{ 
         
    this.endpoint = this.http.generateEndpoint(DriversConstants.PLANTS_PATH_PATCH);
        return this.http.patch(this.endpoint, arrayPlantIds).map((res) => 
        {
            return res;
        }).catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        console.log(error);
        return Promise.reject(error.message || error);
    }

    /*   getPlantById(): PlantDTO {
   
           return new PlantDTO();
       }
   
   
       public updatePlant(plantList:any[], plantId:string):Observable<any> {
           
           let endpoint: string = `${this.http.getWorkingPath()}${PathConstants.PlANTS_MGMT_PATCH}`.replace("{plantId}",plantId);
           return this.http.patch(endpoint,JSON.stringify(plantList), this._options)
           .catch(this.handleError);
       }*/

}