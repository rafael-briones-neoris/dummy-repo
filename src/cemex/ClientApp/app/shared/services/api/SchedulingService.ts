import { Injectable } from '@angular/core';
import { HttpModule, RequestOptions, Headers, Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { DriversConstants } from './../../constants/drivers.constants';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { HttpCemex } from '@cemex-core/angular-services-v2/dist';

import { SchedulingDTO } from '@cemex-core/types-v1/dist';
import { DriversDTO } from '@cemex-core/types-v1/dist';

@Injectable()
export class SchedulingService {
    private endpoint: string = '';
    constructor(private http: HttpCemex) { }
    getScheduling(today: string): Promise<Schedules> {

        this.endpoint = this.http.generateEndpoint(DriversConstants.SCHEDULE_DRIVER_PATH);

        return this.http.get(this.endpoint + '&dateFrom=' + today + 'T00:00:00&dateTo=' + today + 'T23:59:59')
            .toPromise()
            .then(response => {
                let schedulesreturn: Schedules;
                return response.statusText == "OK" ? response.json() as Schedules : schedulesreturn
            })
            .catch(this.handleError);
    }

    public getSchedulingDetail(id: number): Observable<SchedulingDTO> {
        this.endpoint = this.http.generateEndpoint(DriversConstants.SCHEDULE_DETAIL_DRIVER_PATH);
        return this.http.get(this.endpoint + '/' + id)
            .map(response => {

                let schedulesreturn: SchedulingDTO;
                return response.statusText == "OK" ? response.json() as SchedulingDTO : schedulesreturn
            })
            .catch(this.handleError);
    }

    sendSchedule(schedule: any): Promise<string> {

        this.endpoint = this.http.generateEndpoint(DriversConstants.SCHEDULE_DETAIL_DRIVER_PATH + '/' + schedule.scheduleId);

        return this.http.patch(this.endpoint, schedule)
            .toPromise()
            .then(response => {
                let schedulesreturn: SchedulingDTO;
                return response.statusText == "OK" ? 'ok' : ""
            })
            .catch(this.handleError);
    }

    saveSchedulingDetail(schedule: any): Promise<any> {/*+ '/' + schedule.scheduleId */

        var obj = JSON.stringify(schedule);
        console.log("obj " + obj);

        this.endpoint = this.http.generateEndpoint(DriversConstants.SCHEDULE_DETAIL_DRIVER_PATH);
        return this.http.post(this.endpoint, schedule)
            .toPromise()
            .then(response => {

                let schedulesreturn: SchedulingDTO;
                return response.statusText == "OK" ? response.json() : schedulesreturn
            })
            .catch(this.handleError);
    }

    updateSchedulingDetail(schedule: any): Promise<any> {

        this.endpoint = this.http.generateEndpoint(DriversConstants.SCHEDULE_DETAIL_DRIVER_PATH + '/' + schedule.scheduleId);

        return this.http.put(this.endpoint, schedule)
            .toPromise()
            .then(response => {

                let schedulesreturn: "";
                return response.statusText == "OK" ? "" : 'error'
            })
            .catch(this.handleError);
    }

    getAllDrivers(name: string): Promise<Drivers> {

        this.endpoint = this.http.generateEndpoint(DriversConstants.DRIVERS_PATH + '?driver=' + name);

        return this.http.get(this.endpoint)
            .toPromise()
            .then(response => {
                let Driversreturn: Drivers;
                return response.statusText == "OK" ? response.json() as Drivers : Driversreturn
            })
            .catch(this.handleError);
    }

    getLastSchedule(plantId: string): Promise<any> {

        this.endpoint = this.http.generateEndpoint(DriversConstants.SCHEDULE_DETAIL_DRIVER_PATH + '?include=detail&take=1&plantId=' + plantId);

        return this.http.get(this.endpoint)
            .toPromise()
            .then(response => {
                return response.statusText == "OK" ? response.json() : ""
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log(error.message);
        console.error('An error occurred', error.message);
        return Promise.reject(error.message || error);
    }
}
export class Schedules {
    schedules: SchedulingDTO[];
}
export class Drivers {
    drivers: DriversDTO[]
}
