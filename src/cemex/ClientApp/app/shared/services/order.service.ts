import { Injectable } from '@angular/core';
import { HttpModule, RequestOptions, Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { HttpCemex } from '@cemex-core/angular-services-v2/dist';
import { DriversConstants } from '../constants/drivers.constants';
import { ISearchElement, ISearchInputService } from '../components/cmx-input-autocomplete/index';
import { forEach } from '@angular/router/src/utils/collection';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs';

let moment = require("moment");

@Injectable()
export class OrderService implements ISearchInputService {
    dateTo: string;
    dateFrom: string;
    plants: string;
    url: any;

    private endpoint: string = '';
    private _options = new RequestOptions();

    constructor(private http: HttpCemex) { }

 
    public executeService(search: string):Observable<any> {
        const serviceElements: ISearchElement[] = []
        this.endpoint  = this.http.generateEndpoint(DriversConstants.REPORDERS);
        
        //search = "0119"

        if((!this.plants) || (!this.dateFrom) || (!this.dateTo)){ return; }
        let plants = "?plantId=" + this.plants;
        let dates  = "&dateFrom=" + this.dateFrom + "&dateTo=" + this.dateTo;
        let order  = "&orderCode=" + search;
        let type   = "&reportType=pertripstatus";
        const url = plants + dates + order + type;

        return this.http.get(this.endpoint + url ).map(response => {
            let data = response.json();
            response.json().orders.forEach(item => {
                var insert = {
                    displayData: item.orderCode,
                    objectData: {
                        text: item.orderCode,
                        code:item.orderId,
                        value: item.orderId
                    }
                }
                serviceElements.push(insert);
            });
            return serviceElements; //new BehaviorSubject<ISearchElement[]>(serviceElements).asObservable();
        });
    }

    public setPlants(plants) { 
        this.plants = plants;
    }

    public setDates(date) { 
        this.dateFrom = "";
        this.dateTo   = "";
        if( date ) {
            this.dateFrom = moment(date.min).format("YYYY-MM-DD");
            this.dateTo = moment(date.max).format("YYYY-MM-DD");
        }
    }
}