import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Response, RequestOptions, URLSearchParams, Headers } from '@angular/http';

import { HttpCemex } from '@cemex-core/angular-services-v2/dist';

import { DriversConstants } from '../constants/drivers.constants';
import { IPointInterest } from '../types/index.interface';

export enum EGenericPoint {
    PLANT = 1,
    POINT_DELIVERY = 2,
    POINT_INTEREST = 3,
}

@Injectable()
export class GeofencesService {

    static eGenericPoint = EGenericPoint;

    constructor(private httpCemex: HttpCemex) {
    }

    // Point of Delivery | Plants: subject and observable
    // Set and Get methods for generic point
    private pointSubject = new BehaviorSubject(null);
    private point$ = this.pointSubject.asObservable();

    public setPoint(point: any, typeId: number) {
        // if point parameter is not defined, ignore.
        if (!point) {
            return false;
        }

        // if type id does not belong to GenericPoints, ignore.
        if (!(typeId in GeofencesService.eGenericPoint)) {
            return false;
        }

        const localPoint: any = point;
        const localStorageKey: string = this.getLocalStorageKey(typeId);
        localStorage.removeItem(localStorageKey);
        localStorage.setItem(localStorageKey, JSON.stringify(localPoint));
        this.pointSubject.next(localPoint);
    }

    public getPoint() {
        return this.point$;
    }

    // HTTP Requests
    public updatePlantGeofence(plantList: any[], plantId: string) {
        let _options = new RequestOptions()
        _options.headers = new Headers();
        _options.headers.append("Content-Type", "application/json");

        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.PlANTS_MGMT_PATCH.replace("{plantId}", plantId));
        return this.httpCemex.patch(apiUrl, plantList, _options).map(res => res);
    }

    public updateDeliveryPointGeofence(deliveryPointList: any[], deliveryPointId: string) {
        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.DELIVERY_POINTS_PATH.replace("{deliveryPointId}", deliveryPointId));
        return this.httpCemex.patch(apiUrl, deliveryPointList).map(res => res);
    }

    public updateJobsiteGeofence(jobsiteList: any[], jobsiteId: string) {
        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.JOBSITE_PATH.replace("{jobsiteId}", jobsiteId));
        return this.httpCemex.patch(apiUrl, jobsiteList).map(res => res);
    }

    public addPointInterestGeofence(pointInterestList: any[]) {
        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.POINT_INTEREST_PATH);
        return this.httpCemex.post(apiUrl, pointInterestList).map(res => res);
    }

    public updatePointInterestGeofence(pointInterestList: any[], pointInterestId: string) {
        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.POINT_INTEREST_ID_PATH.replace("{pointInterestId}", pointInterestId));
        return this.httpCemex.patch(apiUrl, pointInterestList).map(res => res);
    }

    public deletePointInterestGeofence(pointInterestId: string) {
        let apiUrl: string = this.httpCemex.generateEndpoint(DriversConstants.POINT_INTEREST_ID_PATH.replace("{pointInterestId}", pointInterestId));
        return this.httpCemex.delete(apiUrl).map(res => res);
    }

    // Geofence Points
    private getLocalStorageKey(typeId: number) {
        if (typeId == EGenericPoint.PLANT) {
            return 'lastEditedPlant'
        } else if (typeId == EGenericPoint.POINT_DELIVERY) {
            return 'lastEditedPointDelivery'
        } else if (typeId == EGenericPoint.POINT_INTEREST) {
            return 'lastEditedPointInterest'
        }
        throw new RangeError('Type Id parameter must be inside EGenericPoint definition');
    }

}