import { Observable } from "rxjs";
import { ISearchElement, ISearchInputService } from "../components/cmx-input-autocomplete/index";
import { Injectable } from "@angular/core";

@Injectable()
export class MapService implements ISearchInputService {

    constructor() {
    }

    private _autocompleteService: google.maps.places.AutocompleteService;

    public setAutoCompleteService(acs: google.maps.places.AutocompleteService) {
        this._autocompleteService = acs;
    }

    public executeService(search: string): Observable<any> {
        const serviceElements: ISearchElement[] = []
        return Observable.create((observer) => {
            this._autocompleteService.getPlacePredictions({ input: search }, (predictions, status) => {
                if (!predictions)
                    return false;

                predictions.forEach(item => {
                    let element: any = {
                        displayData: item.description,
                        objectData: {
                            text: item.description,
                            code: item.description,
                            value: item.description
                        }
                    };
                    serviceElements.push(element);
                });
                observer.next(serviceElements);
                return serviceElements;
            });
        });
    }
}