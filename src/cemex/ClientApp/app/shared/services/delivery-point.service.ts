import { Injectable } from '@angular/core';
import { HttpModule, RequestOptions, Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpCemex } from '@cemex-core/angular-services-v2/dist';

import { DriversConstants } from '../constants/drivers.constants';
import { IDeliveryPointDTO } from '../types/index.interface'

@Injectable()
export class DeliveryPointService {
    private _options = new RequestOptions();

    constructor(private http: HttpCemex) { 
        this._options.headers = new Headers();
        this._options.headers.append("Content-Type", "application/json");
    }
    
    public getAllPOD(geofence?:any): Observable<IDeliveryPointDTO[]> {
        const endpoint:string  = this.http.generateEndpoint(DriversConstants.POD_PATH);

        let requestOptions = new RequestOptions();
        if (geofence != null && geofence  != undefined){
            let params: URLSearchParams = new URLSearchParams();
            params.set("geofence", geofence);
            requestOptions.search = params;
        }

        return this.http.get(endpoint, requestOptions)
            .map(
            (response: Response) => {
                console.log("response of getting All PODs:", response);
                return response.status == 200 ? response.json() : [];
            }
            )
            .catch(this.handleError);
    }

    public updateDeliveryPoint(deliveryPointList:any, deliveryPointId:number, isDeliveryPoint:boolean):Observable<any>{
        let endpoint:string  = this.http.generateEndpoint(DriversConstants.POD_PATH);

        if (isDeliveryPoint){
            endpoint = endpoint.replace('jobsites', 'deliverypoints');
            endpoint = endpoint.replace('dm', 'gd');
        }
        return this.http.patch(endpoint,JSON.stringify(deliveryPointList), this._options)
        .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(error);
        return Observable.throw(error);
    }
}