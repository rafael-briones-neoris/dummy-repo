import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Response, RequestOptions, URLSearchParams } from '@angular/http';

import { HttpCemex } from '@cemex-core/angular-services-v2/dist';

import { DriversConstants } from '../constants/drivers.constants';
import { IPointInterest } from '../types/index.interface';

@Injectable()
export class PointInterestService {

    constructor(private http: HttpCemex) { }

    public getPoIByCountry(countryCode: string): Observable<IPointInterest[]> {
        let polyParam = null;
        /**this code comes from drivers angular 1 */
        if (countryCode == "US") {
            polyParam = {
                "fence": [{ "latitude": 50.4015153227824, "longitude": -127.001953125 },
                { "latitude": 36.3151251474805, "longitude": -124.892578125 },
                { "latitude": 31.8028925867068, "longitude": -117.861328125 },
                { "latitude": 30.372875188118, "longitude": -108.193359375 },
                { "latitude": 28.1495032115446, "longitude": -103.095703125 },
                { "latitude": 24.6869524119992, "longitude": -97.119140625 },
                { "latitude": 28.1495032115446, "longitude": -91.845703125 },
                { "latitude": 28.3817350432231, "longitude": -84.638671875 },
                { "latitude": 24.206889622398, "longitude": -81.826171875 },
                { "latitude": 23.885837699862, "longitude": -78.92578125 },
                { "latitude": 28.2269700389183, "longitude": -78.310546875 },
                { "latitude": 31.653381399664, "longitude": -79.453125 },
                { "latitude": 36.6684189189479, "longitude": -72.861328125 },
                { "latitude": 42.9403392336318, "longitude": -67.412109375 },
                { "latitude": 43.1971672825013, "longitude": -61.787109375 },
                { "latitude": 45.7061792853309, "longitude": -58.359375 },
                { "latitude": 50.7364551370107, "longitude": -65.56640625 },
                { "latitude": 44.0875850282452, "longitude": -80.33203125 },
                { "latitude": 47.9899216674142, "longitude": -82.96875 },
                { "latitude": 50.8475729536539, "longitude": -92.98828125 },
                { "latitude": 50.4015153227824, "longitude": -127.001953125 }]

            };
        }

        if (countryCode == "MX") {
            polyParam = {
                "fence": [{ "latitude": 33.3580616127789, "longitude": -117.685546875 },
                { "latitude": 31.87755764334, "longitude": -118.037109375 },
                { "latitude": 27.2155562090297, "longitude": -115.576171875 },
                { "latitude": 21.8614987343726, "longitude": -110.478515625 },
                { "latitude": 23.4834006543256, "longitude": -107.75390625 },
                { "latitude": 19.0621178835147, "longitude": -106.083984375 },
                { "latitude": 15.9613290815966, "longitude": -100.634765625 },
                { "latitude": 14.3495478371854, "longitude": -95.537109375 },
                { "latitude": 13.4109940343217, "longitude": -91.494140625 },
                { "latitude": 16.2146745882485, "longitude": -89.296875 },
                { "latitude": 17.30868788677, "longitude": -89.47265625 },
                { "latitude": 18.3128108464254, "longitude": -86.396484375 },
                { "latitude": 23.0797317624499, "longitude": -85.693359375 },
                { "latitude": 21.6982654968525, "longitude": -91.40625 },
                { "latitude": 19.3111433550646, "longitude": -92.900390625 },
                { "latitude": 20.632784250388, "longitude": -95.712890625 },
                { "latitude": 26.6670958011048, "longitude": -96.416015625 },
                { "latitude": 26.8240707804702, "longitude": -98.876953125 },
                { "latitude": 30.221101852486, "longitude": -100.634765625 },
                { "latitude": 30.4486736792876, "longitude": -103.798828125 },
                { "latitude": 32.6208701831811, "longitude": -106.875 },
                { "latitude": 32.2499744558633, "longitude": -111.533203125 },
                { "latitude": 33.1375511923461, "longitude": -114.609375 },
                { "latitude": 33.3580616127789, "longitude": -117.685546875 }]

            };
        }

        const params: URLSearchParams = new URLSearchParams();
        params.set("polygon", JSON.stringify(polyParam));

        const requestOptions = new RequestOptions();
        requestOptions.search = params;
        
        const endpoint:string  = this.http.generateEndpoint(DriversConstants.POINT_INTEREST_PATH);
        return this.http.get(endpoint, { search: { polygon : JSON.stringify(polyParam)}})
            .map(
            (response: Response) => {

                return response.json().pointsOfInterest as IPointInterest[];
            }
            )
            .catch(this.handleError);
    }

    deletePoI(poiId: number) {

        const endpoint: string = this.http.generateEndpoint(DriversConstants.POINT_INTEREST_PATH);
        return this.http.delete(endpoint + '/' + poiId)
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }

    addPoI(PoI: any): any {

        const endpoint: string = this.http.generateEndpoint(DriversConstants.POINT_INTEREST_PATH);
        return this.http.post(endpoint, PoI)
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }
    updatePoI(PoI: any, id: number): any {
        const endpoint: string = this.http.generateEndpoint(DriversConstants.POINT_INTEREST_PATH);
        return this.http.patch(endpoint + '/' + id, PoI)
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(error);
        return Observable.throw(error);
    }
}