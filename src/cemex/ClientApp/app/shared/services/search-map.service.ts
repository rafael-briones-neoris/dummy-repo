import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DriversCoreService } from './drivers-core.service';
import { ISearchInputService, ISearchElement } from './../components/cmx-input-autocomplete';
import { ESearchMapType, ISearchMapResult } from '../types/index.interface';

@Injectable()
export class SearchMapService implements ISearchInputService {
    constructor(private _driversCore: DriversCoreService) {

    }

    public executeService(value: string): Observable<ISearchElement[]> {
        value = value.toLocaleLowerCase();
        return new Observable(observer => {
            const allTrucks = this._driversCore.allTrucks;
            const trucksByTruckPlate: ISearchMapResult[] = [];
            const trucksByDriver: ISearchMapResult[] = [];
            const trucksByOrder: ISearchMapResult[] = [];
            let allFoundItems: ISearchMapResult[] = [];

            for (const truck of allTrucks) {
                const truckPlate: string = truck.truckPlate;
                if (truckPlate.toLocaleLowerCase().indexOf(value) > -1) {
                    trucksByTruckPlate.push({
                        type: ESearchMapType.TRUCK,
                        title: truckPlate,
                        objectId: truck.truckId
                    });
                }

                const driver = truck.event.driver;
                if (driver != undefined && driver.driverDesc.toLowerCase().indexOf(value) > -1) {
                    trucksByDriver.push({
                        objectId: truck.truckId,
                        title: driver.driverDesc,
                        type: ESearchMapType.DRIVER
                    });
                }

                const validate = truck.event.ticket !== undefined && truck.event.ticket.order !== undefined;
                if (validate && truck.event.ticket.order.orderCode.indexOf(value) > -1) {
                    trucksByOrder.push({
                        objectId: truck.truckId,
                        title: truck.event.ticket.order.orderCode,
                        type: ESearchMapType.ORDER
                    });
                }
            };
            
            allFoundItems = allFoundItems.concat(trucksByTruckPlate).concat(trucksByDriver).concat(trucksByOrder);
            
            const filteredPlants: ISearchMapResult[] = []
            const allPlants = this._driversCore.monitoredPlants;
            for (const plant of allPlants) {
                const foundDesc = plant.plantDesc.toLowerCase().indexOf(value) > -1;
                if (foundDesc || plant.plantCode.toLowerCase().indexOf(value) > -1) {
                    filteredPlants.push({
                        objectId: plant.plantId,
                        title: foundDesc ? plant.plantDesc : plant.plantCode,
                        type: ESearchMapType.PLANT
                    });
                }
            }

            allFoundItems = allFoundItems.concat(filteredPlants);

            const allPoI = this._driversCore.pointInterestList;
            const filteredPoI: ISearchMapResult[] = [];
            for (const poI of allPoI) {
                const foundName = poI.pointName.toLowerCase().indexOf(value) > -1;
                const foundDesc = poI.pointDesc.toLowerCase().indexOf(value) > -1;
                if (foundName || foundDesc) {
                    filteredPoI.push({
                        objectId: poI.pointId,
                        title: foundName ? poI.pointName : poI.pointDesc,
                        type: ESearchMapType.POI
                    });
                }
            }

            allFoundItems = allFoundItems.concat(filteredPoI);

            const result: ISearchElement[] = [];
            for (const searchMap of allFoundItems) {
                result.push({
                    displayData: searchMap.title,
                    objectData: searchMap
                })
            }

            observer.next(result);
            observer.complete();
        });
    }
}