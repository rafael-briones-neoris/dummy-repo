import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { DriversCoreService } from './drivers-core.service';
import { DriversConstants } from '../constants/drivers.constants';

@Injectable()
export class DriversGuard implements CanActivateChild {

    constructor(
        private _router: Router,
        private _core: DriversCoreService) {

    }

    public canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const fullUrl:string[] = [];
        for (const segment of childRoute.url){
            fullUrl.push(segment.path);
        }
        
        if (!this._core.canAccess(fullUrl.join('/'))) {
            this._router.navigate([DriversConstants.ROUTE_TRACKING_MAP]);
        }

        return true;
    }

}