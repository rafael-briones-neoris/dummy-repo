export interface IAnalyticsService {  
    startService(url:string,AppKey:string): void;
    
	//TODO: to check if this applies to other tools than Countly as well. If not, it will be removed from the interface
	debug(flag:boolean):void;
   
	//trackingjson is the json representation of a custom event
	addTracking(key:string, value?:any);
	
	//userfields is the json representation of the fields you want to track as demographic information of a user
	identifyUser(userfields:any);

	//newId is the user identifier
	changeId(newId:string);

	addTrackingPage(page);

	endSession();

	//inits the service
	init();
	
}