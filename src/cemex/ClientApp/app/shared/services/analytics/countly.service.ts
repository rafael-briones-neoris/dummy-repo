import { IAnalyticsService } from './ianalytics.interface';

export class CountlyService implements IAnalyticsService{
    //['track_sessions'],
    public defaultTrackings:any=[['track_pageview'],['track_clicks'],['track_errors'],
    ['track_links'],['track_forms'],['collect_from_forms']];
    public Countly:any;
    constructor(){
        if (!process.env.BROWSER) {
          this.Countly = (<any>global)["Countly"];
        }else{
          this.Countly = (<any>window)["Countly"];
        }
        //Countly.q = Countly.q || [];
    }

    public init(){
        this.Countly.init();
    }

    public startService( url:string, AppKey:string ): void {
        if( this.Countly !== undefined ){
            this.Countly.url = url;
            this.Countly.app_key = AppKey;
            this.Countly.q=this.defaultTrackings;
        }
    }

    public debug(flag:boolean){
        //default tracking is false
        this.Countly.debug=flag;
    }

    public addTracking(key:string, value?:any){
        if(!value){
            this.Countly.q.push([key]);
            return;
        }
        this.Countly.q.push([key,value]);
    }

    public addTrackingPage(page){
        this.Countly.q.push(["track_pageview", page]);
    }

    public changeId(newId:string){
        this.Countly.q.push(["change_id", newId]);
    }
	
	public identifyUser(userfields:any){
        this.Countly.q.push(['user_details',userfields]);
    }

    public endSession(){
        this.Countly.q.push(['end_session']);
    }

    public removeTracking(trackingjson:any){
        this.Countly.q.forEach(element => {
            if(element.indexOf(trackingjson)!=-1){
                element[0]="canceled"
            }
        });
    }
}