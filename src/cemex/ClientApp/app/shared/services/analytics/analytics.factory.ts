import { Injectable } from '@angular/core';
import { IAnalyticsService } from './ianalytics.interface';
import { CountlyService } from './countly.service';
// import { ClevertapService } from './clevertap.service';
import {IAnalyticsConfiguration} from './ianalytics.configuration';


@Injectable()
export class AnalyticsFactory
{
    private service: IAnalyticsService;
    private type: string;
    private _configuration:IAnalyticsConfiguration
    constructor(){
    }
    
    public setConfiguration(configuration:IAnalyticsConfiguration){
        this._configuration=configuration;
    }

    public createService (type:string){        
        this.type = type;
        if( this.type === "countly" ){
            this.service = new CountlyService();
            this.service.startService( this._configuration.url,this._configuration.key );
        }
        else if (type==="clevertap"){};
            // service = new ClevertapService();
        return this.service;
    }

}