CYAN='\033[0;36m'
NC='\033[0m' # No Color

if [ "$1" == "clean" ]; then
    printf "Cleaning ${CYAN}.NET${NC} files\n"
    rm -rf bin/
    rm -rf obj/

    printf "Cleaning ${CYAN}npm modules${NC}\n"
    rm -rf node_modules/

    printf "Cleaning ${CYAN}webpack${NC} dist\n"
    rm -rf ClientApp/dist/
    rm -rf wwwroot/dist/
fi

# Default dev
export ASPNETCORE_ENVIRONMENT=Development
M_PORT=5000
M_ENV=qa

while getopts "p:e:" option
do
    case "${option}"
    in
        p) M_PORT=${OPTARG};;
        e) M_ENV=${OPTARG};;
    esac
    
done


if [ "$M_ENV" == "qa" ]
then
    export API_HOST=https://api.us2.apiconnect.ibmcloud.com/
    export API_ORG=cnx-gbl-org-quality/
    export API_ENV=quality/
    export APP_CODE=Drivers_Console
    export CLIENT_ID=721e5c7b-73b8-40e1-8cb2-31c7dbdbd1be

    export TRUCK_LOC_HOST=https://uscldcnxfgdqsc.azurewebsites.net
    export TRUCK_LOC_ENV=N2hVNXJBQCZ0UnV2OmZyQURBM1VzdGVDKg==
    export API_HOST_MW=https://api.us2.apiconnect.ibmcloud.com/
    export BASE_URL=drivers/
    export DUMMY_KEY_FOR_LOCAL=yes
    export TRANSLATE_URL=http://server-status.mybluemix.net/
fi

if [ "$M_ENV" == "prod" ]
then
    export API_HOST=https://api.us2.apiconnect.ibmcloud.com/
    #export API_HOST_FULL=https://api.us2.apiconnect.ibmcloud.com/cnx-gbl-org-production/production/
    export API_ORG=cnx-gbl-org-production/
    export API_ENV=production/
    export APP_CODE=Drivers_Console
    export CLIENT_ID=67b773d1-5b39-4444-841c-e97f8b2b28b8

    export TRUCK_LOC_HOST=https://uscldcnxfgdpsc.azurewebsites.net
    export TRUCK_LOC_ENV=czhFU2ErK3N3YXJVOmd1UFU4P1dSN3Z1ZA==
    export API_HOST_MW=https://api.us2.apiconnect.ibmcloud.com/
    export BASE_URL=drivers/
    export DUMMY_KEY_FOR_LOCAL=yes
    export TRANSLATE_URL=http://server-status.mybluemix.net/
fi

printf "Restoring .NET ${CYAN}project.json${NC} dependencies\n"
dotnet restore

printf "Restoring npm ${CYAN}package.json${NC} dependencies\n"
yarn install

printf "Compiling with ${CYAN}webpack${NC}\n"
webpack --config webpack.config.vendor.js
webpack --config webpack.config.js

printf "Running .NET ${CYAN}server${NC}\n"
ASPNETCORE_URLS="http://*:${M_PORT}" dotnet run 