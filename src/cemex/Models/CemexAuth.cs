using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationBasic
{
    public class CemexAuth
    {
        
        public string jwt { get; set; }

        public string json { get; set; }
        public OAuth2 oauth2{get;set;}
        public string country{get;set;}
        public Application[] applications {get;set;}
        public UserProfile profile;
    }

    public class OAuth2{
        public string access_token{get;set;}
        public int expires_in{get;set;}
        public string scope{get;set;}
        public string refresh_token{get;set;}
    }

    public class Application{
        public int applicationId{get;set;}
        public string applicationName{get;set;}
        public string applicationCode{get;set;}
        public string applicationDesc{get;set;}
        public AppRole[] roles{get;set;}



    }

    public class AppRole{
        public int roleId{get;set;}
        public string roleCode{get;set;}
        public string roleName{get;set;}
        public string roleDesc{get;set;}
        public string roleType{get;set;}
    }

    public class UserProfile{
        public int customerId{get;set;}
        public int userId{get;set;}
        public string status{get;set;}
        public string userAccount{get;set;}
        public string fullName{get;set;}
        public string firstName{get;set;}
        public string lastName{get;set;}
        public string phoneNumber{get;set;}
        public string userType{get;set;}
        public string userPosition{get;set;}
        public string allowInformationShare{get;set;}
        public string allowEmailUpdates{get;set;}
    }

    public class RoleConstants{

        public static string TranslatorRole="CMX_PROPS";
    }
}
