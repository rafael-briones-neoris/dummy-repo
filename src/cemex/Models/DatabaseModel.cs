using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.IO;

namespace WebApplicationBasic
{
    public class TranslationContext : DbContext
    {
        public DbSet<ApplicationItem> Applications { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<TranslationKey> TranslationKeys { get; set; }
        public DbSet<TranslationValue> TranslationValues { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (Directory.GetCurrentDirectory().IndexOf("home/vcap")!=-1)
            {
                //is in bluemix cloud
                optionsBuilder.UseSqlite("Data Source=/home/vcap/app/src/cemex/translations.db");
            } else {
                optionsBuilder.UseSqlite("Data Source=translations.db");
            }
            
        }
    }

    public class ApplicationItem{
        public int ApplicationItemId{get;set;}
        public string ApplicationItemName{get;set;}
        public string ApplicationItemTechnicalName{get;set;}
        public string ApplicationItemDescription{get;set;}
        public string AuthorApplication{get;set;}
        public int TechnicalMode{get;set;}
    }

    public class Language{
        public int LanguageId{get;set;}
        public string LanguageName{get;set;}
        public string LanguageCountry{get;set;}
        public string LanguageISO{get;set;}
        public string AuthorLanguage{get;set;}
        public string CurrencySymbol{get;set;}
        public string CurrencyFormat{get;set;}
        public string CurrencySymbolFloat{get;set;}
        public string CurrencyName{get;set;}
        public string FormatDate{get;set;}
        public string FormatTime{get;set;}
        public string DecimalSeparator{get;set;}
        public int DecimalNumbers{get;set;}
        public string ThousandSeparator{get;set;}
        public string textFloat{get;set;}
        
        

    }

    public class TranslationKey{
        public int TranslationKeyId{get;set;}
        public string Key{get;set;}
        public string AuthorKey{get;set;}
    }
    public class TranslationValue{
        public int TranslationValueId{get;set;}
        public TranslationKey TranslationKeyReferenced{get;set;}
        public string Translation{get;set;}
        public Language LanguageISO{get;set;}
        public ApplicationItem ApplicationId{get;set;}
        public string AuthorValue{get;set;}
    }

    public class TranslationDTO{
        public string translationKeyString{get;set;}
        public string Translation{get;set;}
        public int LanguageISO{get;set;}
        public int ApplicationId{get;set;}
        
    }

    public class MassUploadDTO{
        public int LanguageId;
        public int ApplicationId;
        public KeyPairTranslation[] MassJson;
    }
    public class KeyPairTranslation{
        public string Key;
        public string Value;
    }
}