using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApplicationBasic.Models
{
    public class Login
    {
        public string grant_type { get; set; }
        public string scope { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string client_id { get; set; }

    }
}