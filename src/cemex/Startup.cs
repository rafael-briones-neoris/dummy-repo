using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.ResponseCompression;
using Chroniton;
using Chroniton.Jobs;
using Chroniton.Schedules;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace WebApplicationBasic
{
    public class Startup
    {
        static string hostTranslate = Environment.GetEnvironmentVariable("TRANSLATE_URL");
        private static readonly HttpClient client = new HttpClient();
        private ILogger _logger;
        public Startup(IHostingEnvironment env)
        {
            client.Timeout = TimeSpan.FromSeconds(GeneralConstant.COMMON_TIMEOUT_SECS);
            System.Console.WriteLine("starting...");
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var factory = new SingularityFactory();
            var singularity = factory.GetSingularity();
            
            var endpointPing = string.Concat(hostTranslate, "api/statusServer/ping");
            var job = new SimpleParameterizedJob<string>(async (parameter, scheduledTime) =>
                await pingAliveToServer(parameter, scheduledTime, endpointPing)
                );

            var schedule = new EveryXTimeSchedule(TimeSpan.FromSeconds(60));
            var scheduledJob = singularity.ScheduleParameterizedJob(
                schedule, job, "PING: ", true); //starts immediately
            try
            {
                singularity.Start();
            }
            catch (AggregateException errors)
            {
                errors.Handle(e =>
                {
                    if (e is TaskCanceledException)
                    {
                        this._logger.LogWarning("A Canceled Task has occurred at doing ping to {endpointPing}", endpointPing);
                    }
                    return true;
                });
            }
        }

        public async Task pingAliveToServer(string pingMessage, DateTime timeSnapshot, string endpointPing)
        {
            var values = new Dictionary<string, string>
                {
                    { "DigitalServiceName", "ngXboilerPlate" },
                    { "TechnicalServiceName", "ngxboilerPlate" }
                };
            string jsonResponse = JsonConvert.SerializeObject(values, Formatting.Indented);
            StringContent jsonStringContent = new StringContent(jsonResponse, Encoding.UTF8, "application/json");
            try
            {
                var response = await client.PostAsync(endpointPing, jsonStringContent);
                var responseString = await response.Content.ReadAsStringAsync();
                //this._logger.LogInformation($"{pingMessage}host:{endpointPing}  \tscheduled: {timeSnapshot.ToString("o")} response: ${responseString}");
            }
            catch (TaskCanceledException)
            {
                // handle somehow
                this._logger.LogWarning("A Canceled Task has occurred at doing ping to {endpointPing}", endpointPing);
            }
            catch (HttpRequestException e)
            {
                this._logger.LogWarning("An error occurred at doing ping to {endpointPing}", endpointPing, e.Message);
            }

        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);
            services.AddResponseCompression(options =>
            {
                options.MimeTypes = new[]
                {
                    // Default
                    "text/plain",
                    "text/css",
                    "application/javascript",
                    "text/html",
                    "application/xml",
                    "text/xml",
                    "application/json",
                    "text/json",
                    // Custom
                    "image/svg+xml",
                    "image/jpeg",
                    "image/jpg",
                    "image/png"
                };

                options.EnableForHttps = true;
            });

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(1500);
                options.Cookie.HttpOnly = true;
            });
            services.AddDbContext<TranslationContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            this._logger = loggerFactory.CreateLogger<Startup>();

            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
