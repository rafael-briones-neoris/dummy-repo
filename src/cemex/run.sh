#!/bin/bash

CYAN='\033[0;36m'
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
PURPLE='\033[1;35m'
NC='\033[0m' # No Color

#Welcome Message
echo -e "${CYAN}Welcome to the compilation wizard. v1.0.0\n"

#SELECT ENVIRONMENT
echo -e "${RED}1/5 - Choose a Environment.${NC}"
PS3="Environment number: "
environmentOptions=("Quality" "Pre-Production" "Production" )
select environmentOption in "${environmentOptions[@]}"
do
    case $environmentOption in
        "Quality" | "Pre-Production" | "Production"  )
            break;;
        *)
        echo -e "${YELLOW}Invalid Environment option, try again.${NC}"
    esac
done
echo -e "Selected Environment: ${GREEN}$environmentOption${NC}."

echo -e "\n"

#SELECT REGION
echo -e "${RED}2/5 - Choose a Region.${NC}"
PS3="Region number: "
regionOptions=("America" "Europe")
select regionOption in "${regionOptions[@]}"
do
    case $regionOption in
        "America" | "Europe")
            break;;
        *)
        echo -e "${YELLOW}Invalid Region option, try again.${NC}"
    esac
done
echo -e "Selected Region: ${GREEN}$regionOption${NC}."

echo -e "\n"

#SELECT IF CLEAN
echo -e "${RED}3/5 - Do you want to Clean? [y/n].${NC}"
while true
do
    read -p "Clean option: " cleanOption
    case $cleanOption in
            [yY] | [yY][Ee][Ss] | 1)
                cleanOption=1
                echo -e "Selected Clean option: ${GREEN}Yes${NC}."
                break;;
            [nN] | [n|N][O|o] | 0)
                cleanOption=0
                echo -e "Selected Clean option: ${GREEN}No${NC}."
                break;;
            *)
                echo -e "${YELLOW}Invalid Clean option, try again.${NC}";;
    esac
done

echo -e "\n"

#SELECT IF RESTORE DOTNET
echo -e "${RED}4/5 - Do you want to Restore .NET? [y/n].${NC}"
while true
do
    read -p "Restore .NET option: " dotnetOption
    case $dotnetOption in
            [yY] | [yY][Ee][Ss] | 1)
                dotnetOption=1
                echo -e "Selected Restore .NET option: ${GREEN}Yes${NC}."
                break;;

            [nN] | [n|N][O|o] | 0)
                dotnetOption=0
                echo -e "Selected Restore .NET option: ${GREEN}No${NC}."
                break;;
            *)
                echo -e "${YELLOW}Invalid Restore .NET option, try again.${NC}";;
    esac
done

echo -e "\n"

#SELECT IF EXEC YARN INSTALL
echo -e "${RED}5/5 - Do you want to execute Yarn Install? [y/n].${NC}"
while true
do
    read -p "Yarn Install option: " yarninstallOption
    case $yarninstallOption in
            [yY] | [yY][Ee][Ss] | 1)
                yarninstallOption=1
                echo -e "Selected Yarn Install option: ${GREEN}Yes${NC}."
                break;;

            [nN] | [n|N][O|o] | 0)
                yarninstallOption=0
                echo -e "Selected Yarn Install option: ${GREEN}No${NC}."
                break;;
            *)
                echo -e "${YELLOW}Invalid Yarn Install option, try again.${NC}";;
    esac
done

echo -e "\n"

#VALIDATE IF CLEAN
if [ $cleanOption == 1 ];
    then
        echo -e "${PURPLE}Cleaning"
            echo -e "\t${CYAN}.NET ${NC}(bin/obj)"
            rm -rf bin/
            rm -rf obj/

            echo -e "\t${CYAN}Node Modules${NC}"
            rm -rf node_modules/

            echo -e "\t${CYAN}Dist Folders ${NC}(ClientApp/wwwroot)"
            rm -rf ClientApp/drivers/dist/
            rm -rf wwwroot/drivers/dist/

        echo -e "${PURPLE}RE-Installing WEBPACK${NC}"
            npm install webpack
    else
        echo -e "${PURPLE}Skipping ${CYAN}Clean${NC}"
fi

#EXPORT VARIABLES FROM FILE MANIFEST
#Create manifest name file
manifestFilePath="../../manifest-{env}-{reg}.yml"

#replace region
case $regionOption in
    "America" | "ame" )
        manifestFilePath=${manifestFilePath/"{reg}"/ame}
        ;;
    "Europe" | "eur" )
        manifestFilePath=${manifestFilePath/"{reg}"/eur}
        ;;
    *)
         manifestFilePath=${manifestFilePath/"{reg}"/ame}
        echo -e "${YELLOW}Invalid Region option. ${PURPLE}Setting${NC} default Region: ${GREEN}America${YELLOW}.${NC}"
esac
#replace environment
case $environmentOption in
    "Quality" | "qa" )
        manifestFilePath=${manifestFilePath/"{env}"/qa}
        ;;
    "Pre-Production" | "pp" | "pre" | "stg" )
        manifestFilePath=${manifestFilePath/"{env}"/stg}
        ;;
    "Production" | "pr" | "prod" )
        manifestFilePath=${manifestFilePath/"{env}"/prod}
        ;;
    *)
        manifestFileName=${manifestFileName/"{env}"/qa}
        echo -e "${YELLOW}Invalid Environment option. ${PURPLE}Setting${NC} default Region: ${GREEN}Quality${YELLOW}.${NC}"
esac

echo -e "\n${PURPLE}Exporting${NC}"
        echo -e "\t${CYAN}Environment Variables ${NC}from ${GREEN}$manifestFilePath${NC}"

parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*'
   local w='[a-zA-Z0-9_]*'
   local fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {
        if (i > indent) {
            echo -e "export";
            delete vname[i]}
        }
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

if [ ! -f $manifestFilePath ]; then
    echo -e "${YELLOW}File NOT found: ${GREEN}$manifestFilePath${NC}".
    else
    eval $(parse_yaml $manifestFilePath )
fi

#exporting variables
export "ASPNETCORE_ENVIRONMENT"="$applications_env_ASPNETCORE_ENVIRONMENT"
export "DOTNET_CLI_TELEMETRY_OPTOUT"="$applications_env_DOTNET_CLI_TELEMETRY_OPTOUT"
export "DOTNET_SKIP_FIRST_TIME_EXPERIENCE"="$applications_env_DOTNET_SKIP_FIRST_TIME_EXPERIENCE"
export "API_HOST"="$applications_env_API_HOST"
export "API_ORG"="$applications_env_API_ORG"
export "API_ENV"="$applications_env_API_ENV"
export "APP_CODE"="$applications_env_APP_CODE"
export "CLIENT_ID"="$applications_env_CLIENT_ID"
export "TRUCK_LOC_HOST"="$applications_env_TRUCK_LOC_HOST"
export "TRUCK_LOC_ENV"="$applications_env_TRUCK_LOC_ENV"
export "API_HOST_MW"="$applications_env_API_HOST_MW"
export "BASE_URL"="$applications_env_BASE_URL"
export "DUMMY_KEY_FOR_LOCAL"="$applications_env_DUMMY_KEY_FOR_LOCAL"
export "TRANSLATE_URL"="$applications_env_TRANSLATE_URL"

#VALIDATE IF RESTORE DOTNET
if [ $dotnetOption == 1 ]; then
    echo -e "\n${PURPLE}Restoring${NC}"
    echo -e "\t${CYAN}.NET${NC}"
    dotnet restore
    else
        echo -e "\n${PURPLE}Skipping ${CYAN}.NET${NC} Restore"
fi


#VALIDATE IF EXEC YARN INSTALL
if [ $yarninstallOption == 1 ]; then
    echo -e "\n${PURPLE}Executing${NC}"

        echo -e "\t${CYAN}Yarn Install ${NC}- ${CYAN}DLS${NC}"
        cd submodules/dls
        yarn install
        cd ../..

        echo -e "\t${CYAN}Yarn Install ${NC}- ${CYAN}Drivers${NC}"
        yarn install
    else
        echo -e "\n${PURPLE}Skipping ${CYAN}Yarn Install${NC}"
fi


echo -e "\n${PURPLE}Compiling ${CYAN}"

    echo -e "\t${CYAN}DLS ${NC}"
    cd submodules/dls
    webpack --config webpack.config.js
    cd ../..

    echo -e "\t${CYAN}Drivers${NC}"
    webpack --config webpack.config.vendor.js --env.prod
    webpack --config webpack.config.js --env.prod


echo -e "\n${PURPLE}Running${NC}"
    echo -e "\t${CYAN}.NET server${NC}"
    dotnet run

# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37